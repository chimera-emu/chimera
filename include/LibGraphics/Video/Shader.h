#pragma once

#include <glad/glad.h>

#include <string>

class Shader
{
private:
  GLuint program;

public:
  Shader() : program(0) {}

  GLuint GetProgram() const { return program; }

public:
 void LoadShaders(const std::string& vertexPath,
                  const std::string& fragmentPath);
};
