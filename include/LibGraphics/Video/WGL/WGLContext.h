#pragma once

#include <LibGraphics/Video/Buffer.h>
#include <LibGraphics/Video/Colour.h>
#include <LibGraphics/Video/Position.h>
#include <LibGraphics/Video/Shader.h>
#include <LibGraphics/Window/Window.h>
#include <LibGraphics/Window/WindowHandle.h>

#include <glad/glad.h>

#include <vector>

class WGLContext
{
private:
  // Window handle
  WindowHandle windowHandle;
  // GDI Device Context
  HDC deviceContext;
  // Rendering Context
  HGLRC renderContext;

public:
  explicit WGLContext(const WindowHandle handle);

public:
  void CreateSurface();

  void CreateContext();

  void MakeCurrent();

  void LoadExtensions();

  void Render(const Shader& shader);
};
