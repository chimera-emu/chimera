#pragma once

#include <LibGraphics/Video/Shader.h>
#include <LibGraphics/Window/WindowHandle.h>

class GLXContext
{
public:
  explicit GLXContext(const WindowHandle) {}

public:
  void CreateSurface() {}

  void CreateContext() {}

  void MakeCurrent() {}

  void LoadExtensions() {}

  void Render(const Shader&) {}
};
