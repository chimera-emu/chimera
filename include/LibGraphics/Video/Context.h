#ifdef PLATFORM_WINDOWS

  #include "WGL/WGLContext.h"

  using Context = WGLContext;

#endif

#ifdef PLATFORM_LINUX

  #include "GLX/GLXContext.h"

  using Context = GLXContext;

#endif
