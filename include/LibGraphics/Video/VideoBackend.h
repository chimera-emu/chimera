#pragma once

#include <LibGraphics/Video/Colour.h>
#include <LibGraphics/Video/Position.h>
#include <LibGraphics/Video/Buffer.h>
#include <LibGraphics/Video/Context.h>
#include <LibGraphics/Video/Shader.h>
#include <LibGraphics/Window/WindowHandle.h>

#include <glad/glad.h>

#include <assert.h>
#include <vector>

class VideoBackend
{
private:
  Context context;

  Shader shader;

  GLuint vao;

  Buffer vbuffer;
  Buffer cbuffer;

  uint32_t bufferSize;

public:
  explicit VideoBackend(const WindowHandle handle);

public:
  static VideoBackend* GetBackend();

  static void Initialise(const WindowHandle handle);

public:
  GLuint GenerateVertexBuffer();

  void PushTriangle(const std::vector<Position>& positions,
                    const std::vector<Colour>& colours);

  void PushQuad(const std::vector<Position>& positions,
                const std::vector<Colour>& colours);

  void Render();
};
