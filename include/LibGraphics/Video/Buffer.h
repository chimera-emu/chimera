#pragma once

#include <glad/glad.h>
#include <vector>

class Buffer
{
private:
  std::vector<GLfloat> data_;

  GLuint index_;

  uint32_t count_;

public:
  Buffer() : index_(0), count_(0) {}

  Buffer(const GLuint index, const uint32_t count) : index_(index), count_(count) {}

  GLuint GetIndex() const { return index_; }
  uint32_t GetCount() const { return count_; }

  size_t ByteSize() const { return (data_.size() * sizeof(GLfloat)); }
  GLfloat* DataPtr() { return &data_[0]; }

public:
  void push_back(const GLfloat value) { data_.push_back(value); }

  bool empty() const { return data_.empty(); }
  void clear() { data_.clear(); }

};
