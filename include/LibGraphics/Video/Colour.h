#pragma once

#include <cstdint>

// TODO: This is only specific to PSX, it should be somewhere else

struct Colour
{
  uint8_t red;
  uint8_t green;
  uint8_t blue;

public:
  Colour() : red(0), green(0), blue(0) {}

  explicit Colour(const uint32_t value)
  {
    red   = static_cast<uint8_t>(value);
    green = static_cast<uint8_t>(value >> 8);
    blue  = static_cast<uint8_t>(value >> 16);
  }

  float NormaliseRed() const { return (static_cast<float>(red) / 256); }
  float NormaliseGreen() const { return (static_cast<float>(green) / 256); }
  float NormaliseBlue() const { return (static_cast<float>(blue) / 256); }
};
