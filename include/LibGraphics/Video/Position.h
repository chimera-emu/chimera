#pragma once

#include <cstdint>

// TODO: This is only specific to PSX, it should be somewhere else

struct Position
{
  int16_t posX;
  int16_t posY;

public:
  Position() : posX(0), posY(0) {}

  explicit Position(const uint32_t value)
  {
    posX = static_cast<int16_t>(value);
    posY = static_cast<int16_t>(value >> 16);
  }

  Position(const int16_t x, const int16_t y) : posX(x), posY(y) {}

  float NormaliseX() const { return (static_cast<float>(posX) / 512) - 1; }
  float NormaliseY() const { return 1 - (static_cast<float>(posY) / 256); }
};
