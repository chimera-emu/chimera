#pragma once

#include "LibGraphics/Window/Keyboard.h"

#include <cstdint>

class Event
{
public:
  enum class EventKind : uint8_t
  {
    EK_Unknown,
    EK_KeyboardEvent
  };

  struct KeyboardEvent
  {
    Keyboard::KeyCode code;
  };

public:
  EventKind eventKind;

  union
  {
    KeyboardEvent keyboardEvent;
  };

public:
  Event() : eventKind(EventKind::EK_Unknown) {}

  explicit Event(const Keyboard::KeyCode code)
    : eventKind(EventKind::EK_KeyboardEvent)
  {
    keyboardEvent.code = code;
  }

public:
  bool IsKeyboardEvent() const
  {
    return eventKind == EventKind::EK_KeyboardEvent;
  }
};
