#pragma once

#include "LibGraphics/Window/Event.h"

#include <assert.h>
#include <cstdint>
#include <deque>

template <class Derived>
class WindowBase
{
protected:
  std::deque<Event> events;

public:
  void PushEvent(const Event& event)
  {
    events.emplace_back(event);
  }

  bool FetchEvent(Event&)
  {
    assert(0 && "Event handling not implemented on this platform");

    return false;
  }

public:
  bool IsOpen() const { return false; }
};
