#pragma once

#ifdef PLATFORM_WINDOWS

  #include <windows.h>

  using WindowHandle = HWND;

#endif

#ifdef PLATFORM_LINUX

  #include <cstdint>

  using WindowHandle = uint32_t;

#endif
