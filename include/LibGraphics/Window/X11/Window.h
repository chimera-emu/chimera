#pragma once

#include <LibGraphics/Window/Base/WindowBase.h>
#include <LibGraphics/Window/WindowHandle.h>

#include <string>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>

class Window : public WindowBase<Window>
{
public:
  using XCBConnection = xcb_connection_t;
  using XCBWindow     = xcb_window_t;
  using XCBScreen     = xcb_screen_t;
  using XCBSetup      = xcb_setup_t;

private:
  XCBConnection* connection;

  XCBScreen* screen;

  XCBWindow window;

public:
  Window(const uint32_t width, const uint32_t height, const std::string& title);

public:
  void SetTitle(const std::string& title);

  WindowHandle RenderHandle() const { return window; }

  // TODO: Not sure this is correct. Check if this is
  bool IsOpen() const { return window != 0; }

  bool FetchEvent(Event& event);

  void HandleEvent(xcb_generic_event_t* event);
};
