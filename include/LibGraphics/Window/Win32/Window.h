#pragma once

#include <LibGraphics/Window/Base/WindowBase.h>
#include <LibGraphics/Window/WindowHandle.h>

#include <cstdint>
#include <string>
#include <windows.h>

class Window : public WindowBase<Window>
{
private:
  HWND handle;

public:
  Window(const uint32_t width, const uint32_t height, const std::string& title);

public:
  bool IsOpen() const { return handle != nullptr; }

  void SetTitle(const std::string& title);

  bool FetchEvent(Event& event)
  {
    MSG message;
    while (PeekMessageW(&message, NULL, 0, 0, 1))
    {
        TranslateMessage(&message);
        DispatchMessageW(&message);
    }

    if (events.empty())
      return false;

    event = events.front();
    events.pop_front();

    return true;
  }

  WindowHandle RenderHandle() const { return handle; }

private:
  void RegisterWindowClass();

  void WndProc(UINT message, WPARAM wParam, LPARAM lParam);

public:
 static LRESULT CALLBACK StaticWndProc(HWND handle, UINT message, WPARAM wParam,
                                       LPARAM lParam);
};
