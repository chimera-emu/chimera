#pragma once

#ifdef PLATFORM_WINDOWS

  #include "Win32/Window.h"

#endif

#ifdef PLATFORM_LINUX

  #include "X11/Window.h"

#endif
