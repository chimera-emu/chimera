#pragma once

#include <Core/PSX/Emulator.h>
#include <Common/ConfigData.h>
#include <LibGraphics/Window/Window.h>

#include <memory>

class Process
{
private:
  ConfigData config_;

  std::unique_ptr<Window> window_;
  std::unique_ptr<Emulator> emulator_;

public:
  explicit Process(const ConfigData& config)
    : config_{config}, window_{nullptr}, emulator_{nullptr} {}

public:
  void Initialise();
  void Run();
  void Shutdown();
};
