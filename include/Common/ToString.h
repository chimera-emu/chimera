#pragma once

#include <sstream>
#include <string>

template <class Type>
inline std::string ToString(const Type& type)
{
  std::ostringstream buffer(std::ios_base::out);

  buffer << type;

  return buffer.str();
}
