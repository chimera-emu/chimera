#pragma once

#include <stdexcept>
#include <string>

class CommandLineException : public std::invalid_argument
{
public:
  CommandLineException(const std::string& error)
    : std::invalid_argument(error) {}
};
