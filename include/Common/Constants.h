#pragma once

constexpr auto  B = 1;
constexpr auto KB = 1024;
constexpr auto MB = 1024 * 1024;
constexpr auto GB = 1024 * 1024 * 1024;
