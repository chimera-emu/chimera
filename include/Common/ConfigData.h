#pragma once

#include <string>

class ConfigData
{
private:
  std::string bios_;

public:
  ConfigData(int argc, char** argv);

public:
  std::string Bios() const { return bios_; }
};
