#pragma once

#include <spdlog/spdlog.h>

inline spdlog::logger* Logger()
{
  static auto logger = spdlog::stdout_color_mt("Bknd").get();

  return logger;
}
