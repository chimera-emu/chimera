#pragma once

#include <Core/PSX/Machine.h>

#include <Common/ConfigData.h>

#include <memory>

class ChimeraProcess
{
private:
  // User configuration set on the commandline
  ConfigData config_;

  std::unique_ptr<Machine> machine_;

public:
  explicit ChimeraProcess(const ConfigData& config)
    : config_{config}, machine_{nullptr} {}

public:
  void Initialise();
  void Run();
  void Shutdown();

private:
  void CreateMachine();
};
