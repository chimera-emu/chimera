#pragma once

#include <Core/PSX/Gpu/CommandBuffer.h>
#include <Core/PSX/Gpu/GpuState.h>
#include <Core/PSX/Gpu/ImageBuffer.h>
#include <Core/PSX/Gpu/Types/DmaDirection.h>

#include <Core/PSX/Interrupts/InterruptController.h>
#include <Core/PSX/Memory/MMIOInterface.h>

#include <LibGraphics/Video/VideoBackend.h>
#include <LibGraphics/Window/WindowHandle.h>

#include <cstdint>

namespace Gpu
{

class GpuController : public MMIOInterface
{
private:
  enum class CommandType : uint32_t
  {
    Command   = 0x00,
    Parameter = 0x01,
    ImageData = 0x02
  };

private:
  GpuState state;

  uint32_t current;

  CommandType type;

  CommandBuffer buffer;
  ImageBuffer image;

  VideoBackend* backend;
  Interrupts::InterruptController* interrupts_;

public:
  explicit GpuController(Interrupts::InterruptController *interrupts);

public:
  void HandleWrite(const uint32_t address, const uint32_t value) override;

public:
  uint32_t HandleRead(const uint32_t address) const override;

private:
  uint32_t HandleGP0Read() const;
  uint32_t HandleGP1Read() const;

private:
  void HandleGP0Write();
  void HandleGP1Write();

  // HACK: This is pretty awful, figure something better out
  inline void HandleGP0Command();
  inline void HandleGP0Parameter();
  inline void HandleGP0ImageData();

  // GP0 Commands
  void NoOperation() {}             // 0x00
  void ClearCache();                // 0x01
  void DrawMonochromeQuad();        // 0x28
  void DrawTexturedQuad();          // 0x2C
  void DrawShadedTriangle();        // 0x30
  void DrawShadedQuad();            // 0x38
  void LoadImageData();             // 0xA0
  void SaveImageData();             // 0xC0
  void SetDrawMode();               // 0xE1
  void SetTextureWindow();          // 0xE2
  void SetDrawingAreaTopLeft();     // 0xE3
  void SetDrawingAreaBottomRight(); // 0xE4
  void SetDrawingOffset();          // 0xE5
  void SetMaskBit();                // 0xE6

  // GP1 Commands
  void ResetGpu();                   // 0x00
  void ResetCommandBuffer();         // 0x01
  void ResetInterrupt();             // 0x02
  void SetDisplayEnabled();          // 0x03
  void SetDmaDirection();            // 0x04
  void SetVramStartArea();           // 0x05
  void SetHorizontalDisplayRange();  // 0x06
  void SetVerticalDisplayRange();    // 0x07
  void SetDisplayMode();             // 0x08
};

} // namespace Gpu

// Gpu Controller
//
// Summary
//
//
// ================
// Command Handling
// ================
//
// GPU commands are sent to the GPU via the I/O ports. An instruction consists
// of an opcode and a variable number of parameters/data that follow it.
//
// The first word received on the port is the opcode for the command. The opcode
// is used to initialise the CommandBuffer. The capacity of the buffer is
// determined by the opcode, commands may require between 0 and 12 additional
// parameters.
//
// For simple commands that require only one word(the opcode) the command is
// executed and the command buffer is reset for the next command.
//
// For commands that require additional parameters, data being received on the
// I/O port is pushed into the command buffer until it reaches the set capacity.
//
// For some commands, such as LoadImageData, image data is sent to the I/O port.
// For these opcodes, once the command buffer is filled, an ImageBuffer is
// created to store the impending image data.
//
// The size of the image buffer is specified by the preceding parameters inside
// the command buffer. We push data into the ImageBuffer is filled then execute
// the command. When the command has finished executing we reset the
// Command and Image buffers and wait for a new command.
//
