#pragma once

#include <cstdint>
#include <vector>

class CommandBuffer
{
private:
  std::vector<uint32_t> buffer;

  // Number of words the current command takes
  uint32_t total;

public:
  CommandBuffer() : buffer(std::vector<uint32_t>()), total(0) {}

public:
  bool IsPending() const;
  // Returns the opcode for the command currently in the buffer
  uint32_t GetCommand() const;
  // Returns the word in the buffer at the given index
  uint32_t GetWord(const uint32_t index) const { return buffer[index]; }

public:
  // Push the given command into the buffer. If this is the first word received
  // determine the buffer size from the opcode in the command and set the
  // expected buffer size accordingly
  void PushCommand(const uint32_t command);
  // Clear all command data out of the buffer, reset total
  void ClearBuffer();
};

// GPU Command Buffer
//
// CommandBuffer is an object which stores a GPU command and a variable number
// of additional parameters that the command requires.
//
// For any given command the opcode for the command is stored in the uppermost
// byte of the first word. The remaining 3 bytes store any parameters for the
// command if any are required. Certain commands, such as the draw commands
// require additional parameters that are sent as additional words and stored in
// the command buffer.
//
// Each command has a specific number of parameters, when pushing the intial
// command word into the buffer we lookup the expected number of parameters for
// the given opcode and accept additional parameters up to this value.
//
// As an example, the layout for the "Draw Monochrome Triangle" command is:
//
// +---+---------------------------------------------------+
// |   |31           23           15           7          0|
// +---+---------------------------------------------------+
// | 0 |   Opcode   |  Colour B  |  Colour G  |  Colour R  |
// +---+---------------------------------------------------+
// | 1 |       Position Y        |       Position X        |
// +---+---------------------------------------------------+
// | 2 |       Position Y        |       Position X        |
// +---+---------------------------------------------------+
// | 3 |       Position Y        |       Position X        |
// +---+---------------------------------------------------+
//
// TODO: Revise the method of looking up the parameter count for a given opcode.
//       It should probably be in some static lookup table along with the
//       command name and any other information relating to the operation of the
//       command.
//
