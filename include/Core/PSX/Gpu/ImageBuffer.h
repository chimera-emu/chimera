#pragma once

#include <iostream>
#include <vector>

class ImageBuffer
{
private:
  uint16_t x_;
  uint16_t y_;

  uint16_t width_;
  uint16_t height_;

  uint32_t size_;

  std::vector<uint32_t> buffer_;

public:
  ImageBuffer() : x_(0), y_(0), width_(0), height_(0), size_(0), buffer_(0) {}

  ImageBuffer(const uint16_t x, const uint16_t y, const uint16_t width,
              const uint16_t height)
    : x_(x), y_(y), width_(width), height_(height), size_(), buffer_()
  {
    size_  = (width_ * height_);
    size_ += (size_ % 2);
    size_ /= 2;

    buffer_.reserve(size_);
  }

  void ClearBuffer()
  {
    buffer_.clear();

    size_ = 0;
  }

  bool IsPending() const
  {
    return (buffer_.size() < size_);
  }

  void PushData(const uint32_t data)
  {
    buffer_.push_back(data);
  }

public:
  uint16_t X() const { return x_; }
  uint16_t Y() const { return y_; }
  uint16_t Width() const { return width_; }
  uint16_t Height() const { return height_; }
};




