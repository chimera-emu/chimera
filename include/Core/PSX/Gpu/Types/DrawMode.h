#include <Core/PSX/RegisterBase.h>

#include <cstdint>
#include <iosfwd>

enum class TexpageColours : uint32_t
{
  _4Bit    = 0x00,
  _8Bit    = 0x01,
  _15Bit   = 0x02,
  Reserved = 0x03
};

std::ostream& operator<<(std::ostream& os, const TexpageColours colours);

enum class SemiTransparency : uint32_t
{
  HBaddHF = 0x00,
  FBaddFF = 0x01,
  FBsubFF = 0x02,
  FBaddQF = 0x03
};

std::ostream& operator<<(std::ostream& os, const SemiTransparency value);

enum class DitherSetting : uint32_t
{
  Disabled = 0x00,
  Enabled  = 0x01
};

std::ostream& operator<<(std::ostream& os, const DitherSetting dither);

enum class DisplayAreaDraw : uint32_t
{
  Prohibited = 0x00,
  Allowed    = 0x01
};

std::ostream& operator<<(std::ostream& os, const DisplayAreaDraw drawArea);

enum class TextureDisabled : uint32_t
{
  Enabled  = 0x00,
  Disabled = 0x01
};

std::ostream& operator<<(std::ostream& os, const TextureDisabled textureState);

class DrawMode : public RegisterBase<DrawMode>
{
public:
  using RegisterBase::RegisterBase;

public:
  void Set(const uint32_t value)
  {
    data_ = value & 0x3FFFU;
  }

public:
  // TODO: What are these?
  uint32_t TexpageX() const { return (Range(0, 3) * 64U); }
  uint32_t TexpageY() const { return (Get(4) * 256U); }

  SemiTransparency GetSemiTransparency() const
  {
    return static_cast<SemiTransparency>(Range(5, 6));
  }

  TexpageColours GetTexpageColours() const
  {
    return static_cast<TexpageColours>(Range(7, 8));
  }

  DitherSetting GetDitherSetting() const
  {
    return static_cast<DitherSetting>(Test(9));
  }

  DisplayAreaDraw GetDisplayAreaDraw() const
  {
    return static_cast<DisplayAreaDraw>(Test(10));
  }

  TextureDisabled GetTextureDisabled() const
  {
    return static_cast<TextureDisabled>(Test(11));
  }

  bool TextureXFlipped() const { return Test(12); }
  bool TextureYFlipped() const { return Test(13); }

  uint32_t GetCommand() const { return Range(24, 31); }
};

std::ostream& operator<<(std::ostream& os, const DrawMode mode);
