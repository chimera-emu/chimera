#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>
#include <iosfwd>

enum class HRes : uint32_t
{
  x256 = 0x00,
  x320 = 0x01,
  x512 = 0x02,
  x640 = 0x03
};

std::ostream& operator<<(std::ostream& os, HRes res);

enum class VRes : uint32_t
{
  x240 = 0x00,
  x480 = 0x04
};

std::ostream& operator<<(std::ostream& os, VRes res);

enum class VideoMode : uint32_t
{
  NTSC = 0x00,
  PAL  = 0x08
};

std::ostream& operator<<(std::ostream& os, VideoMode mode);

enum class ColourDepth : uint32_t
{
  b15 = 0x00,
  b24 = 0x10
};

std::ostream& operator<<(std::ostream& os, ColourDepth mode);

enum class VerticalInterlace : uint32_t
{
  OFF = 0x00,
  ON  = 0x20
};

std::ostream& operator<<(std::ostream& os, VerticalInterlace interlace);

enum class HRes2 : uint32_t
{
  OFF  = 0x00,
  x368 = 0x40
};

std::ostream& operator<<(std::ostream& os, HRes2 hres);

class DisplayMode : public RegisterBase<DisplayMode>
{
public:
  using RegisterBase::RegisterBase;

public:
  // Only the first 8 bits are used, the rest are 0.
  void Set(const uint32_t value)
  {
    data_ = value & 0xFF;
  }

public:
  HRes GetHorizontalResolution() const
  {
    return static_cast<HRes>(Range(0, 1));
  }

  VRes GetVerticalResolution() const
  {
    return static_cast<VRes>(Test(2));
  }

  VideoMode GetVideoMode() const
  {
    return static_cast<VideoMode>(Test(3));
  }

  ColourDepth GetColourDepth() const
  {
    return static_cast<ColourDepth>(Test(4));
  }

  VerticalInterlace GetVerticalInterlace() const
  {
    return static_cast<VerticalInterlace>(Test(5));
  }

  HRes2 GetHorizontalResolutionExt() const
  {
    return static_cast<HRes2>(Test(6));
  }
};

std::ostream& operator<<(std::ostream& os, DisplayMode mode);
