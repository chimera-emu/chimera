#pragma once

#include <cstdint>
#include <iosfwd>

struct TextureWindow
{
  uint8_t maskX_;
  uint8_t maskY_;
  uint8_t offsetX_;
  uint8_t offsetY_;

public:
  TextureWindow() : maskX_{0}, maskY_{0}, offsetX_{0}, offsetY_{0} {}
};

std::ostream& operator<<(std::ostream& os, const TextureWindow& window);
