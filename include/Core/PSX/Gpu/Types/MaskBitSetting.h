#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

class MaskBitSetting : public RegisterBase<MaskBitSetting>
{
public:
  using RegisterBase::RegisterBase;

public:
  // Set mask bit when drawing to pixel
  // TODO: Poor naming, called set, but it's actually a getter
  bool SetMaskOnDraw() const { return Test(0); }
  // Don't overwrite pixels with mask bit set
  bool CheckMaskBeforeDraw() const { return Test(1); }
};

// TODO: wat
