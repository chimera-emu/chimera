#pragma once

#include <Core/PSX/Gpu/Types/DrawMode.h>
#include <Core/PSX/Gpu/Types/MaskBitSetting.h>

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

struct StatusRegister : public RegisterBase<StatusRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  void SetDrawModeFlags(const DrawMode mode)
  {
    // First 10 bits of this register map directly to the draw mode register.
    constexpr auto mask = 0x3FFU;

    data_ = (data_ & ~mask) | (mode.Get() & mask);
  }

  void SetMaskBitSettingFlags(const MaskBitSetting setting)
  {
    SetBit(11, setting.SetMaskOnDraw());
    SetBit(12, setting.CheckMaskBeforeDraw());
  }

public:


};

// GPU Status Register
//
// The GPU status register is mostly an aggregate of various flags and settings
// from other registers within the GPU (mainly the DrawMode register). It is not
// writeable, when a read comes into this I/O port we collate all the values
// into this register before reading.
//
