#pragma once

#include <cstdint>
#include <iosfwd>

// Start of Display area (VRAM)
struct DisplayArea
{
  uint16_t x_;
  uint16_t y_;

public:
  DisplayArea() : x_{0}, y_{0} {}
};

std::ostream& operator<<(std::ostream& os, const DisplayArea& area);
