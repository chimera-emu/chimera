#pragma once

#include <cstdint>
#include <iosfwd>

struct DrawArea
{
  uint32_t top_;
  uint32_t bottom_;
  uint32_t left_;
  uint32_t right_;

public:
  DrawArea() : top_(0), bottom_(0), left_(0), right_(0) {}
};

std::ostream& operator<<(std::ostream& os, const DrawArea& area);
