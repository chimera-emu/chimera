#pragma once

#include <cstdint>
#include <iosfwd>

enum class DmaDirection : uint32_t
{
  Off      = 0x00,
  Fifo     = 0x01,
  CpuToGP0 = 0x02,
  GpuToCpu = 0x03
};

std::ostream& operator<<(std::ostream& os, const DmaDirection direction);
