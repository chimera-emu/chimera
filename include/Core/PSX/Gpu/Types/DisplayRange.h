#pragma once

#include <cstdint>
#include <iosfwd>

struct DisplayRange
{
  uint16_t horizontalStart_;
  uint16_t horizontalEnd_;
  uint16_t verticalStart_;
  uint16_t verticalEnd_;

public:
  DisplayRange()
    : horizontalStart_(0), horizontalEnd_(0), verticalStart_(0),
      verticalEnd_(0) {}
};

std::ostream& operator<<(std::ostream& os, const DisplayRange& range);
