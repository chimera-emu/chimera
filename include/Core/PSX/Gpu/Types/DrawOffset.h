#pragma once

#include <cstdint>
#include <iosfwd>

struct DrawOffset
{
  int16_t x_;
  int16_t y_;

public:
  DrawOffset() : x_{0}, y_{0} {}
};

std::ostream& operator<<(std::ostream& os, const DrawOffset& offset);
