#pragma once

#include "Types/DisplayArea.h"
#include "Types/DisplayMode.h"
#include "Types/DisplayRange.h"
#include "Types/DmaDirection.h"
#include "Types/DrawArea.h"
#include "Types/DrawMode.h"
#include "Types/DrawOffset.h"
#include "Types/TextureWindow.h"

#include <cstdint>

class GpuState
{
private:
  DrawMode drawMode;
  DisplayMode displayMode;

  DmaDirection direction;

  DrawArea drawArea;

  DrawOffset drawOffset;

  TextureWindow textureWindow;

  bool texturePixel;
  bool preservePixel;

  DisplayArea displayArea;
  DisplayRange displayRange;

  bool displayEnabled;
  bool interrupt;

public:
  GpuState()
    : drawMode(), displayMode(), direction(DmaDirection::Off), drawArea(),
      drawOffset(), textureWindow(), texturePixel(false),
      preservePixel(false), displayArea(), displayRange(),
      displayEnabled(false), interrupt(false) {}

public:
  DrawArea GetDrawArea() const { return drawArea; }
  DisplayArea GetDisplayArea() const { return displayArea; }
  DrawMode GetDrawMode() const { return drawMode; }
  DisplayMode GetDisplayMode() const { return displayMode; }
  DrawOffset GetDrawOffset() const { return drawOffset; }
  DisplayRange GetDisplayRange() const { return displayRange; }
  DmaDirection GetDmaDirection() const { return direction; }
  TextureWindow GetTextureWindow() const { return textureWindow; }

public:
  void Reset();
  void ResetInterrupt();

  void SetDrawMode(const uint32_t value) { drawMode.Set(value); }
  void SetDisplayMode(const uint32_t value) { displayMode.Set(value); }
  void SetMaskBit(const bool texture, const bool preserve);
  void SetDmaDirection(const uint32_t value);
  void SetDrawAreaTopLeft(const uint32_t top, const uint32_t left);
  void SetDisplayArea(const uint16_t startX, const uint16_t startY);
  void SetDrawAreaBottomRight(const uint32_t bottom, const uint32_t right);
  void SetDrawOffset(const int16_t offsetX, const int16_t offsetY);
  void SetHorizontalRange(const uint16_t start, const uint16_t end);
  void SetVerticalRange(const uint16_t start, const uint16_t end);
  void SetDisplayEnabled(const bool enabled);
  void SetTextureWindow(const uint8_t maskX, const uint8_t maskY,
                        const uint8_t offsetX, const uint8_t offsetY);
};
