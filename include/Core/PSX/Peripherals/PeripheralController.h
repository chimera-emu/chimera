#pragma once

#include <Core/PSX/Memory/MMIOInterface.h>

#include <cstdint>

class PeripheralController : public MMIOInterface
{
public:
  PeripheralController() {}

public:
  void HandleWrite(const uint32_t address, const uint32_t value) override;

private:
  void WriteJoypadData(const uint32_t value);
  void WriteJoypadStatus(const uint32_t value);
  void WriteJoypadMode(const uint32_t value);
  void WriteJoypadControl(const uint32_t value);
  void WriteJoypadBaudrate(const uint32_t value);

public:
  uint32_t HandleRead(const uint32_t address) const override;

private:
  uint32_t ReadJoypadData() const;
  uint32_t ReadJoypadStatus() const;
  uint32_t ReadJoypadMode() const;
  uint32_t ReadJoypadControl() const;
  uint32_t ReadJoypadBaudrate() const;
};
