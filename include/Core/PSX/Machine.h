#pragma once

#include <Core/PSX/Cdrom/CdromController.h>
#include <Core/PSX/Cpu/Processor.h>
#include <Core/PSX/Dma/DmaController.h>
#include <Core/PSX/Gpu/GpuController.h>
#include <Core/PSX/Interrupts/InterruptController.h>
#include <Core/PSX/Memory/MemoryController.h>
#include <Core/PSX/Peripherals/PeripheralController.h>
#include <Core/PSX/Spu/SpuController.h>
#include <Core/PSX/Timers/TimerController.h>

#include <LibGraphics/Window/WindowHandle.h>

#include <memory>
#include <string>

class Machine
{
private:
  // MMIO registers containing status and masks of interrupts within the PSX.
  // TODO: Consider renaming it to something more accurate
  // TODO: Consider storing/handling it in a more sensible way.
  std::unique_ptr<Interrupts::InterruptController> interrupts;
  std::unique_ptr<Timers::TimerController> timers;

  std::unique_ptr<Memory::MemoryController> memory_;

  std::unique_ptr<Cpu::Processor> processor_;

  std::unique_ptr<Dma::DmaController> dma_;
  std::unique_ptr<Spu::SpuController> spu_;
  std::unique_ptr<Cdrom::CdromController> cdrom_;

  std::unique_ptr<Gpu::GpuController> gpu_;

  // TODO: Probably bad naming, buts it's short...
  PeripheralController ports;

public:
  explicit Machine(const std::string& biospath);

public:
  void Run();


private:
  void Initialise(const std::string& biospath);
  void InitialiseLogger();
  void InitialiseMemory(const std::string& biospath);
};



