#pragma once

#include <Core/PSX/Dma/ChannelIndex.h>
#include <Core/PSX/Dma/Types/BlockControl.h>
#include <Core/PSX/Dma/Types/ChannelControl.h>

#include <cstdint>

namespace Dma
{

class Channel
{
private:
  // Pointer to virtual address to read/write
  uint32_t baseAddress;
  // Number of blocks and size of blocks to transfer
  BlockControl blockControl;
  // Mode of transport etc.
  ChannelControl channelControl;

public:
  Channel() : baseAddress(0), blockControl(0), channelControl(0) {}

public:
  uint32_t GetBaseAddress() const { return baseAddress; }

  BlockControl& GetBlockControl() { return blockControl; }
  const BlockControl& GetBlockControl() const { return blockControl; }

  ChannelControl& GetChannelControl() { return channelControl; }
  const ChannelControl& GetChannelControl() const { return channelControl; }

  void SetBaseAddress(const uint32_t value) { baseAddress = value; }
  void SetBlockControl(const uint32_t value) { blockControl.Set(value); }
  void SetChannelControl(const uint32_t value) { channelControl.Set(value); }
};

}  // namespace Dma
