#pragma once

#include <Core/PSX/Dma/Channel.h>
#include <Core/PSX/Dma/Types/ControlRegister.h>
#include <Core/PSX/Dma/Types/InterruptRegister.h>

#include <Core/PSX/Memory/MMIOInterface.h>

#include <array>
#include <cstdint>

namespace Memory { class MemoryController; }
namespace Interrupts { class InterruptController; }

namespace Dma
{

class DmaController : public MMIOInterface
{
private:
  // DMA Primary Control Register
  ControlRegister control_;
  // DMA Interrupt Register
  InterruptRegister interrupt_;
  // DMA Channels
  std::array<Channel, 7> channels_;

  // Pointers to access/manipulate external controllers.
  Interrupts::InterruptController* interrupts_;
  Memory::MemoryController* memory_;

public:
  DmaController(Interrupts::InterruptController* interrupts,
                Memory::MemoryController* memory);


public:
  // MMIOInterface override
  void HandleWrite(const uint32_t address, const uint32_t value) override;


public:
  // MMIOInterface override
  uint32_t HandleRead(const uint32_t address) const override;


private:
  // Handle write directed at DMA control/interrupt registers
  void WriteControlData(const uint32_t address, const uint32_t value);
  // Handle write directed at the channel registers
  void WriteChannelData(const uint32_t address, const uint32_t value);

  void SetControl(const uint32_t value);
  void SetInterrupt(const uint32_t value);

  // Initialises a data transfer inside the DMA by calling either TransferBlock
  // or TransferList, depending on the SyncMode of the channel. Called if a
  // write enables a channel control register 'Active' bit flag
  void ExecuteTransfer(const uint32_t channel);

  // Transfers a contiguous block of data across the DMA.
  void TransferBlock(const uint32_t channel);
  // Transfers a list of data entries across the DMA until and marker is found
  void TransferList(const uint32_t channel);

  // TODO: What does this do?
  void FinaliseTransfer(const uint32_t channel);


private:
  // Handle read directed at DMA control/interrupt registers
  uint32_t ReadControlData(const uint32_t address) const;
  // Handle read directed at the channel registers
  uint32_t ReadChannelData(const uint32_t address) const;
};

}  // namespace DMA
