#pragma once

#include <cstdint>

enum class ChannelIndex
{
  MDEC_IN  = 0x00,
  MDEC_OUT = 0x01,
  GPU      = 0x02,
  CDROM    = 0x03,
  SPU      = 0x04,
  PIO      = 0x05,
  OTC      = 0x06
};

enum class Step : uint32_t
{
  Forward  = 0x0,
  Backward = 0x1
};

enum class Direction : uint32_t
{
  ToRam   = 0x0,
  FromRam = 0x1
};

enum class SyncMode : uint32_t
{
  Immediate   = 0x0,
  OnRequest   = 0x1,
  LinkedList  = 0x2,
  Reserved    = 0x3
};
