#pragma once

#include <Core/PSX/RegisterBase.h>

namespace Dma
{

class BlockControl : public RegisterBase<BlockControl>
{
public:
  using RegisterBase::RegisterBase;

public:
  uint32_t GetNumWords()    const { return Range( 0, 15); } // SYNC_MODE=0
  uint32_t GetBlockSize()   const { return Range( 0, 15); } // SYNC_MODE=1
  uint32_t GetBlockAmount() const { return Range(15, 31); }
};

}  // namespace Dma

// DMA Block Control Register
//
// TODO: Fill in information relating to this register
//
