#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

namespace Dma
{

class ChannelControl : public RegisterBase<ChannelControl>
{
public:
  using RegisterBase::RegisterBase;

public:
  Direction GetDirection() const { return Direction(Test(0)); }
  Step GetStep() const { return Step(Test(1)); }
  SyncMode GetSyncMode() const { return SyncMode(Range(9, 10)); }

  bool IsChopping() const { return Test( 8); }
  bool IsActive()   const { return Test(24); }
  bool IsTrigger()  const { return Test(28); }
  bool IsPaused()   const { return Test(29); }

  // TODO: Docs suggest this is the magnitude of the window size, check.
  uint32_t GetDmaWindowSize() const { return Range(16, 18); }
  uint32_t GetCpuWindowSize() const { return Range(20, 22); }

public:
  int32_t GetStepValue() const { return GetStep() == Step::Forward ? 4 : -4; }

  // TODO: Explain
  void FinaliseTransfer() { data_ &= (~0x11000000); }
};

}  // namespace Dma

// DMA Channel Control Register
//
// TODO: Fill in information relating to this register
//
