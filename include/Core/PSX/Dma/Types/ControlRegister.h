#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

namespace Dma
{

class ControlRegister : public RegisterBase<ControlRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  bool IsChannelEnabled(const ChannelIndex index) const
  {
    const auto shift = 3 + (4 * static_cast<uint32_t>(index));

    return Test(shift);
  }

  uint32_t GetChannelPriority(const ChannelIndex index) const
  {
    const auto shift = (4 * static_cast<uint32_t>(index));

    return Range(shift, shift + 3);
  }
};

}  // namespace Dma

// TODO: Clarify
//
// DMA Control Register
//
// Provides functions to access the contents of the control register.
//
// The control register contains bit fields describing the priority of each
// channel and whether or not the channel is enabled.
//
// The format for the register is as follows:
//
//  +--+--+--+--+--+--+--+--+--+--+
//  | ... | 7| 6| 5| 4| 3| 2| 1| 0|
//  +--+--+--+--+--+--+--+--+--+--+
//  | ... |En|  Prio  |En|  Prio  |
//  +--+--+--+--+--+--+--+--+--+--+
//  | ... | Channel 1 | Channel 0 |
//  +--+--+--+--+--+--+--+--+--+--+
//
// Each channel has a 3 bit priority value where 0 is highest and 7 is lowest.
// If channels have the same priority the lowest channel (by index) is selected.
//
