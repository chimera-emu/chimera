#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

namespace Dma
{

class InterruptRegister : public RegisterBase<InterruptRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  inline bool IsChannelEnabled(const ChannelIndex channel) const
  {
    return Test(16 + static_cast<uint32_t>(channel));
  }

  inline bool IsChannelFlagSet(const ChannelIndex channel) const
  {
    return Test(24 + static_cast<uint32_t>(channel));
  }

  inline bool IsForceIRQSet()      const { return Test(15); }
  inline bool IsIRQMasterEnabled() const { return Test(23); }
  inline bool IsIRQMasterFlagSet() const { return Test(31); }

public:
  inline void SetChannelFlag(const ChannelIndex channel)
  {
    SetBit(24 + static_cast<uint32_t>(channel));
  }

public:
  inline uint32_t GetChannelEnables() const { return (data_ >> 16) & 0x7F; }
  inline uint32_t GetChannelFlags()   const { return (data_ >> 24) & 0x7F; }

  inline bool IsInterruptPending() const
  {
    if (IsForceIRQSet())
      return true;

    const bool pending = (GetChannelEnables() | GetChannelFlags()) != 0;

    return (pending && IsIRQMasterEnabled());
  }
};

}  // namespace Dma
