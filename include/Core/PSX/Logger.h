#pragma once

#include <spdlog/spdlog.h>

// TODO: Define a proper logging interface
inline spdlog::logger* Logger()
{
  static spdlog::logger* logger = spdlog::stdout_color_mt("CPSX").get();

  return logger;
}
