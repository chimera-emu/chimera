#pragma once

#include <Core/PSX/Machine.h>
#include <LibGraphics/Window/WindowHandle.h>

#include <memory>
#include <string>
#include <thread>

class Emulator
{
private:
  std::string bios_;
  WindowHandle handle_;

  std::unique_ptr<Machine> machine_;

  std::thread main_thread_;

public:
  Emulator(const std::string& bios, const WindowHandle handle);

private:
  void Initialise();
  void Run();
  void Shutdown() {}
};
