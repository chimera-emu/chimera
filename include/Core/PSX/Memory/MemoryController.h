#pragma once

#include <Core/PSX/Memory/MemoryRegion.h>

#include <cstdint>
#include <iosfwd>
#include <vector>

namespace Memory
{

class MemoryController
{
private:
  std::vector<MemoryRegion> regions_;

public:
  MemoryRegion& GetAddressRegion(const uint32_t addr);

public:
  template <class Type>
  Type Read(const uint32_t addr);

  template <class Type>
  void Write(const uint32_t addr, const Type value);

  void AddMemoryRegion(const std::string& name, const uint32_t addr,
                       const uint32_t size);

  void AddMemoryRegion(MemoryRegion&& region);

  void LoadMemoryRegion(const uint32_t addr, const std::vector<uint8_t>& data);

public:
  const std::vector<MemoryRegion>& Regions() const { return regions_; }
};

std::ostream& operator<<(std::ostream& os, const MemoryController& controller);

} // namespace Memory

// Memory Controller
//
// Represents the address space of a machine as a collection of memory regions.
//
// For most machines and devices, not all of the 32-bit address space they use
// is utilized. It would be unwise to allocate memory for the whole address
// space (~4gb) when most devices emulated only use a small fraction of that.
//
// The memory bus holds a collection of memory regions which represent ranges of
// memory within the address space that are actually used by the machine. Memory
// access is handled by passing any read/write calls to the memory region
// responsible for the given region.
//
// TODO: When adding a new region to the address space we should check that then
//       new region does not overlap with any existing ones, panic if it does.
//
// TODO: The representation of memory regions as a vector of individual regions
//       does not give very efficient lookup speed.
//
//       To lookup the memory region, we need to access we must linearly iterate
//       through each region in the vector. For each region, we call 'Contains'
//       to check if the given address lies within a regions range. Given that
//       there are a number of very small regions towards the end of the address
//       space hitting the correct region could take a 'large' amount of time.
//
//       Find a better structure to store regions, maybe some kind of tree, that
//       gives better lookup performance.
//
