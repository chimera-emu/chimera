#pragma once

#include <Core/PSX/Memory/MMIOInterface.h>

#include <cstdint>
#include <functional>
#include <iosfwd>
#include <string>
#include <vector>

namespace Memory
{

class MemoryRegion
{
public:
  using WriteCallback = std::function<void(uint32_t, uint32_t)>;
  using ReadCallback  = std::function<uint32_t(uint32_t)>;

private:
  // Name of the region for debugging purposes
  const std::string name_;
  // Base address of the memory region
  const uint32_t address_;
  // Size of the memory region in bytes
  const uint32_t size_;
  // Byte vector storing the data for this region. This could potentially be an
  // array as the size is defined at compile time.
  std::vector<uint8_t> data_;

  // Function pointers to an MMIO interface's memory read/write callbacks
  WriteCallback writeCallback_;
  ReadCallback readCallback_;

public:
 MemoryRegion(std::string name, const uint32_t address, const uint32_t size)
     : name_(std::move(name)), address_(address), size_(size),
       data_(std::vector<uint8_t>(size)), writeCallback_(nullptr),
       readCallback_(nullptr) {}

 MemoryRegion(std::string name, const uint32_t address, const uint32_t size,
              WriteCallback writeCallback, ReadCallback readCallback)
     : name_(std::move(name)), address_(address), size_(size),
       data_(std::vector<uint8_t>(0)), writeCallback_(std::move(writeCallback)),
       readCallback_(std::move(readCallback)) {}

 MemoryRegion(std::string name, const uint32_t address, const uint32_t size,
              MMIOInterface& mmioInterface);

public:
  std::string Name() const { return name_; }
  uint32_t Address() const { return address_; }
  uint32_t Size() const { return size_; }

  // Does the given address exist within this memory region's range?
  bool Contains(const uint32_t address) const;

  template <class Type>
  Type Read(const uint32_t address);

public:
  void Load(std::vector<uint8_t> input);

  template <class Type>
  void Write(const uint32_t address, const Type value);

};

std::ostream& operator<<(std::ostream& os, const MemoryRegion& region);

} // namespace Memory

// Memory Region
//
// Represents a range of memory within the machine's address space.
//
// The memory region is currently little-endian, any reads and write calls using
// data larger than a byte will be arranged in little-endian order as that's
// currently what is used by the target architecture (PSX).
//
// For raw memory regions (e.g. RAM/BIOS) data is stored in the memory region
// as a vector of bytes. Memory is indexed as an offset from the memory region's
// base address.
//
// For MMIO regions (e.g. GPU/SPU/CD-ROM registers) the memory access is handled
// by the MMIO component's read/write handling functions, stored within the
// memory region as function pointers.
//
// TODO: Currently the memory region is little-endian. At some point, this
//       should support big-endian functionality so it's compatible with
//       different architectures.
//
// TODO: The 'Contain' function could be optimised, currently it computes the
//       end of the region on every call, it could just be stored as a member
//       variable during construction. The benefit needs benchmarking first.
//
// TODO: Consider subclassing the memory region for raw and MMIO type regions
//       instead of representing both using the same class.
//
// TODO: There should be some way of discriminating between Raw and MMIO regions
//       other than checking if the function pointers are null.
//
// TODO: Currently we assume that any memory write/read is to an address that is
//       valid within the region
//
// TODO: The memory address of any read/write to the region is assumed to be
//       valid. For now this is fine because the region is fetched from the
//       memory bus and accessed based on it passing the 'Contains' predicate
//       for the given address. This means we're currently depending on an
//       external class we have no control over here to ensure safe memory
//       access. It should be handled here without impacting performance in case
//       regions need to be used somewhere other than the memory bus class.
//
