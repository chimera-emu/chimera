#pragma once

#include <cstdint>

class MMIOInterface
{
public:
  virtual ~MMIOInterface() {}

public:
  virtual void HandleWrite(const uint32_t address, const uint32_t value) = 0;

public:
  virtual uint32_t HandleRead(const uint32_t address) const = 0;
};
