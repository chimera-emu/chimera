#pragma once

#include <Core/PSX/Memory/MMIOInterface.h>

#include <cstdint>

namespace Spu
{

class SpuController : public MMIOInterface
{
public:
  SpuController() {}

public:
  void HandleWrite(const uint32_t address, const uint32_t value) override
  {
    (void) address;
    (void) value;
  }

public:
  uint32_t HandleRead(const uint32_t address) const override
  {
    (void) address;

    return 0;
  }
};

}  // namespace Spu
