#pragma once

#include <fmt/format.h>

#include <stdexcept>

class PanicException : public std::runtime_error
{
public:
  PanicException() : std::runtime_error("Panic occurred") {}
};

inline void panic_internal(const char* message, fmt::ArgList args)
{
  fmt::print(message, args);

  std::terminate();
}
FMT_VARIADIC(void, panic_internal, const char *)

inline void panic_unreachable_internal(const char* message, fmt::ArgList args)
{
  fmt::print("Unreachable code was executed.");

  panic_internal(message, args);
}
FMT_VARIADIC(void, panic_unreachable_internal, const char *)

// TODO: Probably don't export all these macros in this header
// TODO: Probably find a less messy way of handling this
#define FIRST(...) FIRST_HELPER(__VA_ARGS__, throwaway)
#define FIRST_HELPER(first, ...) first

#define REST(...) REST_HELPER(NUM(__VA_ARGS__), __VA_ARGS__)
#define REST_HELPER(qty, ...) REST_HELPER2(qty, __VA_ARGS__)
#define REST_HELPER2(qty, ...) REST_HELPER_##qty(__VA_ARGS__)
#define REST_HELPER_ONE(first)
#define REST_HELPER_TWOORMORE(first, ...) , __VA_ARGS__
#define NUM(...) \
    SELECT_10TH(__VA_ARGS__, TWOORMORE, TWOORMORE, TWOORMORE, TWOORMORE,\
                TWOORMORE, TWOORMORE, TWOORMORE, TWOORMORE, ONE, throwaway)
#define SELECT_10TH(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, ...) a10


// HACK: The exception being thrown here is never hit. It is used to suppress
//       compiler warnings when putting an unreachable panic at the end of
//       non-void function without specifying a return value. There needs to be
//       a better way of handling this without exceptions.
#define panic(...)                                      \
{                                                       \
  panic_internal(FIRST(__VA_ARGS__) REST(__VA_ARGS__)); \
  throw PanicException();                               \
}

#define panic_unreachable(...)                                      \
{                                                                   \
  panic_unreachable_internal(FIRST(__VA_ARGS__) REST(__VA_ARGS__)); \
  throw PanicException();                                           \
}
