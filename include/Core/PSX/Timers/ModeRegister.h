#pragma once

#include <Core/PSX/Timers/ClockSource.h>
#include <Core/PSX/Timers/TimerIndex.h>

#include <Core/PSX/RegisterBase.h>

#include <array>
#include <cstdint>
#include <ostream>

namespace Timers
{

// TODO: Could be a uint8_t, but really, does it matter?
// TODO: Move these to separate files.
enum class ResetMode : uint32_t
{
  AfterMax    = 0x00,
  AfterTarget = 0x01
};

inline std::ostream& operator<<(std::ostream& os, const ResetMode mode)
{
  switch (mode)
  {
    case ResetMode::AfterMax:    { return os << "After Max";    }
    case ResetMode::AfterTarget: { return os << "After Target"; }
  }

  return os;
}

enum class RepeatMode : uint32_t
{
  Once   = 0x00,
  Repeat = 0x01
};

inline std::ostream& operator<<(std::ostream& os, const RepeatMode mode)
{
  switch (mode)
  {
    case RepeatMode::Once:   { return os << "Once";   }
    case RepeatMode::Repeat: { return os << "Repeat"; }
  }

  return os;
}

enum class ToggleMode : uint32_t
{
  Pulse  = 0x00,
  Toggle = 0x01
};

inline std::ostream& operator<<(std::ostream& os, const ToggleMode mode)
{
  switch (mode)
  {
    case ToggleMode::Pulse:  { return os << "Pulse";  }
    case ToggleMode::Toggle: { return os << "Toggle"; }
  }

  return os;
}

class ModeRegister : public RegisterBase<ModeRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  uint32_t Get() const { return data_; }

public:
  bool SyncEnabled() const { return Test(0); }

  // TODO: This would be better served as an enum, but it seems the mode means
  //       different things for different counters
  uint32_t SyncMode() const { return Range(1, 2); }

  ResetMode ResetEvent() const { return ResetMode(Test(3));}

  // TODO: Phrasing. "Trigger interrupt when counter == target"?
  bool TargetIRQEnabled() const { return Test(4); }
  // TODO: Phrasing. "Trigger interrupt when counter == 0xFFFF"?
  bool MaxIRQEnabled() const { return Test(5); }

  RepeatMode GetRepeatMode() const { return RepeatMode(Test(6)); }
  ToggleMode GetToggleMode() const { return ToggleMode(Test(7)); }

  // TODO: This would be better served as an enum, but it seems the mode means
  //       different things for different counters
  uint32_t GetSource() const { return Range(8, 9); }

  ClockSource GetClockSource(const TimerIndex index) const
  {
    using S = ClockSource;

    static constexpr std::array<std::array<ClockSource, 4>, 3> lookup =
    {{
      {S::SystemClock, S::GpuDotClock,     S::SystemClock,     S::GpuDotClock},
      {S::SystemClock,    S::GpuHSync,     S::SystemClock,        S::GpuHSync},
      {S::SystemClock, S::SystemClock, S::SystemClockDiv8, S::SystemClockDiv8},
    }};

    return lookup[static_cast<uint32_t>(index)][GetSource()];
  }

  bool InterruptRequested() const { return Test(10); }
  bool TargetValueReached() const { return Test(11); }
  bool MaxValueReached() const { return Test(12); }
};


inline std::ostream& operator<<(std::ostream& os, const ModeRegister mode)
{
  os << std::boolalpha
     << "Synchronisation Enabled : " << mode.SyncEnabled()        << "\n"
     << "Synchronisation Mode    : " << mode.SyncMode()           << "\n"
     << "Counter Reset Mode      : " << mode.ResetEvent()         << "\n"
     << "IRQ when counter=Target : " << mode.TargetIRQEnabled()   << "\n"
     << "IRQ when counter=0xFFFF : " << mode.MaxIRQEnabled()      << "\n"
     << "Repeat Mode             : " << mode.GetRepeatMode()      << "\n"
     << "Toggle Mode             : " << mode.GetToggleMode()      << "\n"
     << "Clock Source            : " << mode.GetSource()          << "\n"
     << "Is Interrupt Requested  : " << mode.InterruptRequested() << "\n"
     << "Target Value Reached    : " << mode.TargetValueReached() << "\n"
     << "Max Value Reached       : " << mode.MaxValueReached()    << "\n";

  return os;
}

} // namespace Timers
