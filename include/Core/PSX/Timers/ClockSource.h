#pragma once

#include <cstdint>
#include <ostream>

namespace Timers
{

enum class ClockSource : uint32_t
{
  SystemClock,
  SystemClockDiv8,
  GpuDotClock,
  GpuHSync
};

inline std::ostream& operator<<(std::ostream& os, const ClockSource source)
{
  switch (source)
  {
    case ClockSource::SystemClock:     { return os << "System Clock";        }
    case ClockSource::SystemClockDiv8: { return os << "System Clock/8";      }
    case ClockSource::GpuDotClock:     { return os << "GPU Dot Clock";       }
    case ClockSource::GpuHSync:        { return os << "GPU Horizontal Sync"; }
  }

  return os;
}

}
