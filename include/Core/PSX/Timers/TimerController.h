#pragma once

#include <Core/PSX/Timers/RootCounter.h>

#include <Core/PSX/Memory/MMIOInterface.h>

#include <array>
#include <cstdint>

namespace Timers
{

class TimerController : public MMIOInterface
{
private:
  std::array<RootCounter, 3> counters_;

public:
 TimerController();

public:
  void HandleWrite(const uint32_t address, const uint32_t value) override;

public:
  uint32_t HandleRead(const uint32_t address) const override;
};

}
