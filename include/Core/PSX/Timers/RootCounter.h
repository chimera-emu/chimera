#pragma once

#include <Core/PSX/Timers/ModeRegister.h>
#include <Core/PSX/Timers/TimerIndex.h>

#include <cstdint>
#include <ostream>

namespace Timers
{

class RootCounter
{
private:
  // Root counter indentifier, used to determine clock source
  const TimerIndex index_;
  // Current counter value (incrementing)
  uint32_t current_;
  // Target counter value
  uint32_t target_;
  // Register containing root counter configuration parameters
  ModeRegister mode_;

public:
  explicit RootCounter(const TimerIndex index)
      : index_(index), current_(0), target_(0), mode_(0) {}

public:
  void SetCurrent(const uint32_t value) { current_ = value; }
  void SetTarget(const uint32_t value) { target_ = value; }
  void SetMode(const uint32_t value) { mode_.Set(value); }

public:
  TimerIndex Index() const { return index_; }
  uint32_t Current() const { return current_; }
  uint32_t Target() const { return target_; }
  const ModeRegister& Mode() const { return mode_; }

  ClockSource GetClockSource() const
  {
    return mode_.GetClockSource(index_);
  }

};

inline std::ostream& operator<<(std::ostream& os, const RootCounter counter)
{
  os << "Root Counter:\n"
     << "Identifier    : " << counter.Index()          << "\n"
     << "Clock Source  : " << counter.GetClockSource() << "\n"
     << "Current Value : " << counter.Current()        << "\n"
     << "Target Value  : " << counter.Target()         << "\n"
     << "Counter Mode:\n"  << counter.Mode()           << "\n";

  return os;
}

} // namespace Timers

// Root Counter
//
//
