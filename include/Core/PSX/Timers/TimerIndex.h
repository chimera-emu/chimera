#pragma once

#include <cstdint>
#include <ostream>

namespace Timers
{

enum class TimerIndex : uint32_t
{
  Timer0,
  Timer1,
  Timer2
};

inline std::ostream& operator<<(std::ostream& os, const TimerIndex index)
{
  switch (index)
  {
    case TimerIndex::Timer0: { return os << "Timer 0"; }
    case TimerIndex::Timer1: { return os << "Timer 1"; }
    case TimerIndex::Timer2: { return os << "Timer 2"; }
  }

  return os;
}

}
