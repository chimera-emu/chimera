#pragma once

#include <cstdint>
#include <limits>

template <class Derived, class DataType = uint32_t>
class RegisterBase
{
protected:
  DataType data_;

public:
  RegisterBase() : data_(0) {}

  explicit RegisterBase(const DataType value) : data_(value) {}

public:
  inline void Set(const DataType value) { data_ = value; }

  // Flips all bits in the register
  inline void Flip() { data_ = ~data_; }

  // Toggle bit at given position
  inline void Flip(const uint32_t index)
  {
    data_ ^= (DataType(1) << index);
  }

  inline void SetBit(const uint32_t index, const bool enable = true)
  {
    enable ? Enable(index) : Clear(index);
  }

  // Set all bits in register to 1
  inline void Enable()
  {
    data_ = ~DataType(0U);
  }

  // Set bit at given position to 1
  inline void Enable(const uint32_t index)
  {
    data_ |= (DataType(1) << index);
  }

  // Set all bits in register to 0
  inline void Clear() const
  {
    data_ = DataType(0);
  }

  // Set bit at given position to 0
  inline void Clear(const uint32_t index)
  {
    data_ &= ~(DataType(1) << index);
  }

public:
  // Return data stored in this register
  inline DataType Get() const { return data_; }

  // Returns the value of the bit at the given position as an integer
  inline uint32_t Get(const uint32_t index) const
  {
    return Test(index) ? 1U : 0U;
  }

  // Returns the value of the bit at the given position
  inline bool Test(const uint32_t index) const
  {
    return (data_ & (DataType(1) << index)) != 0;
  }

  // Checks if all of the bits are set
  inline bool All() const
  {
    return (std::numeric_limits<DataType>::max)() == data_;
  }

  // Checks if none of the bits are set
  inline bool None() const { return (DataType(0) == data_); }

  // Checks if any of the bits in the register are set
  inline bool Any() const { return (DataType(0) != data_); }

  // Returns a range of bits between positions lsb and msb, aligned to 0
  // TODO: This doesn't work if you try to get the full range, but why aren't
  //       you using Get()
  inline DataType Range(const uint32_t lsb, const uint32_t msb) const
  {
    return ((data_ >> lsb) & ~(~DataType(0) << (msb - lsb + 1)));
  }

  constexpr inline std::size_t Size() const
  {
    return (sizeof(DataType) * 8);
  }

public:
  static_assert(std::numeric_limits<DataType>::is_integer,
                "Register must have an integral data type");

  static_assert(!std::numeric_limits<DataType>::is_signed,
                "Register must have an unsigned data type");

};

// RegisterBase
//
// Acts as a CRTP base class for 32bit registers.
//
// Currently it does nothing more than provide some bit manipulation functions
// that could probably just be macros.
//
