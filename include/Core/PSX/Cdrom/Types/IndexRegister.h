#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

namespace Cdrom
{

class IndexRegister : public RegisterBase<IndexRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  // Only bits[0:1] are writeable
  void Set(const uint32_t value) { data_ = (value & 0x03); }

  void SetAdpcmFifoEmpty(const bool isEmpty)     { SetBit(2, !isEmpty); }
  void SetParameterFifoEmpty(const bool isEmpty) { SetBit(3,  isEmpty); }
  void SetParameterFifoFull(const bool isFull)   { SetBit(4,  !isFull); }
  void SetResponseFifoEmpty(const bool isEmpty)  { SetBit(5, !isEmpty); }
  void SetDataFifoEmpty(const bool isEmpty)      { SetBit(6, !isEmpty); }
  void SetTransmissionBusy(const bool isBusy)    { SetBit(7,   isBusy); }

public:
  uint32_t GetCdromIndex() const { return (data_ & 0x03); }

  bool IsAdpcmFifoEmpty()     const { return !Test(2); } // 0 == Empty
  bool IsParameterFifoEmpty() const { return  Test(3); } // 1 == Empty
  bool IsParameterFifoFull()  const { return !Test(4); } // 0 == Full
  bool IsResponseFifoEmpty()  const { return !Test(5); } // 0 == Empty
  bool IsDataFifoEmpty()      const { return !Test(6); } // 0 == Empty
  bool IsTransmissionBusy()   const { return  Test(7); } // 1 == Busy
};

}  // namespace Cdrom

// Index Register
//
// Describes the state of operation the CdromController is in
//
// TODO: Fill me in
//
