#pragma once

#include <array>
#include <cstdint>

namespace Cdrom
{

class Fifo
{
public:
  // TODO: Make this a template parameter?
  using Type = uint8_t;

private:
  std::array<Type, 16> buffer_;

  Type writeIndex_;
  Type readIndex_;

public:
  Fifo() : buffer_({0}), writeIndex_(0), readIndex_(0) {}

public:
  // Empty the contents of the FIFO buffer and reset the indexes
  void clear();

  // Push an item into the buffer
  void push_back(const Type value);

  // Return the element at the front of the buffer and increment the read index
  Type pop_back();


public:
  Type length() const;
  bool empty() const;
  bool full() const;
};

}  // namespace Cdrom
