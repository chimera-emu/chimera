#pragma once

#include <Core/PSX/Cdrom/Types/Fifo.h>
#include <Core/PSX/Cdrom/Types/IndexRegister.h>
#include <Core/PSX/Memory/MMIOInterface.h>

#include <cstdint>
#include <exception>

namespace Interrupts { class InterruptController; }

namespace Cdrom
{

class CdromController : public MMIOInterface
{
private:
  IndexRegister index_;

  // TODO: Move this to a separate class?
  uint8_t interruptFlags_;
  uint8_t interruptMask_;

  Fifo parameters_;
  Fifo response_;

  // Pointers to access/manipulate external controllers.
  Interrupts::InterruptController* interrupts_;


public:
  explicit CdromController(Interrupts::InterruptController* interrupts);


public:
  // MMIO interface override
  void HandleWrite(const uint32_t address, const uint32_t value) override;


public:
  // HACK: Resolve this when finishing the MMIO base class, it's just... no
  // MMIO interface override
  uint32_t HandleRead(const uint32_t address) const override;
  uint8_t HandleRead(const uint32_t address);


private:
  // Write functions for each of the MMIO registers
  void HandleGP0Write(const uint8_t value);
  void HandleGP1Write(const uint8_t value);
  void HandleGP2Write(const uint8_t value);
  void HandleGP3Write(const uint8_t value);

  // MMIO address functionality depends on index value stored in the index
  // register. Dispatch write functions depending on it's value.
  void HandleGP1WriteIndex0(const uint8_t value);
  void HandleGP2WriteIndex0(const uint8_t value);
  void HandleGP2WriteIndex1(const uint8_t value);
  void HandleGP3WriteIndex1(const uint8_t value);

  // CD-ROM command functions
  void CommandGetStat(); // 0x01
  void CommandTest();    // 0x19

  // CD-ROM sub functions for the 'Test' command
  void TestVersion();    // 0x20


  // TODO: This can be moved into a seperate class along with the interrupt
  //       flag/mask member data as it shares some commonality with interrupts
  //       in other controllers.
  void ConfigureInterrupt(const uint8_t code);


private:
  // Read functions for each of the MMIO registers
  uint8_t HandleGP0Read();
  uint8_t HandleGP1Read();
  uint8_t HandleGP2Read();
  uint8_t HandleGP3Read();

  // MMIO address functionality depends on index value stored in the index
  // register. Dispatch read functions depending on it's value.
  uint8_t HandleGP1ReadIndex1();


  bool IsInterruptPending() const;
};

}  // namespace Cdrom
