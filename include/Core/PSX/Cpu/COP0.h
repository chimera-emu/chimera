#pragma once

#include <Core/PSX/Cpu/Types/CauseRegister.h>
#include <Core/PSX/Cpu/Types/ExceptionCode.h>
#include <Core/PSX/Cpu/Types/StatusRegister.h>

#include <array>
#include <cstdint>

namespace Interrupts { class InterruptController; }

namespace Cpu
{

class COP0
{
private:
  Interrupts::InterruptController* interrupts_;

  std::array<uint32_t, 32> registers_;

  // TODO: These are already represented by the register array.
  StatusRegister status_;
  CauseRegister cause_;

public:
  explicit COP0(Interrupts::InterruptController* interrupts);

private:
  void SetEPCRegister(const uint32_t value) { registers_[14] = value; }

public:
  void HandleWrite(const uint32_t index, const uint32_t value);

  uint32_t HandleRead(const uint32_t index) const;

public:
  void ReturnFromException();

  // Set correct state in registers with the COP0 and return the address to the
  // exception handling code
  uint32_t EnterException(const ExceptionCode code, const uint32_t pc,
                          const bool branch);

  bool IsInterruptPending() const;
};

}
