#pragma once

#include <array>
#include <cstdint>
#include <utility>

namespace Cpu
{

class CpuState
{
public:
  // TODO: Enforce explicit types
  using Register      = uint32_t;
  using RegisterIndex = uint32_t;

private:
  // GPR registers
  std::array<Register, 32> registers_;

  // Multiply/Divide registers
  Register hi_;
  Register lo_;

  // Load/Branch delay registers
  std::pair<RegisterIndex, Register> load_;
  std::pair<RegisterIndex, Register> save_;

  // Program counters
  Register curr_;
  Register next_;

  // Recently branched flag
  bool branch_;

public:
  CpuState();

public:
  void Write(const RegisterIndex index, const Register value);

  void SetHiRegister(const Register value);
  void SetLoRegister(const Register value);

  void SetPC(const Register value);
  void SetNextPC(const Register value);

  void SetLoad(const RegisterIndex index, const Register value);
  void SetSave(const RegisterIndex index, const Register value);

  void SetBranch(const bool value);

  // When the CPU cycle ends update the GPR registers with the contents of the
  // contents of the load and branch delay registers
  void ExecuteLoadDelay();

  // After fetching an instruction update the current PC with the value of the
  // next PC. Then increment the next PC to the address of the next instruction.
  void UpdatePC();

  void Reset();

public:
  uint32_t Read(const RegisterIndex index) const;
  int32_t  ReadSigned(const RegisterIndex index) const;

  uint32_t LoRegister() const;
  uint32_t HiRegister() const;

  uint32_t PC() const;
  uint32_t NextPC() const;

  uint32_t GetLoad(const RegisterIndex index) const;
  uint32_t GetSave(const RegisterIndex index) const;

  bool InBranch() const;
};

} // namespace Cpu
