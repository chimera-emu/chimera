#pragma once

#include <Core/PSX/Cpu/COP0.h>
#include <Core/PSX/Cpu/CpuState.h>
#include <Core/PSX/Cpu/Instruction.h>
#include <Core/PSX/Cpu/Types/ExceptionCode.h>

#include <cstdint>
#include <memory>

namespace Interrupts { class InterruptController; }
namespace Memory { class MemoryController; }

namespace Cpu
{

class Processor
{
private:
  CpuState state_;

  // TODO: Should this be here, or inside CpuState?
  COP0 cop0_;

  uint32_t pc_;

  Memory::MemoryController* memory_;

public:
  Processor(Interrupts::InterruptController* interrupts,
            Memory::MemoryController* memory);

public:
  void Run();
  void Step();
  void Reset();

private:
  void DecodeInstruction(const Instruction instr);
  void DecodeExtInstruction(const Instruction instr);

private:
  Instruction FetchInstruction(const uint32_t address);

  void HandleException(const ExceptionCode code);

  bool IsInterruptPending();

  void BranchInstruction(const Instruction instr);              // 0x01
  void Jump(const Instruction instr);                           // 0x02
  void JumpAndLink(const Instruction instr);                    // 0x03
  void BranchOnEqual(const Instruction instr);                  // 0x04
  void BranchOnNotEqual(const Instruction instr);               // 0x05
  void BranchOnLTEZero(const Instruction instr);                // 0x06
  void BranchOnGTZero(const Instruction instr);                 // 0x07
  void AddImmediate(const Instruction instr);                   // 0x08
  void AddImmediateUnsigned(const Instruction instr);           // 0x09
  void SetOnLessThanImmediate(const Instruction instr);         // 0x0A
  void SetOnLessThanImmediateUnsigned(const Instruction instr); // 0x0B
  void BitwiseAndImmediate(const Instruction instr);            // 0x0C
  void BitwiseOrImmediate(const Instruction instr);             // 0x0D
  void BitwiseXorImmediate(const Instruction instr);            // 0x0E
  void LoadUpperImmediate(const Instruction instr);             // 0x0F
  void DecodeCOP0Instruction(const Instruction instr);          // 0x10
  void DecodeCOP1Instruction();                                 // 0x11
  void DecodeCOP2Instruction(const Instruction instr);          // 0x12
  void DecodeCOP3Instruction();                                 // 0x13
  void LoadByte(const Instruction instr);                       // 0x20
  void LoadHalfWord(const Instruction instr);                   // 0x21
  void LoadWordLeft(const Instruction instr);                   // 0x22
  void LoadWord(const Instruction instr);                       // 0x23
  void LoadByteUnsigned(const Instruction instr);               // 0x24
  void LoadHalfWordUnsigned(const Instruction instr);           // 0x25
  void LoadWordRight(const Instruction instr);                  // 0x26
  void StoreByte(const Instruction instr);                      // 0x28
  void StoreHalfWord(const Instruction instr);                  // 0x29
  void StoreWordLeft(const Instruction instr);                  // 0x2A
  void StoreWord(const Instruction instr);                      // 0x2B
  void StoreWordRight(const Instruction instr);                 // 0x2E
  void LoadWordCoprocessor0();                                  // 0x30
  void LoadWordCoprocessor1();                                  // 0x31
  void LoadWordCoprocessor2();                                  // 0x32
  void LoadWordCoprocessor3();                                  // 0x33
  void StoreWordCoprocessor0();                                 // 0x38
  void StoreWordCoprocessor1();                                 // 0x39
  void StoreWordCoprocessor2();                                 // 0x3A
  void StoreWordCoprocessor3();                                 // 0x3B

  void ShiftLeftLogical(const Instruction instr);               // 0x00 -- 0x00
  void ShiftRightLogical(const Instruction instr);              // 0x00 -- 0x02
  void ShiftRightArithmetic(const Instruction instr);           // 0x00 -- 0x03
  void ShiftLeftLogicalVar(const Instruction instr);            // 0x00 -- 0x04
  void ShiftRightLogicalVar(const Instruction instr);           // 0x00 -- 0x06
  void ShiftRightArithmeticVar(const Instruction instr);        // 0x00 -- 0x07
  void JumpRegister(const Instruction instr);                   // 0x00 -- 0x08
  void JumpAndLinkRegister(const Instruction instr);            // 0x00 -- 0x09
  void SystemCall();                                            // 0x00 -- 0x0C
  void Break();                                                 // 0x00 -- 0x0D
  void MoveFromHI(const Instruction instr);                     // 0x00 -- 0x10
  void MoveToHI(const Instruction instr);                       // 0x00 -- 0x11
  void MoveFromLO(const Instruction instr);                     // 0x00 -- 0x12
  void MoveToLO(const Instruction instr);                       // 0x00 -- 0x13
  void Multiply(const Instruction instr);                       // 0x00 -- 0x18
  void MultiplyUnsigned(const Instruction instr);               // 0x00 -- 0x19
  void Divide(const Instruction instr);                         // 0x00 -- 0x1A
  void DivideUnsigned(const Instruction instr);                 // 0x00 -- 0x1B
  void AddRegisters(const Instruction instr);                   // 0x00 -- 0x20
  void AddUnsigned(const Instruction instr);                    // 0x00 -- 0x21
  void Subtract(const Instruction instr);                       // 0x00 -- 0x22
  void SubtractUnsigned(const Instruction instr);               // 0x00 -- 0x23
  void BitwiseAnd(const Instruction instr);                     // 0x00 -- 0x24
  void BitwiseOr(const Instruction instr);                      // 0x00 -- 0x25
  void BitwiseXor(const Instruction instr);                     // 0x00 -- 0x26
  void BitwiseNor(const Instruction instr);                     // 0x00 -- 0x27
  void SetOnLessThan(const Instruction instr);                  // 0x00 -- 0x2A
  void SetOnLessThanUnsigned(const Instruction instr);          // 0x00 -- 0x2B

  void MoveFromCoprocessor0(const Instruction instr);           // 0x10 -- 0x00
  void MoveToCoprocessor0(const Instruction instr);             // 0x10 -- 0x04
  void ReturnFromException();                                   // 0x10 -- 0x10
};

} // namespace Cpu
