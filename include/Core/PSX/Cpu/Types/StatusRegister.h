#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>

namespace Cpu
{

struct StatusRegister : public RegisterBase<StatusRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  bool IsCurrentInterruptEnabled()  const { return Test(0); }
  bool IsCurrentUserMode()          const { return Test(1); }
  bool IsPrevInterruptDisabled()    const { return Test(2); }
  bool IsPrevUserMode()             const { return Test(3); }
  bool IsOldInterruptDisabled()     const { return Test(4); }
  bool IsOldUserMode()              const { return Test(5); }

  uint32_t GetInterruptMask()       const { return Range(8, 15); }

  bool IsCacheIsolated()            const { return Test(16); }
  bool IsSwappedCacheMode()         const { return Test(17); }
  bool IsCacheParityWriteEnabled()  const { return Test(18); }
  bool IsCacheMissSet()             const { return Test(19); }
  bool IsParityErrorSet()           const { return Test(20); }
  bool IsTLBShutdownSet()           const { return Test(21); }
  bool IsBootstrapMode()            const { return Test(22); }
  bool HasReverseEndianness()       const { return Test(25); }

  bool IsCOP0Enabled()              const { return Test(28); }
  bool IsCOP1Enabled()              const { return Test(29); }
  bool IsCOP2Enabled()              const { return Test(30); }
  bool IsCOP3Enabled()              const { return Test(31); }

public:
  void ShiftInterruptStateLeft()
  {
    data_ = ((data_ & ~0x3FU) | ((data_ << 2) & 0x3F));
  }

  void ShiftInterruptStateRight()
  {
    data_ = ((data_ & ~0x0F) | ((data_ >> 2) & 0x0F));
  }
};

} // namespace Cpu
