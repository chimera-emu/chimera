#pragma once

#include <cstdint>

namespace Cpu
{

enum COP0Index : uint32_t
{
  BPC      = 0x03, // Breakpoint on execute
  BDA      = 0x05, // Breakpoint on data access
  JumpDest = 0x06, // Jump to Random Address?
  DCIC     = 0x07, // Breakpoint Control
  BadVAddr = 0x08, // Bad Virtual Address
  BDAM     = 0x09, // Data Access Breakpoint Mask
  BPCM     = 0x0B, // Execute Breakpoint Mask
  Status   = 0x0C, // System Status
  Cause    = 0x0D, // Exception Cause
  EPC      = 0x0E, // Exception Return Address
  PRID     = 0x0F  // Processor ID
};

} // namespace Cpu
