#pragma once

#include <Core/PSX/Cpu/Types/ExceptionCode.h>
#include <Core/PSX/RegisterBase.h>

#include <cstdint>

namespace Cpu
{

class CauseRegister : public RegisterBase<CauseRegister>
{
public:
  using RegisterBase::RegisterBase;

public:
  ExceptionCode GetExceptionCode() const
  {
    return static_cast<ExceptionCode>(Range(2, 6));
  }

public:
  // Only Bits[8:9] are writable, mask out the rest before setting the value
  void Set(const uint32_t value)
  {
    constexpr uint32_t MASK = 0x300;

    data_ &= ~(MASK);
    data_ |= value & MASK;
  }

  void SetBranchDelay(const bool enable)
  {
    enable ? SetBit(31) : Clear(31);
  }

  void SetInterruptPending() { data_ &= 0x0000FF00; }

  void SetExceptionCode(const ExceptionCode code)
  {
    data_ &= ~0x007C;
    data_ |= static_cast<uint32_t>(code) << 2;
  }
};

} // namespace Cpu
