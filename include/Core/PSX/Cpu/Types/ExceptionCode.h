#pragma once

#include <cstdint>
#include <iosfwd>

namespace Cpu
{

enum ExceptionCode : uint32_t
{
  ExternalInterrupt   = 0x00,
  ModifyCache         = 0x01,
  LoadCacheError      = 0x02,
  StoreCacheError     = 0x03,
  LoadAddressError    = 0x04,
  StoreAddressError   = 0x05,
  FetchBusError       = 0x06,
  DataBusError        = 0x07,
  SysCall             = 0x08,
  Breakpoint          = 0x09,
  ReservedInstruction = 0x0A,
  CoprocessorUnusable = 0x0B,
  ArithmeticOverflow  = 0x0C
};

std::ostream& operator<<(std::ostream& os, const ExceptionCode code);

} // namespace Cpu
