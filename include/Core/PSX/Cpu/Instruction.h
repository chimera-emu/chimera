#pragma once

#include <Core/PSX/RegisterBase.h>

#include <cstdint>
#include <string>

namespace Cpu
{

class Instruction : public RegisterBase<Instruction>
{
public:
  using RegisterBase::RegisterBase;

public:
  uint32_t Opcode()       const { return Range(26, 31); }
  uint32_t Function()     const { return Range( 0,  5); }
  uint32_t COP0Function() const { return Range(21, 25); }

  uint32_t RS() const { return Range(21, 25); }
  uint32_t RT() const { return Range(16, 20); }
  uint32_t RD() const { return Range(11, 15); }

  uint32_t Shift() const { return Range(6, 10); }

  // TODO: Some of these seem spurious/redundant, resolve them
  uint32_t Immediate() const { return Range(0, 15); }

  int32_t ImmediateSE() const
  {
    return static_cast<int32_t>(static_cast<int16_t>(Range(0, 16)));
  }

  uint32_t ImmediateSU() const
  {
    return static_cast<uint32_t>(ImmediateSE());
  }

  uint32_t ImmediateJump() const { return (Range(0, 25) << 2); }

  std::string ToString() const;

private:
  std::string FormatExtInstruction() const;
  std::string FormatCOP0Instruction() const;

};

} // namespace Cpu
