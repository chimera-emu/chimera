#pragma once

#include <Core/PSX/Interrupts/InterruptChannel.h>

#include <Core/PSX/Memory/MMIOInterface.h>

#include <cstdint>

namespace Interrupts
{

class InterruptController : public MMIOInterface
{
private:
  uint16_t status_;
  uint16_t mask_;

public:
  InterruptController() : status_(0), mask_(0) {}

public:
  void HandleWrite(const uint32_t address, const uint32_t value) override;

  void Trigger(const InterruptChannel channel);

private:
  // Acknowledges interrupts on certain channels.
  void WriteStatus(const uint32_t value);

  // Enable or disable interrupts on certain channels.
  void WriteMask(const uint32_t value);


public:
  uint32_t HandleRead(const uint32_t address) const override;

  bool IsActive() const;
};

} // namespace Interrupts
