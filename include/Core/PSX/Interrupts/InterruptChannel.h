#pragma once

#include <cstdint>
#include <ostream>

namespace Interrupts
{

enum class InterruptChannel : uint16_t
{
  VBLANK   = 1U << 0,
  GPU      = 1U << 1,
  CDROM    = 1U << 2,
  Dma      = 1U << 3,
  TMR0     = 1U << 4,
  TMR1     = 1U << 5,
  TMR2     = 1U << 6,
  MEMPAD   = 1U << 7,
  SIO      = 1U << 8,
  SPU      = 1U << 9,
  LIGHTGUN = 1U << 10
};

inline std::ostream& operator<<(std::ostream& os, const InterruptChannel channel)
{
  switch (channel)
  {
    case InterruptChannel::VBLANK:   { return os << "VBlank";   };
    case InterruptChannel::GPU:      { return os << "GPU";      };
    case InterruptChannel::CDROM:    { return os << "CD-ROM";   };
    case InterruptChannel::Dma:      { return os << "Dma";      };
    case InterruptChannel::TMR0:     { return os << "Timer 0";  };
    case InterruptChannel::TMR1:     { return os << "Timer 1";  };
    case InterruptChannel::TMR2:     { return os << "Timer 2";  };
    case InterruptChannel::MEMPAD:   { return os << "Mempad";   };
    case InterruptChannel::SIO:      { return os << "Serial";   };
    case InterruptChannel::SPU:      { return os << "SPU";      };
    case InterruptChannel::LIGHTGUN: { return os << "Lightgun"; };
  }

  return os;
}

}
