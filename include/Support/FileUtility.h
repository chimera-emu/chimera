#pragma once

#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace Utility
{

inline bool FileExists(const std::string& filepath)
{
  std::ifstream infile(filepath.c_str());

  return infile.is_open();
}

inline std::vector<uint8_t> FileToByteArray(const std::string &filepath)
{
  std::ifstream stream(filepath, std::ios::binary | std::ios::ate);

  const std::ifstream::pos_type fileSize = stream.tellg();

  std::vector<uint8_t> buffer(static_cast<uint32_t>(fileSize));

  stream.seekg(0, std::ios::beg);
  stream.read(reinterpret_cast<char*>(&buffer[0]), fileSize);

  return buffer;
}

inline std::string FileToString(const std::string& filepath)
{
  // TODO: Should return an empty string or at least a sensible exception
  if (!FileExists(filepath))
    throw std::runtime_error("File does not exist");

  std::ifstream fileStream(filepath.c_str(), std::ios::in);

  std::stringstream buffer;
  buffer << fileStream.rdbuf();

  return buffer.str();
}

}  // namespace Utility
