#pragma once

#include <cstdint>
#include <limits>
#include <stdexcept>

namespace Utility
{

// TODO: Phrasing
inline bool AddWillOverflow(const int32_t x, const int32_t y)
{
  const int64_t sum = static_cast<int64_t>(x) + static_cast<int64_t>(y);

  if (sum > std::numeric_limits<int32_t>::max())
    return true;

  if (sum < std::numeric_limits<int32_t>::min())
    return true;

  return false;
}

inline bool SubWillUnderflow(const int32_t x, const int32_t y)
{
  const int64_t sum = static_cast<int64_t>(x) - static_cast<int64_t>(y);

  if (sum > std::numeric_limits<int32_t>::max())
    return true;

  if (sum < std::numeric_limits<int32_t>::min())
    return true;

  return false;
}

} // namespace Utility
