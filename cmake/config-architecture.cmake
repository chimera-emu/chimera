# If an architecture wasn't specified, assume we want 64 bits
if(NOT ARCH)
  set(ARCH "x64")
endif()

if(ARCH STREQUAL "x64")
  include(cmake/Architecture/config-x64.cmake)
elseif(ARCH STREQUAL "x86")
  include(cmake/Architecture/config-x86.cmake)
elseif(ARCH STREQUAL "arm")
  include(cmake/Architecture/config-arm.cmake)
else()
  message(FATAL_ERROR "Unsupported architecture specified: ${ARCH}")
endif()
