# By default target whatever operating system we're currently on
if(NOT PLATFORM)
  set(PLATFORM "${CMAKE_HOST_SYSTEM_NAME}")
endif()

if(PLATFORM STREQUAL "Linux")
  include(cmake/Platform/config-linux.cmake)
elseif(PLATFORM STREQUAL "Windows")
  include(cmake/Platform/config-windows.cmake)
else()
  message(FATAL_ERROR "Target platform not supported: ${PLATFORM}")
endif()
