find_package(Threads REQUIRED)

set(PLATFORM_LIBS "${PLATFORM_LIBS}Threads::Threads")

add_compiler_flags(-DPLATFORM_LINUX)
