include(FindPackageHandleStandardArgs)

find_path(LIBXCB_INCLUDE_DIR xcb/xcb.h)
find_library(LIBXCB_LIBRARY NAMES xcb libxcb)

set(LIBXCB_LIBRARIES ${LIBXCB_LIBRARY})
set(LIBXCB_INCLUDE_DIRS ${LIBXCB_INCLUDE_DIR})

find_package_handle_standard_args(LIBXCB DEFAULT_MSG LIBXCB_LIBRARY
                                                     LIBXCB_INCLUDE_DIR)

foreach(XCB_COMPONENT ${XCB_FIND_COMPONENTS})
  string(TOUPPER ${XCB_COMPONENT} XCB_UPPER)
  string(TOLOWER ${XCB_COMPONENT} XCB_LOWER)

  find_library(LIBXCB_${XCB_UPPER}_LIBRARY NAMES libxcb-${XCB_LOWER}
                                                    xcb-${XCB_LOWER})

  find_package_handle_standard_args(LIBXCB_${XCB_UPPER} DEFAULT_MSG
                                    LIBXCB_${XCB_UPPER}_LIBRARY
                                    LIBXCB_INCLUDE_DIR)

  list(APPEND LIBXCB_LIBRARIES ${LIBXCB_${XCB_UPPER}_LIBRARY})

  if(NOT LIBXCB_${XCB_UPPER}_FOUND)
      message(FATAL_ERROR "xcb-${XCB_LOWER} not found")
  endif()
endforeach()

list(REMOVE_DUPLICATES LIBXCB_INCLUDE_DIR)
