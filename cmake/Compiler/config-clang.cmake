#
add_compiler_flags(-stdlib=libstdc++)

# Enabled Warnings
add_compiler_flags(-Weverything)

# Disabled Warnings
add_compiler_flags(-Wno-c++98-compat-pedantic)
add_compiler_flags(-Wno-c++98-compat)
add_compiler_flags(-Wno-missing-braces)
add_compiler_flags(-Wno-padded)
add_compiler_flags(-Wno-weak-vtables)
add_compiler_flags(-Wno-unneeded-internal-declaration)
add_compiler_flags(-Wno-missing-noreturn)

# Enable in future
add_compiler_flags(-Wno-exit-time-destructors)
add_compiler_flags(-Wno-global-constructors)
add_compiler_flags(-Wno-sign-conversion)
