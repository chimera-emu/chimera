Chimera
=======

|       | Windows                                                                                                                                   | Linux                                                                                                                              |
| ----- | ----------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| Build | [![Build status](https://ci.appveyor.com/api/projects/status/7w4stpjqpfdfntey?svg=true)](https://ci.appveyor.com/project/chimera/chimera) | [![build status](https://gitlab.com/chimera/chimera/badges/master/build.svg)](https://gitlab.com/scartmell/chimera/commits/master) |


Chimera will be a multi-platform multi-system emulator.
