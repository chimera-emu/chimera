"""Chimera configuration script.

Usage:
  configure.sh
  configure.sh [--build-tests] [--build-examples] [--build-docs] [--arch=ARCH]
               [--platform=PLAT] [--compiler=CXX] [--build-type=TYPE]

Options:
  --compiler=CXX          Compiler [default: clang]
  --build-type=TYPE       Build type [default: release]
  --platform=PLAT         Target platform [default: linux]
  --arch=ARCH             Target architecture [default: x64]

  --build-docs            Build documentation
  --build-tests           Build test binaries
  --build-examples        Build example binaries
"""

from __future__ import print_function
from distutils.spawn import find_executable

from docopt import docopt
import shutil
import subprocess
import os


def validate_arguments(arguments):
  # Validate 'Compiler'
  compiler        = arguments["--compiler"].lower()
  valid_compilers = ["gcc", "clang"]

  if compiler not in valid_compilers:
    print("Invalid compiler specified, expected: ", end="")
    print(", ".join(valid_compilers))

    return False

  # Validate 'Build Type'
  build_type   = arguments["--build-type"].lower()
  valid_builds = ["release", "debug", "instrumented"]

  if build_type not in valid_builds:
    print("Invalid build type specified, expected: ", end="")
    print(", ".join(valid_builds))

    return False

  # Validate 'Target Platform'
  platform        = arguments["--platform"].lower()
  valid_platforms = ["linux"]

  if platform not in valid_platforms:
    print("Invalid target platform specified, expected: ", end="")
    print(", ".join(valid_platforms))

    return False

  # Validate 'Target Architecture'
  architecture        = arguments["--arch"].lower()
  valid_architectures = ["x86", "x64"]

  if architecture not in valid_architectures:
    print("Invalid target platform specified, expected: ", end="")
    print(", ".join(valid_architectures))

    return False

  return True


def check_cmake_installation():
  return find_executable("cmake") is not None


def set_cmake_arguments(arguments):
  cmake_arguments = []

  specified_compiler = arguments["--compiler"].lower()
  compiler_argument  = "-DCMAKE_CXX_COMPILER="

  if (specified_compiler == "gcc"):
    compiler_argument += "g++"
  elif(specified_compiler == "clang"):
    compiler_argument += "clang++"

  cmake_arguments.append(compiler_argument)

  specified_build_type = arguments["--build-type"].lower()
  build_type_argument  = "-DCMAKE_BUILD_TYPE="

  if (specified_build_type == "release"):
    build_type_argument += "Release"
  elif(specified_build_type == "debug"):
    build_type_argument += "Debug"

  cmake_arguments.append(build_type_argument)

  if (arguments["--build-docs"]):
    cmake_arguments.append("-DBUILD_DOCS=on")

  if (arguments["--build-tests"]):
    cmake_arguments.append("-DBUILD_TESTS=on")

  if (arguments["--build-examples"]):
    cmake_arguments.append("-DBUILD_EXAMPLES=on")

  return cmake_arguments


def run_clean_stage():
  print("Deleting build directory")
  shutil.rmtree("build", ignore_errors=True)

  print("Deleting output directory")
  shutil.rmtree("bin", ignore_errors=True)


def run_build_stage(arguments):
  if (not check_cmake_installation()):
    print("CMake is not installed")

    return

  print("Creating build directory")
  os.makedirs("build")
  os.chdir("build")

  args    = ["cmake"] + set_cmake_arguments(arguments) + [".."]
  command = " ".join(args)

  subprocess.call([command], shell=True)
  subprocess.call(["make", "-j14"])


def execute_script(arguments):
  if (not validate_arguments(arguments)):
    return

  run_clean_stage()
  run_build_stage(arguments)

if __name__ == '__main__':
  arguments = docopt(__doc__)

  execute_script(arguments)
