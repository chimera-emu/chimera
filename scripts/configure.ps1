
[CmdletBinding()]
param
(
  # TODO: Passing a list of arguments doesn't currently work when using the .bat
  #       file to run this build script. Batch files parse commas incorrectly
  #       which screws everything up
  [Parameter(Mandatory=$False, Position=0)]
  [ValidateSet("Clean", "Update", "Build", "Test", "Package", "Install")]
  [string[]]
  $Actions = @("Clean", "Build"),

  ##----------------------------------------------------------------------------
  # Parameters pertaining to "Build"
  ##----------------------------------------------------------------------------
  [Parameter(Mandatory=$False)]
  [ValidateSet("Windows")]
  [string]
  $TargetPlatform = "Windows",

  [Parameter(Mandatory=$False)]
  [ValidateSet("x64", "x86", "ARM")]
  [string]
  $TargetArchitecture = "x64",

  [Parameter(Mandatory=$False)]
  [ValidateSet("Release", "Debug", "Instrumented")]
  [string]
  $BuildConfiguration = "Release",

  [Parameter(Mandatory=$False)]
  [ValidateSet("MSVC", "Clang", "GCC")]
  [string]
  $Compiler = "MSVC",

  [Parameter(Mandatory=$False)]
  [switch]
  $BuildTests,

  [Parameter(Mandatory=$False)]
  [switch]
  $BuildDocs
)

##------------------------------------------------------------------------------
# Global constants
##------------------------------------------------------------------------------
# HACK: There's an assumption here that this script lives in the scripts folder
$CurrentDirectory = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$ProjectDirectory = (Get-Item $CurrentDirectory).parent.FullName

# TODO: These should be Parameters so the user can configure them
$BuildDirectory  = $ProjectDirectory + "\build"
$BinaryDirectory = $ProjectDirectory + "\bin"

##------------------------------------------------------------------------------
# Helper functions
#
# TODO: Move these to a seperate utility script so they don't clutter this one
##------------------------------------------------------------------------------
function IsCMakeInstalled
{
  return [bool](Get-Command "cmake" -ErrorAction SilentlyContinue)
}

function IsVisualStudioInstalled
{
  (Get-ChildItem Env: | where { $_.name -eq "VS140COMNTOOLS" }) -ne $null
}

function InitializeCMakeArguments
{
  $Arguments = New-Object System.Collections.ArrayList

  $Generator = "-GVisual Studio 14 2015"

  if ($TargetArchitecture -eq "x64")
  {
    $Generator = ("{0} {1}" -f $Generator, "Win64")
  }
  elseif ($TargetArchitecture -eq "ARM")
  {
    $Generator = ("{0} {1}" -f $Generator, "ARM")
  }

  $Arguments.Add($Generator)
  $Arguments.Add($ProjectDirectory)

  if ($BuildTests.IsPresent)
  {
    $Arguments.Add("-DBUILD_TESTS=on")
  }

  if ($BuildDocs.IsPresent)
  {
    $Arguments.Add("-DBUILD_DOCS=on")
  }

  return $Arguments
}

function FindMSBuildLocation
{
  # TODO: This path may change with newer versions?
  $regLocation = "HKLM:\SOFTWARE\Microsoft\MSBuild\ToolsVersions\14.0"
  $regTable    = Get-ItemProperty $regLocation -ErrorAction SilentlyContinue

  if ($regTable -eq $null)
  {
    Write-Error "Could not locate the MsBuild registry table"
    Exit-PSSession
  }

  if ($regTable.MSBuildToolsPath -eq $null)
  {
    Write-Error "Could not locate the build tool path entry in the registry"
    Exit-PSSession
  }

  return (Join-Path $regTable.MSBuildToolsPath -ChildPath "MsBuild.exe")
}

##------------------------------------------------------------------------------
# Build Script stages
##------------------------------------------------------------------------------
function clean
{
  Write-Output ("Deleting Build Directory: {0}" -f $BuildDirectory)
  Remove-Item $BuildDirectory -Force -Recurse -ErrorAction SilentlyContinue

  Write-Output ("Deleting Output Directory: {0}" -f $BinaryDirectory)
  Remove-Item $BinaryDirectory -Force -Recurse -ErrorAction SilentlyContinue
}

# TODO: Print some suggested instructions on resolving missing dependancies
function build
{
  if (-Not (IsCMakeInstalled))
  {
    Write-Error "Could not locate CMake installation"
    Exit-PSSession
  }

  if (-Not (IsVisualStudioInstalled))
  {
    Write-Error "Could not locate Visual Studio 14 2015 installation"
    Exit-PSSession
  }

  # Create the build folder if it doesn't currently exist
  if (-Not (Test-Path $BuildDirectory -PathType Container))
  {
    New-Item $BuildDirectory -ItemType Directory | Out-Null
  }

  # Move to the selected build directory before running the cmake command
  Set-Location -Path $BuildDirectory

  $arguments = InitializeCMakeArguments
  & 'cmake' $arguments

  $MsBuildPath = FindMSBuildLocation

  & $MsBuildPath (Join-Path $BuildDirectory -ChildPath "Chimera.sln")
}


foreach($Action in $Actions)
{
  Write-Output "Executing Step: $Action"

  &$Action
}
