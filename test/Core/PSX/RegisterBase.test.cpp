#include <doctest/doctest.h>

#include <cstdint>

#define private public

#include <Core/PSX/RegisterBase.h>

// TODO: Add behaviours for out of bound index access

class Derived {};

TEST_CASE("RegisterBase - Initialises with the correct bits")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    CHECK(test.Get() == 0xF0);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    CHECK(test.Get() == 0xFFFF0000);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    CHECK(test.Get() == 0xFFFFFFFF00000000);
  }
}

TEST_CASE("RegisterBase - Set functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    test.Set(0x00);
    CHECK(test.Get() == 0x00);

    test.Set(0xFF);
    CHECK(test.Get() == 0xFF);

    test.Set(0x0F);
    CHECK(test.Get() == 0x0F);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    test.Set(0x00000000);
    CHECK(test.Get() == 0x00000000);

    test.Set(0xFFFFFFFF);
    CHECK(test.Get() == 0xFFFFFFFF);

    test.Set(0x0000FFFF);
    CHECK(test.Get() == 0x0000FFFF);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    test.Set(0x0000000000000000);
    CHECK(test.Get() == 0x0000000000000000);

    test.Set(0xFFFFFFFFFFFFFFFF);
    CHECK(test.Get() == 0xFFFFFFFFFFFFFFFF);

    test.Set(0x00000000FFFFFFFF);
    CHECK(test.Get() == 0x00000000FFFFFFFF);
  }
}

TEST_CASE("RegisterBase - Flip() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test1 = RegisterBase<Derived, uint8_t>(0xF0);
    auto test2 = RegisterBase<Derived, uint8_t>(0x00);
    auto test3 = RegisterBase<Derived, uint8_t>(0xFF);

    test1.Flip();
    test2.Flip();
    test3.Flip();

    CHECK(test1.Get() == 0x0F);
    CHECK(test2.Get() == 0xFF);
    CHECK(test3.Get() == 0x00);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test1 = RegisterBase<Derived, uint32_t>(0xFFFF0000);
    auto test2 = RegisterBase<Derived, uint32_t>(0x00000000);
    auto test3 = RegisterBase<Derived, uint32_t>(0xFFFFFFFF);

    test1.Flip();
    test2.Flip();
    test3.Flip();

    CHECK(test1.Get() == 0x0000FFFF);
    CHECK(test2.Get() == 0xFFFFFFFF);
    CHECK(test3.Get() == 0x00000000);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test1 = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);
    auto test2 = RegisterBase<Derived, uint64_t>(0x0000000000000000);
    auto test3 = RegisterBase<Derived, uint64_t>(0xFFFFFFFFFFFFFFFF);

    test1.Flip();
    test2.Flip();
    test3.Flip();

    CHECK(test1.Get() == 0x00000000FFFFFFFF);
    CHECK(test2.Get() == 0xFFFFFFFFFFFFFFFF);
    CHECK(test3.Get() == 0x0000000000000000);
  }
}

TEST_CASE("RegisterBase - Flip(position) functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    test.Flip(0);
    test.Flip(1);
    test.Flip(7);

    CHECK(test.Test(0) ==  true);
    CHECK(test.Test(1) ==  true);
    CHECK(test.Test(7) == false);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    test.Flip(0);
    test.Flip(1);
    test.Flip(16);

    CHECK(test.Test(0)  ==  true);
    CHECK(test.Test(1)  ==  true);
    CHECK(test.Test(16) == false);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    test.Flip(0);
    test.Flip(1);
    test.Flip(32);

    CHECK(test.Test(0)  ==  true);
    CHECK(test.Test(1)  ==  true);
    CHECK(test.Test(32) == false);
  }
}


TEST_CASE("RegisterBase - Test() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    CHECK(test.Test(0) == false);
    CHECK(test.Test(3) == false);
    CHECK(test.Test(4) ==  true);
    CHECK(test.Test(7) ==  true);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    CHECK(test.Test( 0) == false);
    CHECK(test.Test(15) == false);
    CHECK(test.Test(16) ==  true);
    CHECK(test.Test(31) ==  true);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    CHECK(test.Test( 0) == false);
    CHECK(test.Test(31) == false);
    CHECK(test.Test(32) ==  true);
    CHECK(test.Test(63) ==  true);
  }
}

TEST_CASE("RegisterBase - Get() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    CHECK(test.Get(0) == 0);
    CHECK(test.Get(3) == 0);
    CHECK(test.Get(4) == 1);
    CHECK(test.Get(7) == 1);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    CHECK(test.Get( 0) == 0);
    CHECK(test.Get(15) == 0);
    CHECK(test.Get(16) == 1);
    CHECK(test.Get(31) == 1);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    CHECK(test.Get( 0) == 0);
    CHECK(test.Get(31) == 0);
    CHECK(test.Get(32) == 1);
    CHECK(test.Get(63) == 1);
  }
}

TEST_CASE("RegisterBase - Range() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    CHECK(test.Range(0, 0) == 0x00);
    CHECK(test.Range(0, 7) == 0xF0);
    CHECK(test.Range(0, 3) == 0x00);
    CHECK(test.Range(4, 7) == 0x0F);
    CHECK(test.Range(2, 6) == 0x1C);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    CHECK(test.Range( 0,  0) == 0x00000000);
    CHECK(test.Range( 0, 15) == 0x00000000);
    CHECK(test.Range(16, 31) == 0x0000FFFF);
    CHECK(test.Range( 8, 23) == 0x0000FF00);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    CHECK(test.Range( 0,  0) == 0x0000000000000000);
    CHECK(test.Range( 0, 31) == 0x0000000000000000);
    CHECK(test.Range(32, 63) == 0x00000000FFFFFFFF);
    CHECK(test.Range(16, 47) == 0x00000000FFFF0000);
  }
}

TEST_CASE("RegisterBase - Enable() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    test.Enable(0);
    test.Enable(2);
    test.Enable(7);

    CHECK(test.Get(0) == 1);
    CHECK(test.Get(2) == 1);
    CHECK(test.Get() == 0xF5);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    test.Enable( 0);
    test.Enable( 2);
    test.Enable(31);

    CHECK(test.Get(0) == 1);
    CHECK(test.Get(2) == 1);
    CHECK(test.Get() == 0xFFFF0005);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    test.Enable( 0);
    test.Enable( 2);
    test.Enable(63);

    CHECK(test.Get(0) == 1);
    CHECK(test.Get(2) == 1);
    CHECK(test.Get() == 0xFFFFFFFF00000005);
  }
}

TEST_CASE("RegisterBase - Clear() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    test.Clear(0);
    test.Clear(4);
    test.Clear(7);

    CHECK(test.Get(0) == 0);
    CHECK(test.Get(4) == 0);
    CHECK(test.Get() == 0x60);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    test.Clear( 0);
    test.Clear(16);
    test.Clear(31);

    CHECK(test.Get(0) == 0);
    CHECK(test.Get(2) == 0);
    CHECK(test.Get() == 0x7FFE0000);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    test.Clear( 0);
    test.Clear(32);
    test.Clear(63);

    CHECK(test.Get(0)  == 0);
    CHECK(test.Get(32) == 0);
    CHECK(test.Get() == 0x7FFFFFFE00000000);
  }
}

TEST_CASE("RegisterBase - SetBit() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    SUBCASE("SetBit(false) clears bits correctly")
    {
      test.SetBit(0, false);
      test.SetBit(4, false);
      test.SetBit(7, false);

      CHECK(test.Get(0) == 0);
      CHECK(test.Get(4) == 0);
      CHECK(test.Get() == 0x60);
    }

    SUBCASE("SetBit(true) sets bits correctly")
    {
      test.SetBit(0, true);
      test.SetBit(3, true);
      test.SetBit(7, true);

      CHECK(test.Get(0) == 1);
      CHECK(test.Get(3) == 1);
      CHECK(test.Get() == 0xF9);
    }

    SUBCASE("SetBit(default) enables bits by default")
    {
      test.SetBit(0);
      test.SetBit(3);
      test.SetBit(7);

      CHECK(test.Get(0) == 1);
      CHECK(test.Get(3) == 1);
      CHECK(test.Get() == 0xF9);
    }
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<uint32_t>(0xFFFF0000);

    SUBCASE("SetBit(false) clears bits correctly")
    {
      test.SetBit( 0, false);
      test.SetBit(16, false);
      test.SetBit(31, false);

      CHECK(test.Get(16) == 0);
      CHECK(test.Get(31) == 0);
      CHECK(test.Get() == 0x7FFE0000);
    }

    SUBCASE("SetBit(true) sets bits correctly")
    {
      test.SetBit( 0, true);
      test.SetBit(16, true);
      test.SetBit(31, true);

      CHECK(test.Get(16) == 1);
      CHECK(test.Get(31) == 1);
      CHECK(test.Get() == 0xFFFF0001);
    }

    SUBCASE("SetBit(default) enables bits by default")
    {
      test.SetBit( 0);
      test.SetBit( 2);
      test.SetBit(31);

      CHECK(test.Get(0) == 1);
      CHECK(test.Get(2) == 1);
      CHECK(test.Get() == 0xFFFF0005);
    }
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    SUBCASE("SetBit(false) clears bits correctly")
    {
      test.SetBit( 0, false);
      test.SetBit(32, false);
      test.SetBit(63, false);

      CHECK(test.Get(31) == 0);
      CHECK(test.Get(63) == 0);
      CHECK(test.Get() == 0x7FFFFFFE00000000);
    }

    SUBCASE("SetBit(true) sets bits correctly")
    {
      test.SetBit( 0, true);
      test.SetBit(31, true);
      test.SetBit(63, true);

      CHECK(test.Get(31) == 1);
      CHECK(test.Get(63) == 1);
      CHECK(test.Get() == 0xFFFFFFFF80000001);
    }

    SUBCASE("SetBit(default) enables bits by default")
    {
      test.SetBit( 0);
      test.SetBit(31);
      test.SetBit(63);

      CHECK(test.Get(31) == 1);
      CHECK(test.Get(63) == 1);
      CHECK(test.Get() == 0xFFFFFFFF80000001);
    }
  }
}

TEST_CASE("RegisterBase - All() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test1 = RegisterBase<Derived, uint8_t>(0xF0U);
    auto test2 = RegisterBase<Derived, uint8_t>(0x00U);
    auto test3 = RegisterBase<Derived, uint8_t>(0xFFU);

    CHECK(test1.All() == false);
    CHECK(test2.All() == false);
    CHECK(test3.All() ==  true);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test1 = RegisterBase<Derived, uint32_t>(0xFFFF0000);
    auto test2 = RegisterBase<Derived, uint32_t>(0x00000000);
    auto test3 = RegisterBase<Derived, uint32_t>(0xFFFFFFFF);

    CHECK(test1.All() == false);
    CHECK(test2.All() == false);
    CHECK(test3.All() ==  true);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test1 = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);
    auto test2 = RegisterBase<Derived, uint64_t>(0x0000000000000000);
    auto test3 = RegisterBase<Derived, uint64_t>(0xFFFFFFFFFFFFFFFF);

    CHECK(test1.All() == false);
    CHECK(test2.All() == false);
    CHECK(test3.All() ==  true);
  }
}

TEST_CASE("RegisterBase - None() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test1 = RegisterBase<Derived, uint8_t>(0xF0);
    auto test2 = RegisterBase<Derived, uint8_t>(0x00);
    auto test3 = RegisterBase<Derived, uint8_t>(0xFF);

    CHECK(test1.None() == false);
    CHECK(test2.None() ==  true);
    CHECK(test3.None() == false);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test1 = RegisterBase<Derived, uint32_t>(0xFFFF0000);
    auto test2 = RegisterBase<Derived, uint32_t>(0x00000000);
    auto test3 = RegisterBase<Derived, uint32_t>(0xFFFFFFFF);

    CHECK(test1.None() == false);
    CHECK(test2.None() ==  true);
    CHECK(test3.None() == false);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test1 = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);
    auto test2 = RegisterBase<Derived, uint64_t>(0x0000000000000000);
    auto test3 = RegisterBase<Derived, uint64_t>(0xFFFFFFFFFFFFFFFF);

    CHECK(test1.None() == false);
    CHECK(test2.None() ==  true);
    CHECK(test3.None() == false);
  }
}

TEST_CASE("RegisterBase - Any() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test1 = RegisterBase<Derived, uint8_t>(0xF0);
    auto test2 = RegisterBase<Derived, uint8_t>(0x00);
    auto test3 = RegisterBase<Derived, uint8_t>(0xFF);

    CHECK(test1.Any() ==  true);
    CHECK(test2.Any() == false);
    CHECK(test3.Any() ==  true);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test1 = RegisterBase<Derived, uint32_t>(0xFFFF0000);
    auto test2 = RegisterBase<Derived, uint32_t>(0x00000000);
    auto test3 = RegisterBase<Derived, uint32_t>(0xFFFFFFFF);

    CHECK(test1.Any() ==  true);
    CHECK(test2.Any() == false);
    CHECK(test3.Any() ==  true);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test1 = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);
    auto test2 = RegisterBase<Derived, uint64_t>(0x0000000000000000);
    auto test3 = RegisterBase<Derived, uint64_t>(0xFFFFFFFFFFFFFFFF);

    CHECK(test1.Any() ==  true);
    CHECK(test2.Any() == false);
    CHECK(test3.Any() ==  true);
  }
}

TEST_CASE("RegisterBase - Size() functions correctly")
{
  SUBCASE("<uint8_t> data")
  {
    auto test = RegisterBase<Derived, uint8_t>(0xF0);

    CHECK(test.Size() == 8);
  }

  SUBCASE("<uint32_t> data")
  {
    auto test = RegisterBase<Derived, uint32_t>(0xFFFF0000);

    CHECK(test.Size() == 32);
  }

  SUBCASE("<uint64_t> data")
  {
    auto test = RegisterBase<Derived, uint64_t>(0xFFFFFFFF00000000);

    CHECK(test.Size() == 64);
  }
}
