#include <doctest/doctest.h>

#include <cstdint>
#include <functional>
#include <numeric>

#define private public

#include <Core/PSX/Memory/MemoryRegion.h>

using namespace Memory;

TEST_CASE("Memory Region -- "
          "'Contains' function works correctly")
{
  const auto region = MemoryRegion("Test Region", 1000, 1000);

  // Address is inside region
  REQUIRE(region.Contains(1000));
  REQUIRE(region.Contains(1500));
  REQUIRE(region.Contains(2000));

  // Address is greater than region
  REQUIRE_FALSE(region.Contains(   -1));
  REQUIRE_FALSE(region.Contains( 2001));
  REQUIRE_FALSE(region.Contains(10000));

  // Address is less than region
  REQUIRE_FALSE(region.Contains(  0));
  REQUIRE_FALSE(region.Contains(500));
  REQUIRE_FALSE(region.Contains(999));
}

TEST_CASE("Memory Region -- "
          "Write function sets data in little endian order")
{
  auto region = MemoryRegion("Test Region", 0, 1000);

  SUBCASE("<uint32_t> data")
  {
    const uint32_t value = 0xAABBCCDD;

    // Write value into the memory region
    region.Write<uint32_t>(0, value);

    // Assert that data was written in little-endian order.
    const auto data = region.data_;
    REQUIRE(data[0] == 0xDD);
    REQUIRE(data[1] == 0xCC);
    REQUIRE(data[2] == 0xBB);
    REQUIRE(data[3] == 0xAA);
  }

  SUBCASE("<uint16_t> data")
  {
    const uint16_t value = 0xAABB;

    // Write value into the memory region
    region.Write<uint16_t>(0, value);

    // Assert that data was written in little-endian order.
    const auto data = region.data_;
    REQUIRE(data[0] == 0xBB);
    REQUIRE(data[1] == 0xAA);
  }

  SUBCASE("<uint8_t> data")
  {
    const uint8_t value = 0xAA;

    // Write value into the memory region
    region.Write<uint8_t>(0, value);

    // Assert that data was written in little-endian order.
    const auto data = region.data_;
    REQUIRE(data[0] == 0xAA);
  }
}

TEST_CASE("Memory Region -- "
          "Read function gets data in little endian order")
{
  auto region = MemoryRegion("Test Region", 0, 1000);

  SUBCASE("<uint8_t> data")
  {
    const uint8_t valueA = 0xAA;
    const uint8_t valueB = 0xFF;
    const uint8_t valueC = 0xBC;
    const uint8_t valueD = 0xDD;

    // Write value into the memory region
    region.Write<uint8_t>( 0, valueA);
    region.Write<uint8_t>(10, valueB);
    region.Write<uint8_t>(20, valueC);
    region.Write<uint8_t>(30, valueD);

    // Assert that data was written in little-endian order.
    REQUIRE(region.Read<uint8_t>( 0) == valueA);
    REQUIRE(region.Read<uint8_t>(10) == valueB);
    REQUIRE(region.Read<uint8_t>(20) == valueC);
    REQUIRE(region.Read<uint8_t>(30) == valueD);
  }

  SUBCASE("<uint16_t> data")
  {
    const uint16_t valueA = 0xAABB;
    const uint16_t valueB = 0xCCDD;

    // Write value into the memory region
    region.Write<uint16_t>(0, valueA);
    region.Write<uint16_t>(8, valueB);

    // Assert that data was written in little-endian order.
    REQUIRE(region.Read<uint16_t>(0) == valueA);
    REQUIRE(region.Read<uint16_t>(8) == valueB);
  }

  SUBCASE("<uint32_t> data")
  {
    const uint32_t valueA = 0xAABBCCDD;

    // Write value into the memory region
    region.Write<uint32_t>(0, valueA);

    // Assert that data was written in little-endian order.
    REQUIRE(region.Read<uint32_t>(0) == valueA);
  }
}

TEST_CASE("Memory Region -- "
          "MMIO callbacks passthrough correct data")
{
  uint32_t currentValue   = 0;
  uint32_t currentAddress = 0;

  std::function<void(uint32_t, uint32_t)> writeFunc =
    [&currentValue, &currentAddress](uint32_t address, uint32_t value)
    {
      currentAddress = address;
      currentValue   = value;
    };

  std::function<uint32_t(uint32_t)> readFunc =
    [&currentValue, &currentAddress](uint32_t address)
    {
      currentAddress = address;

      return 50;
    };

  auto region = MemoryRegion("Test Region", 0, 1000, writeFunc, readFunc);

  SUBCASE("Write Callback")
  {
    region.Write<uint32_t>(20, 42);
    REQUIRE(currentAddress == 20);
    REQUIRE(currentValue   == 42);

    region.Write<uint32_t>(500, 215);
    REQUIRE(currentAddress == 500);
    REQUIRE(currentValue   == 215);
  }

  SUBCASE("Read Callback")
  {
    currentValue = region.Read<uint32_t>(20);
    REQUIRE(currentAddress == 20);
    REQUIRE(currentValue   == 50);

    currentValue = region.Read<uint32_t>(500);
    REQUIRE(currentAddress == 500);
    REQUIRE(currentValue   ==  50);
  }
}

TEST_CASE("Memory Region -- "
          "Set Memory functions correctly")
{
  auto region = MemoryRegion("Test Region", 0, 1000);
  auto data   = std::vector<uint8_t>(255);

  std::iota(std::begin(data), std::end(data), 0);

  region.Load(data);

  REQUIRE(region.Read<uint8_t>(0)   == data[0]);
  REQUIRE(region.Read<uint8_t>(200) == data[200]);
  REQUIRE(region.Read<uint8_t>(254) == data[254]);
}
