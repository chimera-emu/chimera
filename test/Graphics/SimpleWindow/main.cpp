#include <LibGraphics/Window/Event.h>
#include <LibGraphics/Window/Keyboard.h>
#include <LibGraphics/Window/Window.h>

#include <iostream>

int main()
{
  Window window = Window{800, 600, "Simple Window"};

  while (window.IsOpen())
  {
    Event event;
    while (window.FetchEvent(event))
    {
      if (event.IsKeyboardEvent())
      {
        const std::string newTitle = std::string("Key Pressed: " +
            Keyboard::KeyCodeToString(event.keyboardEvent.code));

        window.SetTitle(newTitle);
      }
    }
  }
}
