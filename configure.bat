@ECHO OFF

set arg1=%1
set arg2=%2
set arg3=%3
set arg4=%4
set arg5=%5

set currentPath=%~dp0
set scriptsFolder=scripts\
set scriptName=%~n0

set "str=%currentPath%%scriptsFolder%%scriptName%"

PowerShell.exe -NoProfile -ExecutionPolicy Bypass -Command "& '%str%.ps1'" %arg1% %arg2% %arg3% %arg4% %arg5%