#include "PSX/Interrupts/InterruptController.h"

#include <Core/PSX/Logger.h>
#include <Core/PSX/Panic.h>

namespace Interrupts
{

void InterruptController::HandleWrite(const uint32_t address,
                                      const uint32_t value)
{
  switch (address)
  {
    case 0x1F801070: { WriteStatus(value); return; }
    case 0x1F801074: { WriteMask(value);   return; }
    default:
      panic_unreachable("Invalid address: [{:#x}]", address);
  }
}

uint32_t InterruptController::HandleRead(const uint32_t address) const
{
  switch (address)
  {
    case 0x1F801070: { return static_cast<uint32_t>(status_); }
    case 0x1F801074: { return static_cast<uint32_t>(mask_); }
    default:
      panic_unreachable("Invalid address: [{:#x}]", address);
  }
}

void InterruptController::Trigger(const InterruptChannel channel)
{
  status_ |= static_cast<uint16_t>(channel);
}

void InterruptController::WriteStatus(const uint32_t value)
{
  // Clear handled interrupt flags
  status_ &= static_cast<uint16_t>(value);
}

void InterruptController::WriteMask(const uint32_t value)
{
  mask_ = static_cast<uint16_t>(value);

  // Panic if any masks being written are not currently implemented.
  // TODO: Remove this when all interrupts are implemented
  static const uint16_t flags =
      static_cast<uint16_t>(InterruptChannel::VBLANK) |
      static_cast<uint16_t>(InterruptChannel::Dma) |
      static_cast<uint16_t>(InterruptChannel::CDROM);

  if ((mask_ & ~flags) != 0)
    throw std::runtime_error(fmt::format("Unhandled mask: {:x}", value));
}

bool InterruptController::IsActive() const
{
  return (status_ & mask_) != 0;
}

}
