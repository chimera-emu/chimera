#include "PSX/Machine.h"

#include <Core/PSX/Logger.h>

#include <Common/Constants.h>
#include <Common/ToString.h>
#include <Support/FileUtility.h>

#include <spdlog/spdlog.h>

#include <string>
#include <thread>

using namespace std::placeholders;

Machine::Machine(const std::string &biospath)
    : interrupts(std::make_unique<Interrupts::InterruptController>()),
      timers(std::make_unique<Timers::TimerController>()),
      memory_(std::make_unique<Memory::MemoryController>()),
      processor_(std::make_unique<Cpu::Processor>(interrupts.get(), memory_.get())),
      dma_(std::make_unique<Dma::DmaController>(interrupts.get(), memory_.get())),
      spu_(std::make_unique<Spu::SpuController>()),
      cdrom_(std::make_unique<Cdrom::CdromController>(interrupts.get())),
      gpu_(std::make_unique<Gpu::GpuController>(interrupts.get())),
      ports()
{
  Initialise(biospath);
}

void Machine::Run()
{
  processor_->Run();
}

void Machine::Initialise(const std::string& biospath)
{
  InitialiseLogger();
  InitialiseMemory(biospath);
}

void Machine::InitialiseLogger()
{
  static const auto log_name = std::string("output.log");

  std::ofstream out;
  out.open(log_name, std::ofstream::out | std::ofstream::trunc);
  out.close();

  std::vector<spdlog::sink_ptr> sinks;
  sinks.push_back(std::make_shared<spdlog::sinks::simple_file_sink_mt>(log_name, true));

  auto logger_mt = std::make_shared<spdlog::logger>("Emulator", begin(sinks), end(sinks));
  spdlog::register_logger(logger_mt);
}

void Machine::InitialiseMemory(const std::string& biospath)
{
  using namespace Memory;

  // TODO: Check the correctness of this
  memory_->AddMemoryRegion(        "RAM", 0x00000000,   8 * MB);
  memory_->AddMemoryRegion("EXPANSION-1", 0x1F000000,   8 * MB);
  memory_->AddMemoryRegion(  "MEMCTRL-1", 0x1F801000,  36 *  B);
  memory_->AddMemoryRegion(  "MEMCTRL-2", 0x1F801060,   4 *  B);
  memory_->AddMemoryRegion("EXPANSION-2", 0x1F802000,  66 *  B);
  memory_->AddMemoryRegion(    "Scratch", 0x1F800000,   1 * KB);
  memory_->AddMemoryRegion(       "BIOS", 0x1FC00000, 512 * KB);
  memory_->AddMemoryRegion(  "MEMCTRL-3", 0xFFFE0000, 512 *  B);

  // Load BIOS file into MemoryRegion
  memory_->LoadMemoryRegion(0x1FC00000, Utility::FileToByteArray(biospath));

  // Bind MMIO Handlers and regions
  memory_->AddMemoryRegion(MemoryRegion("DMA", 0x1F801080, 124 * B, *dma_));
  memory_->AddMemoryRegion(MemoryRegion("GPU", 0x1F801810,   8 * B, *gpu_));
  memory_->AddMemoryRegion(MemoryRegion("SPU", 0x1F801C00, 640 * B, *spu_));
  memory_->AddMemoryRegion(MemoryRegion("CDR", 0x1F801800,   4 * B, *cdrom_));
  memory_->AddMemoryRegion(MemoryRegion("PAD", 0x1F801040,  32 * B, ports));
  memory_->AddMemoryRegion(MemoryRegion("INT", 0x1F801070,   8 * B, *interrupts));
  memory_->AddMemoryRegion(MemoryRegion("TMR", 0x1F801100,  48 * B, *timers));

  // TODO: Remove this state dump.
  Logger()->info("Memory Controller: \n{}", ToString(*memory_));
}
