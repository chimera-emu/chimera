#include "PSX/Gpu/Types/DrawOffset.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const DrawOffset& offset)
{
  return os << "[" << offset.x_ << ", " << offset.y_ << "]";
}
