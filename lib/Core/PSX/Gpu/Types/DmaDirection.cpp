#include "PSX/Gpu/Types/DmaDirection.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, DmaDirection direction)
{
  switch (direction)
  {
    case DmaDirection::Off      : return os << "OFF";
    case DmaDirection::Fifo     : return os << "FIFO";
    case DmaDirection::CpuToGP0 : return os << "CPU TO GP0";
    case DmaDirection::GpuToCpu : return os << "GPU TO CPU";
  }

  return os << static_cast<uint32_t>(direction);
}
