#include "PSX/Gpu/Types/DisplayMode.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, HRes res)
{
  switch (res)
  {
    case HRes::x256 : return os << "256p";
    case HRes::x320 : return os << "320p";
    case HRes::x512 : return os << "512p";
    case HRes::x640 : return os << "640p";
  };

  return os << static_cast<uint32_t>(res);
}

std::ostream& operator<<(std::ostream& os, VRes res)
{
  switch (res)
  {
    case VRes::x240 : return os << "240p";
    case VRes::x480 : return os << "480p";
  };

  return os << static_cast<uint32_t>(res);
}

std::ostream& operator<<(std::ostream& os, VideoMode mode)
{
  switch (mode)
  {
    case VideoMode::NTSC : return os << "NTSC/60Hz";
    case VideoMode::PAL  : return os << "PAL/50Hz";
  }

  return os << static_cast<uint32_t>(mode);
}

std::ostream& operator<<(std::ostream& os, ColourDepth mode)
{
  switch (mode)
  {
    case ColourDepth::b15 : return os << "15 bit";
    case ColourDepth::b24 : return os << "24 bit";
  }

  return os << static_cast<uint32_t>(mode);
}

std::ostream& operator<<(std::ostream& os, HRes2 hres)
{
  switch (hres)
  {
    case HRes2::OFF  : return os << "OFF";
    case HRes2::x368 : return os << "368p";
  }

  return os << static_cast<uint32_t>(hres);
}

std::ostream& operator<<(std::ostream& os, VerticalInterlace interlace)
{
  switch (interlace)
  {
    case VerticalInterlace::OFF : return os << "OFF";
    case VerticalInterlace::ON  : return os << "ON";
  }

  return os << static_cast<uint32_t>(interlace);
}

std::ostream& operator<<(std::ostream& os, DisplayMode mode)
{
  os << "       HRES: " << mode.GetHorizontalResolution()    << "\n";
  os << "       VRES: " << mode.GetVerticalResolution()      << "\n";
  os << "       MODE: " << mode.GetVideoMode()               << "\n";
  os << "COLOR DEPTH: " << mode.GetColourDepth()             << "\n";
  os << "V INTERLACE: " << mode.GetVerticalInterlace()       << "\n";
  os << "      HRES2: " << mode.GetHorizontalResolutionExt();

  return os;
}
