#include "PSX/Gpu/Types/DisplayRange.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const DisplayRange& range)
{
  return os << "H: [Start: " << range.horizontalStart_
            << ", End: "     << range.horizontalEnd_ << "]  "
            << "V: [Start: " << range.verticalStart_
            << ", End: "     << range.verticalEnd_ << "]";
}
