#include "PSX/Gpu/Types/DrawArea.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const DrawArea& area)
{
  os << "[" << area.left_ << ", " << area.top_ << "]";
  os << " ";
  os << "[" << area.right_ << ", " << area.bottom_ << "]";

  return os;
}
