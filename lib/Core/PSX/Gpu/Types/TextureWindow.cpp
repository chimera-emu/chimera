#include "PSX/Gpu/Types/TextureWindow.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const TextureWindow& window)
{
  os << "Mask[" << +window.maskX_ << ", " << +window.maskY_ << "]";
  os << " ";
  os << "Offset[" << +window.offsetX_ << ", " << +window.offsetY_ << "]";

  return os;
}
