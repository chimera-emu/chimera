#include "PSX/Gpu/Types/DisplayArea.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const DisplayArea& area)
{
  return os << "[X: " << area.x_ << ", Y: " << area.y_ << "]";
}
