#include <PSX/Gpu/Types/DrawMode.h>

#include <ostream>

std::ostream& operator<<(std::ostream& os, const TexpageColours colours)
{
  switch (colours)
  {
    case TexpageColours::_4Bit:     { return os << "4 Bit";    }
    case TexpageColours::_8Bit:     { return os << "8 Bit";    }
    case TexpageColours::_15Bit:    { return os << "15 Bit";   }
    case TexpageColours::Reserved:  { return os << "Reserved"; }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const SemiTransparency value)
{
  switch (value)
  {
    case SemiTransparency::HBaddHF: { return os << "Half Back + Half Front";  }
    case SemiTransparency::FBaddFF: { return os << "Full Back + Full Front";  }
    case SemiTransparency::FBsubFF: { return os << "Full Back - Full Front";  }
    case SemiTransparency::FBaddQF: { return os << "Full Back + Quart Front"; }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const DitherSetting dither)
{
  switch (dither)
  {
    case DitherSetting::Disabled: { return os << "Disabled"; }
    case DitherSetting::Enabled:  { return os << "Enabled";  }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const DisplayAreaDraw drawArea)
{
  switch (drawArea)
  {
    case DisplayAreaDraw::Prohibited: { return os << "Prohibited"; }
    case DisplayAreaDraw::Allowed:    { return os << "Allowed";    }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const TextureDisabled textureState)
{
  switch (textureState)
  {
    case TextureDisabled::Enabled:  { return os << "Enabled";  }
    case TextureDisabled::Disabled: { return os << "Disabled"; }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const DrawMode mode)
{
  os << "Draw Mode Configuration:\n"
     << "Texture Base      : "
     << "[X: " << mode.TexpageX() << ", Y: " << mode.TexpageY() << "]\n"
     << "Semi Transparency : " << mode.GetSemiTransparency() << "\n"
     << "Texpage Colours   : " << mode.GetTexpageColours() << "\n"
     << "Dither Setting    : " << mode.GetDitherSetting() << "\n"
     << "Display Area Draw : " << mode.GetDisplayAreaDraw() << "\n"
     << "Texture Disabled? : " << mode.GetTextureDisabled() << "\n"
     << "Texture X Flipped : " << mode.TextureXFlipped() << "\n"
     << "Texture Y Flipped : " << mode.TextureYFlipped() << "\n";

  return os;
}
