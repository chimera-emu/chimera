#include "PSX/Gpu/GpuController.h"

#include <Core/PSX/Gpu/CommandBuffer.h>
#include <Core/PSX/Gpu/GpuState.h>
#include <Core/PSX/Gpu/ImageBuffer.h>

#include <Core/PSX/Panic.h>
#include <Core/PSX/Logger.h>

#include <Common/ToString.h>

#include <gsl/gsl_assert>

namespace Gpu
{

GpuController::GpuController(Interrupts::InterruptController* interrupts)
  : state(), current(0), type(CommandType::Command), buffer(), image(),
    interrupts_(interrupts)
{
  Expects(interrupts != nullptr);

  backend = VideoBackend::GetBackend();
}

uint32_t GpuController::HandleRead(const uint32_t address) const
{
  switch (address)
  {
    case 0x1F801810: { return HandleGP0Read(); }
    case 0x1F801814: { return HandleGP1Read(); }
    default:
      panic_unreachable("Invalid memory address [{:#x}]", address);
  }
}

uint32_t GpuController::HandleGP0Read() const
{
  Logger()->error("Unimplemented GPU GP0 Read");

  return 0x1C000000;
}

uint32_t GpuController::HandleGP1Read() const
{
  Logger()->error("Unimplemented GPU GP1 Read");

  return 0x1C000000;
}

void GpuController::HandleWrite(const uint32_t address, const uint32_t value)
{
  current = value;

  switch (address)
  {
    case 0x1F801810: { return HandleGP0Write(); }
    case 0x1F801814: { return HandleGP1Write(); }
    default:
      panic_unreachable("Invalid memory address [{:#x}]", address);
  }
}

void GpuController::HandleGP0Write()
{
  switch (type)
  {
    case CommandType::Command:   { HandleGP0Command();   break; }
    case CommandType::Parameter: { HandleGP0Parameter(); break; }
    case CommandType::ImageData: { HandleGP0ImageData(); break; }
  }

  if (type != CommandType::Command)
    return;

  const uint32_t command = buffer.GetCommand();

  switch (command)
  {
    case 0x00: { NoOperation();               break; }
    case 0x01: { ClearCache();                break; }
    case 0xA0: { LoadImageData();             break; }
    case 0xC0: { SaveImageData();             break; }
    case 0x28: { DrawMonochromeQuad();        break; }
    case 0x2C: { DrawTexturedQuad();          break; }
    case 0x30: { DrawShadedTriangle();        break; }
    case 0x38: { DrawShadedQuad();            break; }
    case 0xE1: { SetDrawMode();               break; }
    case 0xE2: { SetTextureWindow();          break; }
    case 0xE3: { SetDrawingAreaTopLeft();     break; }
    case 0xE4: { SetDrawingAreaBottomRight(); break; }
    case 0xE5: { SetDrawingOffset();          break; }
    case 0xE6: { SetMaskBit();                break; }
    default:
      panic("Unhandled GP0 command: [{:#x}]", command);
  }

  buffer.ClearBuffer();
  image.ClearBuffer();
}

void GpuController::HandleGP0Command()
{
  buffer.PushCommand(current);

  if (buffer.IsPending())
    type = CommandType::Parameter;
}

void GpuController::HandleGP0Parameter()
{
  buffer.PushCommand(current);

  if (buffer.IsPending())
    return;

  if (buffer.GetCommand() == 0xA0)
  {
    type = CommandType::ImageData;

    const auto position = buffer.GetWord(1);
    const uint16_t x = static_cast<uint16_t>(position);
    const uint16_t y = static_cast<uint16_t>(position >> 16);

    const auto resolution = buffer.GetWord(2);
    const uint16_t width  = static_cast<uint16_t>(resolution);
    const uint16_t height = static_cast<uint16_t>(resolution >> 16);

    image = ImageBuffer(x, y, width, height);
  }
  else
    type = CommandType::Command;
}

void GpuController::HandleGP0ImageData()
{
  image.PushData(current);

  if (!image.IsPending())
    type = CommandType::Command;
}


void GpuController::HandleGP1Write()
{
  const uint32_t command = (current >> 24);

  switch (command)
  {
    case 0x00: { ResetGpu();                  return; }
    case 0x01: { ResetCommandBuffer();        return; }
    case 0x02: { ResetInterrupt();            return; }
    case 0x03: { SetDisplayEnabled();         return; }
    case 0x04: { SetDmaDirection();           return; }
    case 0x05: { SetVramStartArea();          return; }
    case 0x06: { SetHorizontalDisplayRange(); return; }
    case 0x07: { SetVerticalDisplayRange();   return; }
    case 0x08: { SetDisplayMode();            return; }
    default:
      panic("Unhandled GP1 command: [{:#x}]", command);
  }
}


void GpuController::SetDrawMode()
{
  state.SetDrawMode(current);

  Logger()->info("Set Draw Mode: \n{}", ToString(state.GetDrawMode()));
}

// GP0(0x01)
void GpuController::ClearCache()
{
  Logger()->info("Clear TextureCache");
}

// GP0(0x28)
void GpuController::DrawMonochromeQuad()
{
  Logger()->info("Draw Monochrome Quadrilateral");

  auto positions = std::vector<Position>();
  positions.reserve(4);
  positions.emplace_back(buffer.GetWord(1));
  positions.emplace_back(buffer.GetWord(2));
  positions.emplace_back(buffer.GetWord(3));
  positions.emplace_back(buffer.GetWord(4));

  const auto colours = std::vector<Colour>(4, Colour(buffer.GetWord(0)));

  backend->PushQuad(positions, colours);
}

// GP0(0x2C)
void GpuController::DrawTexturedQuad()
{
  Logger()->info("Draw Textured Quadrilateral");

  auto positions = std::vector<Position>();
  positions.reserve(4);
  positions.emplace_back(buffer.GetWord(1));
  positions.emplace_back(buffer.GetWord(3));
  positions.emplace_back(buffer.GetWord(5));
  positions.emplace_back(buffer.GetWord(7));

  // TODO: Textures are currently not supported, anything textured is now green
  const auto colours = std::vector<Colour>(4, Colour(0xFF00FF00));

  backend->PushQuad(positions, colours);
}

// GP0(0x30)
void GpuController::DrawShadedTriangle()
{
  Logger()->info("Draw Shaded Triangle");

  auto positions = std::vector<Position>();
  positions.reserve(3);
  positions.emplace_back(buffer.GetWord(1));
  positions.emplace_back(buffer.GetWord(3));
  positions.emplace_back(buffer.GetWord(5));

  auto colours = std::vector<Colour>();
  colours.reserve(3);
  colours.emplace_back(buffer.GetWord(0));
  colours.emplace_back(buffer.GetWord(2));
  colours.emplace_back(buffer.GetWord(4));

  backend->PushTriangle(positions, colours);
}

// GP0(0x38)
void GpuController::DrawShadedQuad()
{
  Logger()->info("Draw Shaded Quadrilateral");

  auto positions = std::vector<Position>();
  positions.reserve(4);
  positions.emplace_back(buffer.GetWord(1));
  positions.emplace_back(buffer.GetWord(3));
  positions.emplace_back(buffer.GetWord(5));
  positions.emplace_back(buffer.GetWord(7));

  auto colours = std::vector<Colour>();
  colours.reserve(4);
  colours.emplace_back(buffer.GetWord(0));
  colours.emplace_back(buffer.GetWord(2));
  colours.emplace_back(buffer.GetWord(4));
  colours.emplace_back(buffer.GetWord(6));

  backend->PushQuad(positions, colours);
}

// GP0(0xA0)
void GpuController::LoadImageData()
{
  Logger()->info("Load Image Data");
  // Do Stuff
}

// GP0(0xC0)
void GpuController::SaveImageData()
{
  Logger()->info("Save Image Data");
  // Do Stuff
}

// GP0(0xE2)
void GpuController::SetTextureWindow()
{
  const uint8_t maskX   = (current & 0x1F);
  const uint8_t maskY   = ((current >>  5) & 0x1F);
  const uint8_t offsetX = ((current >> 10) & 0x1F);
  const uint8_t offsetY = ((current >> 15) & 0x1F);

  state.SetTextureWindow(maskX, maskY, offsetX, offsetY);

  Logger()->info("Set Texture Window:\n{}", ToString(state.GetTextureWindow()));
}

// GP0(0xE3)
void GpuController::SetDrawingAreaTopLeft()
{
  const uint32_t top  = ((current >> 10) & 0x3FF);
  const uint32_t left = (current & 0x3FF);

  state.SetDrawAreaTopLeft(top, left);

  Logger()->info("Set Drawing Area - TL: {}", ToString(state.GetDrawArea()));
}

// GP0(0xE4)
void GpuController::SetDrawingAreaBottomRight()
{
  const uint32_t bottom = ((current >> 10) & 0x3FF);
  const uint32_t right  = (current & 0x3FF);

  state.SetDrawAreaBottomRight(bottom, right);

  Logger()->info("Set Drawing Area - BR: {}", ToString(state.GetDrawArea()));
}

// GP0(0xE5)
void GpuController::SetDrawingOffset()
{
  const uint16_t y = ((current >> 11) & 0x7FF);
  const uint16_t x = (current & 0x7FF);

  // Sign extend the 11 bit current to 16 bits
  const int16_t offsetY = (y & 0x7FF) | ((y & 0x400) != 0 ? 0xF800 : 0);
  const int16_t offsetX = (x & 0x7FF) | ((x & 0x400) != 0 ? 0xF800 : 0);

  state.SetDrawOffset(offsetX, offsetY);

  Logger()->info("Set Drawing Offset: {}", ToString(state.GetDrawOffset()));

  interrupts_->Trigger(Interrupts::InterruptChannel::VBLANK);
  backend->Render();
}

// GP0(0xE6)
void GpuController::SetMaskBit()
{
  Logger()->info("Set Mask Bit");

  const bool texture  = (current & 0x01) != 0;
  const bool preserve = (current & 0x02) != 0;

  state.SetMaskBit(texture, preserve);
}

// GP1(0x00)
void GpuController::ResetGpu()
{
  Logger()->info("Reset GPU");

  state.Reset();
  ResetCommandBuffer();
}

// GP1(0x01)
void GpuController::ResetCommandBuffer()
{
  Logger()->info("Reset Command Buffer");

  buffer.ClearBuffer();

  type = CommandType::Command;
}

// GP1(0x02)
void GpuController::ResetInterrupt()
{
  Logger()->info("Reset Interrupt");

  state.ResetInterrupt();
}

// GP1(0x03)
void GpuController::SetDisplayEnabled()
{
  Logger()->info("Set Display Enabled");

  state.SetDisplayEnabled(current % 2 != 0);
}

// GP1(0x04)
void GpuController::SetDmaDirection()
{
  state.SetDmaDirection(current);

  Logger()->info("Set Dma Direction: ", ToString(state.GetDmaDirection()));
}

// GP1(0x05)
void GpuController::SetVramStartArea()
{
  const uint16_t startX = (current & 0x3FF);
  const uint16_t startY = ((current >> 10) & 0x1FF);

  state.SetDisplayArea(startX, startY);

  Logger()->info("Set Display Area:\n{}", ToString(state.GetDisplayArea()));
}

// GP1(0x06)
void GpuController::SetHorizontalDisplayRange()
{
  const uint16_t start = (current & 0xFFF);
  const uint16_t end   = ((current >> 12) & 0xFFF);

  state.SetHorizontalRange(start, end);

  Logger()->info("Set Display Range H:\n{}", ToString(state.GetDisplayRange()));
}

// GP1(0x07)
void GpuController::SetVerticalDisplayRange()
{
  Logger()->info("Set Display Range V:\n{}", ToString(state.GetDisplayRange()));

  const uint16_t start = (current & 0x3FF);
  const uint16_t end   = ((current >> 10) & 0x3FF);

  state.SetVerticalRange(start, end);
}

// GP1(0x08)
void GpuController::SetDisplayMode()
{
  state.SetDisplayMode(current);

  Logger()->info("Set Display Mode:\n{}", ToString(state.GetDisplayMode()));
}

} // namespace Gpu
