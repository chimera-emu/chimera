#include <Core/PSX/Gpu/CommandBuffer.h>

#include <Core/PSX/Panic.h>
#include <Core/PSX/Logger.h>

#include <gsl/gsl_assert>

#include <cstdint>

// TODO: Move this somewhere logical
namespace
{
  uint32_t GetGpuCommandSize(const uint32_t command)
  {
    const auto opcode = command >> 24;

    switch (opcode)
    {
      case 0x00: { return 1; }
      case 0x01: { return 1; }
      case 0x28: { return 5; }
      case 0x2C: { return 8; }
      case 0x30: { return 6; }
      case 0x38: { return 8; }
      case 0xA0: { return 3; }
      case 0xC0: { return 1; }
      case 0xE1: { return 1; }
      case 0xE2: { return 1; }
      case 0xE3: { return 1; }
      case 0xE4: { return 1; }
      case 0xE5: { return 1; }
      case 0xE6: { return 1; }
      default:
        panic("Undefined command size for command: {:#x}", opcode);
    }
  }
} // anonymous namespace

void CommandBuffer::PushCommand(const uint32_t command)
{
  if (buffer.empty())
    total = GetGpuCommandSize(command);

  buffer.push_back(command);
}

void CommandBuffer::ClearBuffer()
{
  buffer.clear();

  total = 0;
}

bool CommandBuffer::IsPending() const
{
  return (buffer.size() < total);
}

uint32_t CommandBuffer::GetCommand() const
{
  Expects(!buffer.empty());

  return (buffer[0] >> 24);
}
