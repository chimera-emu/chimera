#include "PSX/Gpu/GpuState.h"

void GpuState::Reset()
{
  drawMode    = DrawMode();
  displayMode = DisplayMode();
  direction   = DmaDirection::Off;

  drawArea = DrawArea();

  drawOffset = DrawOffset();

  textureWindow = TextureWindow();

  texturePixel  = false;
  preservePixel = false;

  displayArea  = DisplayArea();
  displayRange = DisplayRange();

  displayEnabled = false;
  interrupt      = false;
}

void GpuState::ResetInterrupt()
{
  interrupt = false;
}

void GpuState::SetDmaDirection(const uint32_t value)
{
  direction = DmaDirection(value & 0x03);
}

void GpuState::SetDrawAreaTopLeft(const uint32_t top, const uint32_t left)
{
  drawArea.top_  = top;
  drawArea.left_ = left;
}

void GpuState::SetDrawAreaBottomRight(const uint32_t bottom, const uint32_t right)
{
  drawArea.bottom_ = bottom;
  drawArea.right_  = right;
}

void GpuState::SetDrawOffset(const int16_t offsetX, const int16_t offsetY)
{
  drawOffset.x_ = offsetX;
  drawOffset.y_ = offsetY;
}

void GpuState::SetTextureWindow(const uint8_t maskX, const uint8_t maskY,
                                const uint8_t offsetX, const uint8_t offsetY)
{
  textureWindow.maskX_   = maskX;
  textureWindow.maskY_   = maskY;
  textureWindow.offsetX_ = offsetX;
  textureWindow.offsetY_ = offsetY;
}

void GpuState::SetMaskBit(const bool texture, const bool preserve)
{
  texturePixel  = texture;
  preservePixel = preserve;
}

void GpuState::SetDisplayArea(const uint16_t startX, const uint16_t startY)
{
  displayArea.x_ = startX;
  displayArea.y_ = startY;
}

void GpuState::SetHorizontalRange(const uint16_t start, const uint16_t end)
{
  displayRange.horizontalStart_ = start;
  displayRange.horizontalEnd_   = end;
}

void GpuState::SetVerticalRange(const uint16_t start, const uint16_t end)
{
  displayRange.verticalStart_ = start;
  displayRange.verticalEnd_   = end;
}

void GpuState::SetDisplayEnabled(const bool enabled)
{
  displayEnabled = enabled;
}
