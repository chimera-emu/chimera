#include "PSX/Dma/DmaController.h"

#include <Core/PSX/Dma/Channel.h>
#include <Core/PSX/Dma/ChannelIndex.h>

#include <Core/PSX/Interrupts/InterruptController.h>
#include <Core/PSX/Memory/MemoryController.h>
#include <Core/PSX/Panic.h>

#include <gsl/gsl_assert>

namespace Dma
{
DmaController::DmaController(Interrupts::InterruptController* interrupts,
                             Memory::MemoryController* memory)
    : control_(0), interrupt_(0), channels_(), interrupts_(interrupts),
      memory_(memory)
{
  Expects(memory != nullptr);
  Expects(interrupts != nullptr);
}

void DmaController::HandleWrite(const uint32_t address, const uint32_t value)
{
  // Write value is aligned to 32 bits
  const auto alignedValue = (value << ((address % 4) * 8));

  // The channel we're writing to (7 would be the dma controller itself)
  const uint32_t channel = (address & 0x7F) >> 4;

  if (channel == 0x07)
    WriteControlData(address, alignedValue);
  else
    WriteChannelData(address, alignedValue);
}



uint32_t DmaController::HandleRead(const uint32_t address) const
{
  // The channel we're writing to (7 would be the dma controller itself)
  const uint32_t channel = (address & 0x7F) >> 4;

  uint32_t value = 0;

  if (channel == 0x07)
    value = ReadControlData(address);
  else
    value = ReadChannelData(address);

  value >>= (address % 4) * 8;

  return value;
}



void DmaController::WriteControlData(const uint32_t addr, const uint32_t value)
{
  switch (addr & 0x0C)
  {
    case 0x00: { return SetControl(value);   }
    case 0x04: { return SetInterrupt(value); }
    default:
      panic_unreachable("Invalid memory address [{:#x}]", addr);
  }
}


void DmaController::WriteChannelData(const uint32_t addr, const uint32_t value)
{
  const uint32_t channel = (addr & 0x7F) >> 4;

  switch (addr & 0x0C)
  {
    case 0x00: { channels_[channel].SetBaseAddress(value);    break; }
    case 0x04: { channels_[channel].SetBlockControl(value);   break; }
    case 0x08: { channels_[channel].SetChannelControl(value); break; }
    default:
      panic_unreachable("Invalid memory address [{:#x}]", addr);
  }

  if (channels_[channel].GetChannelControl().IsActive())
    ExecuteTransfer(channel);
}


void DmaController::SetControl(const uint32_t value)
{
  control_.Set(value);
}


// TODO: The logic for this should probably be in the InterruptRegister class.
//       It's here now because it requires access to the InterruptController.
void DmaController::SetInterrupt(const uint32_t value)
{
  const bool previousState = interrupt_.IsInterruptPending();

  // Writing a '1' to a flag clears the flag bit.
  interrupt_ = InterruptRegister(value & ~(value & 0x7F000000));

  const bool currentState = interrupt_.IsInterruptPending();

  // Interrupt only triggers on the rising edge of bit[31]
  if (!previousState && currentState)
    interrupts_->Trigger(Interrupts::InterruptChannel::Dma);
}

void DmaController::ExecuteTransfer(const uint32_t channel)
{
  Channel& current = channels_[channel];

  if (current.GetChannelControl().GetSyncMode() == SyncMode::LinkedList)
    TransferList(channel);
  else
    TransferBlock(channel);

  FinaliseTransfer(channel);
}


void DmaController::TransferBlock(const uint32_t channel)
{
  Channel& current = channels_[channel];

  auto address    = current.GetBaseAddress();
  const auto step = current.GetChannelControl().GetStepValue();

  for (int i = current.GetBlockControl().GetBlockSize(); i > 0; --i)
  {
    if (current.GetChannelControl().GetDirection() == Direction::ToRam)
    {
      uint32_t value = 0;

      switch (ChannelIndex(channel))
      {
        case ChannelIndex::OTC:
        {
          value = (i == 1 ? 0xFFFFFFFF : ((address - 4) & 0x001FFFFF));
          break;
        }
        case ChannelIndex::MDEC_IN:
        case ChannelIndex::MDEC_OUT:
        case ChannelIndex::GPU:
        case ChannelIndex::CDROM:
        case ChannelIndex::SPU:
        case ChannelIndex::PIO:
          panic("Unhandled 'To RAM' channel: {:#x}", channel);
      }

      memory_->Write<uint32_t>(address, value);
    }
    else
    {
      uint32_t value = memory_->Read<uint32_t>(address);

      switch (ChannelIndex(channel))
      {
        case ChannelIndex::GPU:
        {
          // TODO: Get rid of this magic number
          memory_->Write<uint32_t>(0x1F801810, value);
          break;
        }
        case ChannelIndex::MDEC_IN:
        case ChannelIndex::MDEC_OUT:
        case ChannelIndex::CDROM:
        case ChannelIndex::SPU:
        case ChannelIndex::PIO:
        case ChannelIndex::OTC:
          panic("Unhandled 'From RAM' channel: {:#x}", channel);
      }
    }

    address += step;
  }
}


void DmaController::TransferList(const uint32_t channel)
{
  Channel& current = channels_[channel];

  uint32_t address = current.GetBaseAddress();

  while (true)
  {
    const uint32_t header = memory_->Read<uint32_t>(address);

    for (uint32_t i = header >> 24; i > 0; --i)
    {
      address += 4;

      const uint32_t command = memory_->Read<uint32_t>(address);

      // TODO: Get rid of this magic number for GPU command register.
      memory_->Write<uint32_t>(0x1F801810, command);
    }

    if ((header & 0x800000) != 0)
      break;

    address = header & 0x1FFFFC;
  }
}


void DmaController::FinaliseTransfer(const uint32_t channel)
{
  channels_[channel].GetChannelControl().FinaliseTransfer();

  const bool previousState = interrupt_.IsInterruptPending();

  if (interrupt_.IsChannelEnabled(static_cast<ChannelIndex>(channel)))
    interrupt_.SetChannelFlag(static_cast<ChannelIndex>(channel));

  const bool currentState = interrupt_.IsInterruptPending();

  // Interrupt only triggers on the rising edge of bit[31]
  if (!previousState && currentState)
    interrupts_->Trigger(Interrupts::InterruptChannel::Dma);
}



uint32_t DmaController::ReadControlData(const uint32_t address) const
{
  switch (address & 0x0C)
  {
    case 0x00: { return control_.Get();   }
    case 0x04: { return interrupt_.Get(); }
    default:
      panic_unreachable("Invalid memory address [{:#x}]", address);
  }
}

uint32_t DmaController::ReadChannelData(const uint32_t address) const
{
  const uint32_t channel = (address & 0x7F) >> 4;

  switch (address & 0x0C)
  {
    case 0x00: { return channels_[channel].GetBaseAddress(); }
    case 0x04: { return channels_[channel].GetBlockControl().Get(); }
    case 0x08: { return channels_[channel].GetChannelControl().Get(); }
    default:
      panic_unreachable("Invalid memory address [{:#x}]", address);
  }
}

} // namespace Dma
