#include "PSX/Peripherals/PeripheralController.h"

#include <Core/PSX/Panic.h>
#include <Core/PSX/Logger.h>

void PeripheralController::HandleWrite(const uint32_t address,
                                       const uint32_t value)
{
  Logger()->info("PERIPHERAL Write: [{:#x}] <- {:#x}", address, value);

  switch (address)
  {
    case 0x1F801040: { WriteJoypadData(value);     break; }
    case 0x1F801044: { WriteJoypadStatus(value);   break; }
    case 0x1F801048: { WriteJoypadMode(value);     break; }
    case 0x1F80104A: { WriteJoypadControl(value);  break; }
    case 0x1F80104E: { WriteJoypadBaudrate(value); break; }
    default:
      panic("Unhandled peripheral write: [{:#x}]", address);
  }
}

uint32_t PeripheralController::HandleRead(const uint32_t address) const
{
  Logger()->info("PERIPHERAL Read: [{:#x}]", address);

  switch (address)
  {
    case 0x1F801040: { ReadJoypadData();     break; }
    case 0x1F801044: { ReadJoypadStatus();   break; }
    case 0x1F801048: { ReadJoypadMode();     break; }
    case 0x1F80104A: { ReadJoypadControl();  break; }
    case 0x1F80104E: { ReadJoypadBaudrate(); break; }
    default:
      panic("Unhandled peripheral read: [{:#x}]", address);
  }

  return 0;
}

void PeripheralController::WriteJoypadData(const uint32_t value)
{
  Logger()->info("PERIPHERAL WriteJoypadData(): [{:#x}]", value);
}

void PeripheralController::WriteJoypadStatus(const uint32_t value)
{
  Logger()->info("PERIPHERAL WriteJoypadStatus(): [{:#x}]", value);
}

void PeripheralController::WriteJoypadMode(const uint32_t value)
{
  Logger()->info("PERIPHERAL WriteJoypadMode(): [{:#x}]", value);
}

void PeripheralController::WriteJoypadControl(const uint32_t value)
{
  Logger()->info("PERIPHERAL WriteJoypadControl(): [{:#x}]", value);
}

void PeripheralController::WriteJoypadBaudrate(const uint32_t value)
{
  Logger()->info("PERIPHERAL WriteJoypadBaudrate(): [{:#x}]", value);
}

uint32_t PeripheralController::ReadJoypadData() const
{
  Logger()->info("PERIPHERAL ReadJoypadData()");

  return 0;
}

uint32_t PeripheralController::ReadJoypadStatus() const
{
  Logger()->info("PERIPHERAL ReadJoypadStatus()");

  return 0;
}

uint32_t PeripheralController::ReadJoypadMode() const
{
  Logger()->info("PERIPHERAL ReadJoypadMode()");

  return 0;
}

uint32_t PeripheralController::ReadJoypadControl() const
{
  Logger()->info("PERIPHERAL ReadJoypadControl()");

  return 0;
}

uint32_t PeripheralController::ReadJoypadBaudrate() const
{
  Logger()->info("PERIPHERAL ReadJoypadBaudrate()");

  return 0;
}
