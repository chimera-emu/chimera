#include "PSX/Cpu/CpuState.h"

namespace Cpu
{

CpuState::CpuState()
  : registers_{{0}}, hi_{0}, lo_{0}, load_{std::make_pair(0, 0)},
    save_{std::make_pair(0, 0)}, curr_{0xBFC00000}, next_{curr_ + 4},
    branch_{false} {}


void CpuState::Write(const uint32_t index, const uint32_t value)
{
  // The zero index register is always tied to 0. The CPU can issue writes to
  // this index, but the writes should be ignored.
  if (index != 0 || value == 0)
    save_ = std::make_pair(index, value);
}


void CpuState::SetHiRegister(const Register value) { hi_ = value; }
void CpuState::SetLoRegister(const Register value) { lo_ = value; }


void CpuState::SetPC(const Register value)     { curr_ = value; }
void CpuState::SetNextPC(const Register value) { next_ = value; }


void CpuState::SetLoad(const RegisterIndex index, const Register value)
{
  load_ = std::make_pair(index, value);
}

void CpuState::SetSave(const RegisterIndex index, const Register value)
{
  save_ = std::make_pair(index, value);
}

void CpuState::SetBranch(const bool value) { branch_ = value; }


void CpuState::ExecuteLoadDelay()
{
  registers_[load_.first] = load_.second;
  registers_[save_.first] = save_.second;

  load_ = std::make_pair(0, 0);
  save_ = std::make_pair(0, 0);
}


void CpuState::UpdatePC()
{
  curr_ = next_;
  next_ += 4;
}

void CpuState::Reset()
{
  registers_.fill(0);

  hi_ = 0;
  lo_ = 0;

  load_ = std::make_pair(0, 0);
  save_ = std::make_pair(0, 0);

  curr_ = 0xBFC00000;
  next_ = curr_ + 4;

  branch_ = false;
}


uint32_t CpuState::Read(const RegisterIndex index) const
{
  return registers_[index];
}

int32_t CpuState::ReadSigned(const RegisterIndex index) const
{
  return static_cast<int32_t>(Read(index));
}

uint32_t CpuState::LoRegister() const { return lo_; }
uint32_t CpuState::HiRegister() const { return hi_; }

uint32_t CpuState::PC() const { return curr_; }
uint32_t CpuState::NextPC() const { return next_; }


uint32_t CpuState::GetLoad(const RegisterIndex index) const
{
  return (load_.first == index ? load_.second : 0);
}

uint32_t CpuState::GetSave(const RegisterIndex index) const
{
  return (save_.first == index ? save_.second : 0);
}

bool CpuState::InBranch() const { return branch_; }


} // namespace Cpu
