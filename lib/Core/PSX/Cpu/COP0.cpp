#include "PSX/Cpu/COP0.h"

#include <Core/PSX/Cpu/Types/COP0Index.h>
#include <Core/PSX/Interrupts/InterruptController.h>

#include <gsl/gsl>

namespace Cpu
{

COP0::COP0(Interrupts::InterruptController* interrupts)
  : interrupts_{interrupts}, registers_{0}
{
  Expects(interrupts != nullptr);
}

void COP0::HandleWrite(const uint32_t index, const uint32_t value)
{
  switch (index)
  {
    case COP0Index::Status: { status_.Set(value); return; }
    case COP0Index::Cause:  {  cause_.Set(value); return; }
    default:
      gsl::at(registers_, index) = value;
  }
}

uint32_t COP0::HandleRead(const uint32_t index) const
{
  Expects(index < registers_.size());

  switch (index)
  {
    case COP0Index::Status: { return status_.Get(); }
    case COP0Index::Cause:
    {
      // Bit 10 in the cause register is one of the hardware interrupt flags.
      // It is set depending on the state of the interrupt controller. If the
      // controller is active i.e has an interrupt pending ((flags & mask) != 0)
      // the bit is set. The other hardware interrupt flags 11/12 aren't used.
      return cause_.Get() | (static_cast<uint32_t>(interrupts_->IsActive()) << 10);
    }
    default:
      return registers_[index];
  }
}

uint32_t COP0::EnterException(const ExceptionCode code, const uint32_t pc,
                              const bool branch)
{
  // Shift the interrupt state stack left when we enter an exception, enables
  // exception recursion to a depth of 3
  status_.ShiftInterruptStateLeft();

  // Update the cause register by overwriting the existing exception code with
  // the current one.
  cause_.SetExceptionCode(code);

  if (branch)
  {
    SetEPCRegister(pc - 4);
    cause_.SetBranchDelay(true);
  }
  else
  {
    SetEPCRegister(pc);
    cause_.SetBranchDelay(false);
  }

  // The location of the exception handler depends on if the status register
  // says we're still in bootstrap mode.
  const uint32_t handler = status_.IsBootstrapMode() ? 0xBFC00180 : 0x80000080;

  return handler;
}

void COP0::ReturnFromException()
{
  status_.ShiftInterruptStateRight();
}

bool COP0::IsInterruptPending() const
{
  if (!status_.IsCurrentInterruptEnabled())
    return false;

  const auto value = HandleRead(13);

  return ((value & status_.Get()) & 0x700) != 0;
}

} // namespace Cpu
