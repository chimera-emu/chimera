#include "PSX/Cpu/Instruction.h"

#include <fmt/format.h>

namespace Cpu
{

std::string Instruction::ToString() const
{
  switch (Opcode())
  {
    case 0x00:
      return FormatExtInstruction();

    case 0x01:
    {
      const bool grt = ((data_ >> 16) & 1) != 0;
      const bool lnk = ((data_ >> 20) & 1) != 0;

      auto name = fmt::format("b{}z{}", (grt ? "ge" : "lt"), (lnk ? "al" : ""));

      return fmt::format("{} ${:0>2}, {}",
                         name, RS(), ImmediateSE() << 2);
    }
    case 0x02:
      return fmt::format("{} (PC & 0xf0000000) | {:x}",
                         "j", ImmediateJump());
    case 0x03:
      return fmt::format("{} (PC & 0xf0000000) | {:x}",
                         "jal", ImmediateJump());
    case 0x04:
      return fmt::format("{} ${:0>2}, ${:0>2}, {}",
                         "beq", RS(), RT(), ImmediateSE() << 2);
    case 0x05:
      return fmt::format("{} ${:0>2}, ${:0>2}, {}",
                         "bne", RS(), RT(), ImmediateSE() << 2);
    case 0x06:
      return fmt::format("{} ${:0>2}, {}",
                         "blez", RS(), ImmediateSE() << 2);
    case 0x07:
      return fmt::format("{} ${:0>2}, {}",
                         "bgtz", RS(), ImmediateSE() << 2);
    case 0x08:
      return fmt::format("{} ${:0>2}, ${:0>2}, {:0>#x}",
                         "addi", RT(), RS(), ImmediateSU());
    case 0x09:
      return fmt::format("{} ${:0>2}, ${:0>2}, {:0>#x}",
                         "addiu", RT(), RS(), ImmediateSU());
    case 0x0a:
      return fmt::format("{} ${:0>2}, ${:0>2}, {:0>#x}",
                         "slti", RT(), RS(), ImmediateSE());
    case 0x0b:
      return fmt::format("{} ${:0>2}, ${:0>2}, {:0>#x}",
                         "sltiu", RT(), RS(), ImmediateSU());
    case 0x0c:
      return fmt::format("{} ${:0>2}, ${:0>2}, {:0>#x}",
                         "andi", RT(), RS(), Immediate());
    case 0x0d:
      return fmt::format("{} ${:0>2}, ${:0>2}, {:0>#x}",
                         "ori", RT(), RS(), Immediate());
    case 0x0f:
      return fmt::format("{} ${:0>2}, {:0>#x}",
                         "lui", RT(), Immediate());
    case 0x10:
      return FormatCOP0Instruction();

    case 0x20:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lb", RT(), RS(), ImmediateSU());
    case 0x21:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lh", RT(), RS(), ImmediateSU());
    case 0x22:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lwl", RT(), RS(), ImmediateSE());
    case 0x23:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lw", RT(), RS(), ImmediateSE());
    case 0x24:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lbu", RT(), RS(), ImmediateSE());
    case 0x25:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lhu", RT(), RS(), ImmediateSE());
    case 0x26:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "lwr", RT(), RS(), ImmediateSE());
    case 0x28:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "sb", RT(), RS(), ImmediateSE());
    case 0x29:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "sh", RT(), RS(), ImmediateSE());
    case 0x2a:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "swl", RT(), RS(), ImmediateSE());
    case 0x2b:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "sw", RT(), RS(), ImmediateSE());
    case 0x2e:
      return fmt::format("{} ${:0>2}, [${:0>2} + {:0>#x}]",
                         "swr", RT(), RS(), ImmediateSE());
    default:
      return fmt::format("Invalid instruction: {:x}", Opcode());
  }
}

std::string Instruction::FormatExtInstruction() const
{
  switch (Function())
  {
    case 0x00:
      return fmt::format("{} ${:0>2}, ${:0>2} << {}",
                         "sll", RD(), RT(), Shift());
    case 0x02:
      return fmt::format("{} ${:0>2}, ${:0>2} >> {}",
                         "srl", RD(), RT(), Shift());
    case 0x03:
      return fmt::format("{} ${:0>2}, ${:0>2} >> {}",
                         "sra", RD(), RT(), Shift());
    case 0x04:
      return fmt::format("{} ${:0>2}, ${:0>2} << {:0>2}",
                         "sllv", RD(), RT(), RS());
    case 0x06:
      return fmt::format("{} ${:0>2}, ${:0>2} >> {:0>2}",
                         "srlv", RD(), RT(), RS());
    case 0x07:
      return fmt::format("{} ${:0>2}, ${:0>2} >> {:0>2}",
                         "srav", RD(), RT(), RS());
    case 0x08:
      return fmt::format("{} ${:0>2}",
                         "jr", RS());
    case 0x09:
      return fmt::format("{} ${:0>2}, ${:0>2}",
                         "jalr", RD(), RS());
    case 0x0C:
      return fmt::format("{}"
                         "syscall");
    case 0x10:
      return fmt::format("{} ${:0>2}",
                         "mfhi", RD());
    case 0x11:
      return fmt::format("{} ${:0>2}",
                         "mthi", RS());
    case 0x12:
      return fmt::format("{} ${:0>2}",
                         "mflo", RD());
    case 0x13:
      return fmt::format("{} ${:0>2}",
                         "mtlo", RS());
    case 0x18:
      return fmt::format("{} ${:0>2}, ${:0>2}",
                         "mult", RS(), RT());
    case 0x19:
      return fmt::format("{} ${:0>2}, ${:0>2}",
                         "multu", RS(), RT());
    case 0x1A:
      return fmt::format("{} ${:0>2}, ${:0>2}",
                         "div", RS(), RT());
    case 0x1B:
      return fmt::format("{} ${:0>2}, ${:0>2}",
                         "divu", RS(), RT());
    case 0x20:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "add", RD(), RS(), RT());
    case 0x21:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "addu", RD(), RS(), RT());
    case 0x22:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "sub", RD(), RS(), RT());
    case 0x23:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "subu", RD(), RS(), RT());
    case 0x24:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "and", RD(), RS(), RT());
    case 0x25:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "or", RD(), RS(), RT());
    case 0x26:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "xor", RD(), RS(), RT());
    case 0x27:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "nor", RD(), RS(), RT());
    case 0x2A:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "slt", RD(), RS(), RT());
    case 0x2B:
      return fmt::format("{} ${:0>2}, ${:0>2}, ${:0>2}",
                         "sltu", RD(), RS(), RT());
    default:
      return fmt::format("Invalid instruction: {:x}", Function());
  }
}

std::string Instruction::FormatCOP0Instruction() const
{
  switch (COP0Function())
  {
    case 0x00:
      return fmt::format("{} ${:0>2}, cop0r{:}",
                         "mfc0", RT(), RD());
    case 0x04:
      return fmt::format("{} ${:0>2}, cop0r{:}",
                         "mtc0", RT(), RD());
    case 0x10:
      return fmt::format("{}",
                         "rfe");
    default:
      return fmt::format("Invalid instruction: {:x}", Function());
  }
}

} // namespace Cpu
