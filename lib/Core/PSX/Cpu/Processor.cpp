#include "PSX/Cpu/Processor.h"

#include <Core/PSX/Cpu/Instruction.h>

#include <Core/PSX/Memory/MemoryController.h>
#include <Core/PSX/Panic.h>
#include <Support/MathUtility.h>

#include <fmt/format.h>
#include <gsl/gsl_assert>

namespace Cpu
{

Processor::Processor(Interrupts::InterruptController* interrupts,
                     Memory::MemoryController* memory)
    : state_{}, cop0_{interrupts}, pc_{0}, memory_{memory}
{
  Expects(memory != nullptr);
  Expects(interrupts != nullptr);
}

void Processor::Run()
{
  for (;;)
    Step();
}

void Processor::Step()
{
  pc_ = state_.PC();

  if ((pc_ & 0x03) != 0)
    return HandleException(ExceptionCode::LoadAddressError);

  const auto instruction = FetchInstruction(pc_);

  state_.UpdatePC();

  if (cop0_.IsInterruptPending())
    HandleException(ExceptionCode::ExternalInterrupt);
  else
    DecodeInstruction(instruction);

  state_.ExecuteLoadDelay();
  state_.SetBranch(false);
}

void Processor::Reset()
{
  // TODO: Implement the reset
}

Instruction Processor::FetchInstruction(const uint32_t address)
{
  return Instruction(memory_->Read<uint32_t>(address));
}

void Processor::DecodeInstruction(const Instruction instr)
{
  switch (instr.Opcode())
  {
    case 0x00: { DecodeExtInstruction(instr);           return; }
    case 0x01: { BranchInstruction(instr);              return; }
    case 0x02: { Jump(instr);                           return; }
    case 0x03: { JumpAndLink(instr);                    return; }
    case 0x04: { BranchOnEqual(instr);                  return; }
    case 0x05: { BranchOnNotEqual(instr);               return; }
    case 0x06: { BranchOnLTEZero(instr);                return; }
    case 0x07: { BranchOnGTZero(instr);                 return; }
    case 0x08: { AddImmediate(instr);                   return; }
    case 0x09: { AddImmediateUnsigned(instr);           return; }
    case 0x0A: { SetOnLessThanImmediate(instr);         return; }
    case 0x0B: { SetOnLessThanImmediateUnsigned(instr); return; }
    case 0x0C: { BitwiseAndImmediate(instr);            return; }
    case 0x0D: { BitwiseOrImmediate(instr);             return; }
    case 0x0E: { BitwiseXorImmediate(instr);            return; }
    case 0x0F: { LoadUpperImmediate(instr);             return; }
    case 0x10: { DecodeCOP0Instruction(instr);          return; }
    case 0x11: { DecodeCOP1Instruction();               return; }
    case 0x12: { DecodeCOP2Instruction(instr);          return; }
    case 0x13: { DecodeCOP3Instruction();               return; }
    case 0x20: { LoadByte(instr);                       return; }
    case 0x21: { LoadHalfWord(instr);                   return; }
    case 0x22: { LoadWordLeft(instr);                   return; }
    case 0x23: { LoadWord(instr);                       return; }
    case 0x24: { LoadByteUnsigned(instr);               return; }
    case 0x25: { LoadHalfWordUnsigned(instr);           return; }
    case 0x26: { LoadWordRight(instr);                  return; }
    case 0x28: { StoreByte(instr);                      return; }
    case 0x29: { StoreHalfWord(instr);                  return; }
    case 0x2A: { StoreWordLeft(instr);                  return; }
    case 0x2B: { StoreWord(instr);                      return; }
    case 0x2E: { StoreWordRight(instr);                 return; }
    case 0x30: { LoadWordCoprocessor0();                return; }
    case 0x31: { LoadWordCoprocessor1();                return; }
    case 0x32: { LoadWordCoprocessor2();                return; }
    case 0x33: { LoadWordCoprocessor3();                return; }
    case 0x38: { StoreWordCoprocessor0();               return; }
    case 0x39: { StoreWordCoprocessor1();               return; }
    case 0x3A: { StoreWordCoprocessor2();               return; }
    case 0x3B: { StoreWordCoprocessor3();               return; }
  }

  HandleException(ExceptionCode::ReservedInstruction);
}

void Processor::DecodeExtInstruction(const Instruction instr)
{
  switch (instr.Function())
  {
    case 0x00: { ShiftLeftLogical(instr);        return; }
    case 0x02: { ShiftRightLogical(instr);       return; }
    case 0x03: { ShiftRightArithmetic(instr);    return; }
    case 0x04: { ShiftLeftLogicalVar(instr);     return; }
    case 0x06: { ShiftRightLogicalVar(instr);    return; }
    case 0x07: { ShiftRightArithmeticVar(instr); return; }
    case 0x08: { JumpRegister(instr);            return; }
    case 0x09: { JumpAndLinkRegister(instr);     return; }
    case 0x0C: { SystemCall();                   return; }
    case 0x0D: { Break();                        return; }
    case 0x10: { MoveFromHI(instr);              return; }
    case 0x11: { MoveToHI(instr);                return; }
    case 0x12: { MoveFromLO(instr);              return; }
    case 0x13: { MoveToLO(instr);                return; }
    case 0x18: { Multiply(instr);                return; }
    case 0x19: { MultiplyUnsigned(instr);        return; }
    case 0x1A: { Divide(instr);                  return; }
    case 0x1B: { DivideUnsigned(instr);          return; }
    case 0x20: { AddRegisters(instr);            return; }
    case 0x21: { AddUnsigned(instr);             return; }
    case 0x22: { Subtract(instr);                return; }
    case 0x23: { SubtractUnsigned(instr);        return; }
    case 0x24: { BitwiseAnd(instr);              return; }
    case 0x25: { BitwiseOr(instr);               return; }
    case 0x26: { BitwiseXor(instr);              return; }
    case 0x27: { BitwiseNor(instr);              return; }
    case 0x2A: { SetOnLessThan(instr);           return; }
    case 0x2B: { SetOnLessThanUnsigned(instr);   return; }
  }

  HandleException(ExceptionCode::ReservedInstruction);
}

void Processor::DecodeCOP0Instruction(const Instruction instr)
{
  auto opcode = instr.COP0Function();

  switch (opcode)
  {
    case 0x00: { MoveFromCoprocessor0(instr); return; }
    case 0x04: { MoveToCoprocessor0(instr);   return; }
    case 0x10: { ReturnFromException();       return; }
    default:
      panic("Unhandled COP0 instruction: {:#x}", opcode);
  }
}

void Processor::DecodeCOP1Instruction()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

void Processor::DecodeCOP2Instruction(const Instruction instr)
{
  panic("Unhandled COP2 opcode: {:#x}", instr.COP0Function());
}

void Processor::DecodeCOP3Instruction()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

void Processor::HandleException(const ExceptionCode code)
{
  const uint32_t handler = cop0_.EnterException(code, pc_, state_.InBranch());

  state_.SetPC(handler);
  state_.SetNextPC(handler + 4);
}

// BGEZ   -- Branch on GTE zero          (bit[16] == 1 && bit[20] == 0)
// BLTZ   -- Branch on  LT zero          (bit[16] == 0 && bit[20] == 0)
// BGEZAL -- Branch on GTE zero and link (bit[16] == 1 && bit[20] == 1)
// BLTZAL -- Branch on  LT zero and link (bit[16] == 0 && bit[20] == 1)
void Processor::BranchInstruction(const Instruction instr)
{
  const int32_t value = state_.ReadSigned(instr.RS());
  const uint32_t data = instr.Get();

  const bool greater  = ((data >> 16) & 1) != 0;
  const bool linked   = ((data >> 20) & 1) != 0;

  if (greater && value < 0)
    return;

  if (!greater && value >= 0)
    return;

  const uint32_t branch = instr.ImmediateSU();
  const uint32_t offset = branch << 2;

  state_.SetNextPC(state_.PC() + offset);
  state_.SetBranch(true);

  if (linked)
    state_.Write(31, state_.PC() + 4);
}

// J -- Jump (j target)
// Jumps to the calculated address [nPC = (PC & 0xf0000000) | (target << 2);]
void Processor::Jump(const Instruction instr)
{
  const uint32_t address = instr.ImmediateJump();
  const uint32_t target  = (state_.PC() & 0xF0000000) | address;

  state_.SetNextPC(target);
  state_.SetBranch(true);
}

// JAL -- Jump and link (jal target)
// Jumps to the calculated address and stores the return address in $31
void Processor::JumpAndLink(const Instruction instr)
{
  const uint32_t address = instr.ImmediateJump();
  const uint32_t target  = (state_.PC() & 0xF0000000) | address;

  state_.Write(31, state_.PC() + 4);
  state_.SetNextPC(target);
  state_.SetBranch(true);
}

// BEQ -- Branch on equal (beq $s, $t, offset)
// Branches if the two registers are equal
void Processor::BranchOnEqual(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  if (valueS != valueT)
    return;

  const uint32_t branch = instr.ImmediateSU();
  const uint32_t offset = branch << 2;

  state_.SetNextPC(state_.PC() + offset);
  state_.SetBranch(true);
}

// BNE -- Branch on not equal (bne $s, $t, offset)
// Branches if the two registers are not equal
void Processor::BranchOnNotEqual(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  if (valueS == valueT)
    return;

  const uint32_t branch = instr.ImmediateSU();
  const uint32_t offset = branch << 2;

  state_.SetNextPC(state_.PC() + offset);
  state_.SetBranch(true);
}

// BLEZ -- Branch on less than or equal to zero (blez $s, offset)
// Branches if the register is greater than zero
void Processor::BranchOnLTEZero(const Instruction instr)
{
  const int32_t value = state_.ReadSigned(instr.RS());

  if (value > 0)
    return;

  const uint32_t branch = instr.ImmediateSU();
  const uint32_t offset = branch << 2;

  state_.SetNextPC(state_.PC() + offset);
  state_.SetBranch(true);
}

// BGTZ -- Branch on greater than zero (bgtz $s, offset)
// Branches if the register is greater than zero
void Processor::BranchOnGTZero(const Instruction instr)
{
  const int32_t value = state_.ReadSigned(instr.RS());

  if (value <= 0)
    return;

  const uint32_t branch = instr.ImmediateSU();
  const uint32_t offset = branch << 2;

  state_.SetNextPC(state_.PC() + offset);
  state_.SetBranch(true);
}

// ADDI -- Add immediate (with overflow) (addi $t, $s, imm)
// Adds a register and an SE immediate value and stores the result in a register
void Processor::AddImmediate(const Instruction instr)
{
  const int32_t value   = state_.ReadSigned(instr.RS());
  const int32_t imm     = instr.ImmediateSE();
  const uint32_t target = instr.RT();

  // Trigger an exception if the add overflows.
  if (Utility::AddWillOverflow(value, imm))
    return HandleException(ExceptionCode::ArithmeticOverflow);

  state_.Write(target, (value + imm));
}

// ADDIU -- Add immediate unsigned (no overflow) (addiu $t, $s, imm)
// Adds a register and a SE immediate value and stores the result in a register.
void Processor::AddImmediateUnsigned(const Instruction instr)
{
  const uint32_t value  = state_.Read(instr.RS());
  const uint32_t imm    = instr.ImmediateSU();
  const uint32_t target = instr.RT();

  const uint32_t result = value + imm;

  state_.Write(target, result);
}

// LUI -- Load upper immediate (lui $t, imm)
// The immediate value is shifted left 16 bits and stored in the register.
// The lower 16 bits are zeroes.
void Processor::LoadUpperImmediate(const Instruction instr)
{
  const uint32_t value  = instr.Immediate() << 16;
  const uint32_t target = instr.RT();

  state_.Write(target, value);
}

// SLTI -- Set on less than immediate (slti $t, $s, imm)
// If $s is less than immediate, $t is set to one. It gets zero otherwise.
void Processor::SetOnLessThanImmediate(const Instruction instr)
{
  const int32_t imm     = instr.ImmediateSE();
  const int32_t value   = state_.ReadSigned(instr.RS());
  const uint32_t result = static_cast<uint32_t>(value < imm);

  const uint32_t target = instr.RT();

  state_.Write(target, result);
}

// ANDI -- Bitwise and immediate (andi $t, $s, imm)
// Bitwise ands a register and an imm value and stores the result in a register
void Processor::BitwiseAndImmediate(const Instruction instr)
{
  const uint32_t value  = state_.Read(instr.RS());
  const uint32_t imm    = instr.Immediate();
  const uint32_t target = instr.RT();

  const uint32_t result = value & imm;

  state_.Write(target, result);
}

// ORI -- Bitwise or immediate (ori $t, $s, imm)
// Bitwise OR a register and immediate value and store the result in a register.
void Processor::BitwiseOrImmediate(const Instruction instr)
{
  const uint32_t value  = state_.Read(instr.RS());
  const uint32_t imm    = instr.Immediate();
  const uint32_t target = instr.RT();

  const uint32_t result = value | imm;

  state_.Write(target, result);
}

// XORI -- Bitwise xor immediate (xori $t, $s, imm)
// Bitwise XOR a register and immediate value and store the result in a register
void Processor::BitwiseXorImmediate(const Instruction instr)
{
  const uint32_t value  = state_.Read(instr.RS());
  const uint32_t imm    = instr.Immediate();
  const uint32_t target = instr.RT();

  const uint32_t result = value ^ imm;

  state_.Write(target, result);
}

// LB -- Load byte (lb $t, offset($s))
// A byte is loaded into a register from the specified address.
void Processor::LoadByte(const Instruction instr)
{
  const int32_t offset  = instr.ImmediateSU();
  const uint32_t base   = state_.Read(instr.RS());
  const uint32_t target = instr.RT();

  const uint32_t address = base + offset;
  const int8_t value     = memory_->Read<uint8_t>(address);
  const uint32_t final   = static_cast<uint32_t>(value);

  state_.SetLoad(target, final);
}

// LH -- Load Half Word (lh $t, offset($s))
// A half-word is loaded into a register from the specified address.
void Processor::LoadHalfWord(const Instruction instr)
{
  const int32_t offset   = instr.ImmediateSE();
  const uint32_t base    = state_.Read(instr.RS());
  const uint32_t target  = instr.RT();
  const uint32_t address = base + offset;

  if (address % 2 != 0)
    return HandleException(ExceptionCode::LoadAddressError);

  const int16_t value  = memory_->Read<uint16_t>(address);
  const uint32_t final = static_cast<uint32_t>(value);

  state_.SetLoad(target, final);
}

// LWL -- Load Word Left (lwl $t, offset($s))
// Load unaligned word
void Processor::LoadWordLeft(const Instruction instr)
{
  const int32_t offset  = instr.ImmediateSE();
  const uint32_t base   = state_.Read(instr.RS());
  const uint32_t target = instr.RT();

  const uint32_t address = base + offset;
  const uint32_t aligned = address & (~0x3);

  const uint32_t current = state_.GetLoad(target);
  const uint32_t value   = memory_->Read<uint32_t>(aligned);

  const uint32_t shift = (address % 4) * 8;
  const uint32_t mask  = 0x00FFFFFF >> shift;

  const uint32_t result = (current & mask) | (value << (24 - shift));

  state_.SetLoad(target, result);
}

// LW -- Load word (lw $t, offset($s))
// A word is loaded into a register from the specified address.
void Processor::LoadWord(const Instruction instr)
{
  const int32_t offset   = instr.ImmediateSE();
  const uint32_t base    = state_.Read(instr.RS());
  const uint32_t target  = instr.RT();
  const uint32_t address = base + offset;

  if (address % 4 != 0)
    return HandleException(ExceptionCode::LoadAddressError);

  const uint32_t value = memory_->Read<uint32_t>(address);

  state_.SetLoad(target, value);
}

// LBU -- Load Byte Unsigned (lbu $t, offset($s))
// An unsigned byte is loaded into a register from the specified address.
void Processor::LoadByteUnsigned(const Instruction instr)
{
  const int32_t offset  = instr.ImmediateSE();
  const uint32_t base   = state_.Read(instr.RS());
  const uint32_t target = instr.RT();

  const uint32_t address = base + offset;
  const uint8_t value    = memory_->Read<uint8_t>(address);
  const uint32_t final   = static_cast<uint32_t>(value);

  state_.SetLoad(target, final);
}

// LHU -- Load Half Word Unsigned (lhu $t, offset($s))
// An unsigned half-word is loaded into a register from the specified address.
void Processor::LoadHalfWordUnsigned(const Instruction instr)
{
  const int32_t offset   = instr.ImmediateSE();
  const uint32_t base    = state_.Read(instr.RS());
  const uint32_t target  = instr.RT();
  const uint32_t address = base + offset;

  if (address % 2 != 0)
    return HandleException(ExceptionCode::LoadAddressError);

  const uint16_t value = memory_->Read<uint16_t>(address);
  const uint32_t final = static_cast<uint32_t>(value);

  state_.SetLoad(target, final);
}

// LWR -- Load Word Right (lwr $t, offset($s))
// Load unaligned word
void Processor::LoadWordRight(const Instruction instr)
{
  const int32_t offset  = instr.ImmediateSE();
  const uint32_t base   = state_.Read(instr.RS());
  const uint32_t target = instr.RT();

  const uint32_t address = base + offset;
  const uint32_t aligned = address & (~0x3);

  const uint32_t current = state_.GetLoad(target);
  const uint32_t value   = memory_->Read<uint32_t>(aligned);

  const uint32_t shift = (address % 4) * 8;
  const uint32_t mask  = 0xFFFFFF00 << (24 - shift);

  const uint32_t result = (current & mask) | (value >> shift);

  state_.SetLoad(target, result);
}

// SB - Store Byte (sb $t, offset($s))
// The least significant byte of $t is stored at the specified address.
void Processor::StoreByte(const Instruction instr)
{
  // Ignore writes to memory if cache bit is enabled, for now.
  if ((cop0_.HandleRead(12) & 0x10000) != 0)
    return;

  const int32_t offset = instr.ImmediateSE();
  const uint32_t base  = state_.Read(instr.RS());
  const uint8_t value  = (state_.Read(instr.RT()) & 0xFF);

  const uint32_t address = base + offset;

  memory_->Write<uint8_t>(address, value);
}

// SH - Store Half Word (sh $t, offset($s)
// Lower 16 bits of RT is stored at the specified address. Address = RS + IMM
void Processor::StoreHalfWord(const Instruction instr)
{
  // Ignore writes to memory if cache bit is enabled, for now.
  if ((cop0_.HandleRead(12) & 0x10000) != 0)
    return;

  const int32_t offset = instr.ImmediateSE();
  const uint32_t base  = state_.Read(instr.RS());
  const uint16_t value = (state_.Read(instr.RT()) & 0xFFFF);

  const uint32_t address = base + offset;

  if (address % 2 != 0)
    return HandleException(ExceptionCode::StoreAddressError);

  memory_->Write<uint16_t>(address, value);
}

// SWL -- Store Word Left (swl $t, offset($s))
void Processor::StoreWordLeft(const Instruction instr)
{
  const int32_t offset = instr.ImmediateSE();
  const uint32_t base  = state_.Read(instr.RS());
  const uint32_t value = state_.Read(instr.RT());

  const uint32_t address = base + offset;
  const uint32_t aligned = address & (~0x3);

  const uint32_t current = memory_->Read<uint32_t>(aligned);

  const uint32_t shift = (address % 4) * 8;
  const uint32_t mask  = 0xFFFFFF00 << shift;

  const uint32_t result = (current & mask) | (value >> (24 - shift));

  memory_->Write<uint32_t>(address, result);
}

// SW -- Store word (sw $t, offset($s)
// The contents of RT is stored at the specified address. Address = RS + IMM
void Processor::StoreWord(const Instruction instr)
{
  // Ignore writes to memory if cache bit is enabled, for now.
  if ((cop0_.HandleRead(12) & 0x10000) != 0)
    return;

  const int32_t offset = instr.ImmediateSE();
  const uint32_t base  = state_.Read(instr.RS());
  const uint32_t value = state_.Read(instr.RT());

  const uint32_t address = base + offset;

  if (address % 4 != 0)
    return HandleException(ExceptionCode::StoreAddressError);

  memory_->Write<uint32_t>(address, value);
}

// SWR -- Store Word Right (swr $t, offset($s))
void Processor::StoreWordRight(const Instruction instr)
{
  const int32_t offset = instr.ImmediateSE();
  const uint32_t base  = state_.Read(instr.RS());
  const uint32_t value = state_.Read(instr.RT());

  const uint32_t address = base + offset;
  const uint32_t aligned = address & (~0x3);

  const uint32_t current = memory_->Read<uint32_t>(aligned);

  const uint32_t shift = (address % 4) * 8;
  const uint32_t mask  = 0x00FFFFFF >> (24 - shift);

  const uint32_t result = (current & mask) | (value << shift);

  memory_->Write<uint32_t>(address, result);
}

// LWC0 - Load Word Coprocessor 0
void Processor::LoadWordCoprocessor0()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

// LWC1 - Load Word Coprocessor 1
void Processor::LoadWordCoprocessor1()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

// LWC2 - Load Word Coprocessor 2
void Processor::LoadWordCoprocessor2()
{
  // TODO: Finish when GTE is implemented
  panic("LoadWordCoprocessor2 instruction not implemented");
}

// LWC3 - Load Word Coprocessor 3
void Processor::LoadWordCoprocessor3()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

// SWC0 - Store Word Coprocessor 0
void Processor::StoreWordCoprocessor0()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

// SWC1 - Store Word Coprocessor 1
void Processor::StoreWordCoprocessor1()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

// SWC2 - Store Word Coprocessor 2
void Processor::StoreWordCoprocessor2()
{
  // TODO: Finish when GTE is implemented
  panic("StoreWordCoprocessor2 instruction not implemented");
}

// SWC3 - Store Word Coprocessor 3
void Processor::StoreWordCoprocessor3()
{
  HandleException(ExceptionCode::CoprocessorUnusable);
}

// SLL -- Shift left logical (sll $d, $t, h)
// Shifts a register value left by the shift amount listed in the instruction.
// Result is written to $RD. Zeroes are shifted in.
void Processor::ShiftLeftLogical(const Instruction instr)
{
  const uint32_t shift  = instr.Shift();
  const uint32_t value  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  const uint32_t result = value << shift;

  state_.Write(target, result);
}

// SRL -- Shift right logical (srl $d, $t, h)
// Shifts a register value right by the shift amount listed in the instruction.
// Result is written to $RD. Zeroes are shifted in.
void Processor::ShiftRightLogical(const Instruction instr)
{
  const uint32_t shift  = instr.Shift();
  const uint32_t value  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  const uint32_t result = value >> shift;

  state_.Write(target, result);
}

// SRA -- Shift right arithmetic (sra $d, $t, h)
// Shifts a register value right by the shift amount listed in the instruction.
// Result is written to $RD. The sign bit it shifted in
void Processor::ShiftRightArithmetic(const Instruction instr)
{
  const uint32_t shift  = instr.Shift();
  const uint32_t value  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  const uint32_t result = (static_cast<int32_t>(value)) >> shift;

  state_.Write(target, result);
}

// SLLV -- Shift left logical variable (sllv $d, $t, $s)
// Shifts a register value left by the value in a second register and places
// the result in a third register. Zeroes are shifted in.
void Processor::ShiftLeftLogicalVar(const Instruction instr)
{
  const uint32_t shift  = state_.Read(instr.RS());
  const uint32_t value  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  const uint32_t result = (value << (shift & 0x1F));

  state_.Write(target, result);
}

// SRLV -- Shift right logical variable (srlv $d, $t, $s)
// Shifts a register value right by the value in a second register and places
// the result in a third register. Zeroes are shifted in.
void Processor::ShiftRightLogicalVar(const Instruction instr)
{
  const uint32_t shift  = state_.Read(instr.RS());
  const uint32_t value  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  const uint32_t result = (value >> (shift & 0x1F));

  state_.Write(target, result);
}

// SRAV -- Shift right arithmetic variable (srav $d, $t, $s)
// Shifts a register value right by the value in a second register and places
// the result in a third register. The sign bit is shifted in.
void Processor::ShiftRightArithmeticVar(const Instruction instr)
{
  const uint32_t shift  = state_.Read(instr.RS());
  const uint32_t value  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  const uint32_t result = (static_cast<int32_t>(value)) >> (shift & 0x1F);

  state_.Write(target, result);
}

// JR -- Jump register (jr $s)
// Jump to the address contained in register $s
void Processor::JumpRegister(const Instruction instr)
{
  const uint32_t address = state_.Read(instr.RS());

  state_.SetNextPC(address);
  state_.SetBranch(true);
}

// JALR -- Jump and link register (jalr $target, $link)
// Jumps to the address in $s and stores return address in $d
void Processor::JumpAndLinkRegister(const Instruction instr)
{
  const uint32_t target = state_.Read(instr.RS());
  const uint32_t linked = state_.PC();

  state_.Write(instr.RD(), linked + 4);
  state_.SetNextPC(target);
  state_.SetBranch(true);
}

// SYSCALL -- System call (syscall)
// Generates a software interrupt.
void Processor::SystemCall()
{
  HandleException(ExceptionCode::SysCall);
}

// BREAK
void Processor::Break()
{
  HandleException(ExceptionCode::Breakpoint);
}

// MFHI -- Move from HI (mfhi $d)
// The contents of register HI are moved to the specified register
void Processor::MoveFromHI(const Instruction instr)
{
  const uint32_t value  = state_.HiRegister();
  const uint32_t target = instr.RD();

  state_.Write(target, value);
}

// MFHI -- Move to HI (mthi $s)
// The contents of register RS are moved to register HI
void Processor::MoveToHI(const Instruction instr)
{
  const uint32_t value = state_.Read(instr.RS());

  state_.SetHiRegister(value);
}
// MFLO -- Move from LO (mflo $d)
// The contents of register LO are moved to the specified register
void Processor::MoveFromLO(const Instruction instr)
{
  const uint32_t value  = state_.LoRegister();
  const uint32_t target = instr.RD();

  state_.Write(target, value);
}

// MTLO -- Move to LO (mtlo $s)
// The contents of register RS are moved to register LO
void Processor::MoveToLO(const Instruction instr)
{
  const uint32_t value = state_.Read(instr.RS());

  state_.SetLoRegister(value);
}

// MULT -- Multiply (mult $s, $t)
// Multiplies $s by $t and stores the result in $LO.
void Processor::Multiply(const Instruction instr)
{
  const int64_t valueS = state_.Read(instr.RS());
  const int64_t valueT = state_.Read(instr.RT());

  const int64_t result = valueS * valueT;

  state_.SetHiRegister(static_cast<uint32_t>(result >> 32));
  state_.SetLoRegister(static_cast<uint32_t>(result));
}

// MULTU -- Multiply Unsigned (multu $s, $t)
// Multiplies $s by $t and stores the result in $LO.
void Processor::MultiplyUnsigned(const Instruction instr)
{
  const uint64_t valueS = state_.Read(instr.RS());
  const uint64_t valueT = state_.Read(instr.RT());

  const uint64_t result = valueS * valueT;

  state_.SetHiRegister(static_cast<uint32_t>(result >> 32));
  state_.SetLoRegister(static_cast<uint32_t>(result));
}

// DIV -- Divide (div $s, $t)
// Divides $s by $t and stores the quotient in $LO and the remainder in $HI
void Processor::Divide(const Instruction instr)
{
  const int32_t valueS = state_.Read(instr.RS());
  const int32_t valueT = state_.Read(instr.RT());

  uint32_t high = 0;
  uint32_t low  = 0;

  if (valueT == 0)
  {
    high = valueS;
    low  = (valueS >= 0 ? -1 : 1);
  }
  else if ((valueT == -1) && (valueS == static_cast<int32_t>(0x80000000)))
  {
    high = 0;
    low  = 0x80000000;
  }
  else
  {
    high = valueS % valueT;
    low  = valueS / valueT;
  }

  state_.SetHiRegister(high);
  state_.SetLoRegister(low);
}


// DIVU -- Divide Unsigned (divu $s, $t)
// Divides $s by $t and stores the quotient in $LO and the remainder in $HI
void Processor::DivideUnsigned(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  uint32_t high = valueS;
  uint32_t low  = 0xFFFFFFFF;

  if (valueT != 0)
  {
    high = valueS % valueT;
    low  = valueS / valueT;
  }

  state_.SetHiRegister(high);
  state_.SetLoRegister(low);
}

// ADD – Add (with overflow)(add $d, $s, $t)
// Adds two registers and stores the result in a register
void Processor::AddRegisters(const Instruction instr)
{
  const int32_t valueA  = state_.Read(instr.RS());
  const int32_t valueB  = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  // Trigger an exception if the add overflows.
  if (Utility::AddWillOverflow(valueA, valueB))
    return HandleException(ExceptionCode::ArithmeticOverflow);

  state_.Write(target, (valueA + valueB));
}

// ADDU -- Add unsigned (no overflow) (addu $d, $s, $t)
// Adds two registers and stores the result in a register
void Processor::AddUnsigned(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = valueS + valueT;

  state_.Write(target, result);
}

// SUB -- Subtract (with overflow)(sub $d, $s, $t)
// Subtract two registers and stores the result in a register
void Processor::Subtract(const Instruction instr)
{
  const uint32_t valueA = state_.Read(instr.RS());
  const uint32_t valueB = state_.Read(instr.RT());
  const uint32_t target = instr.RD();

  // Trigger an exception if the subtract underflows.
  if (Utility::SubWillUnderflow(valueA, valueB))
    return HandleException(ExceptionCode::ArithmeticOverflow);

  state_.Write(target, (valueA - valueB));
}

// SUBU -- Subtract unsigned (no overflow) (subu $d, $s, $t)
// Subtract two registers and stores the result in a register
void Processor::SubtractUnsigned(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = valueS - valueT;

  state_.Write(target, result);
}

// AND -- Bitwise and (and $d, $s, $t)
// Bitwise ands two registers and stores the result in a register.
void Processor::BitwiseAnd(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = valueS & valueT;

  state_.Write(target, result);
}

// OR -- Bitwise or (or $d, $s, $t)
// Bitwise logical ors two registers and stores the result in a register.
void Processor::BitwiseOr(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = valueS | valueT;

  state_.Write(target, result);
}

// XOR -- Bitwise xor (xor $d, $s, $t)
// Bitwise logical xors two registers and stores the result in a register.
void Processor::BitwiseXor(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = valueS ^ valueT;

  state_.Write(target, result);
}

// NOR -- Bitwise nor (nor $d, $s, $t)
// Bitwise logical nors two registers and stores the result in a register.
void Processor::BitwiseNor(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = ~(valueS | valueT);

  state_.Write(target, result);
}

// SLTIU -- Set on less than immediate unsigned (sltiu $t, $s, imm)
// If $s is less than unsigned imm, $t is set to one. It gets zero otherwise.
void Processor::SetOnLessThanImmediateUnsigned(const Instruction instr)
{
  const uint32_t imm    = instr.ImmediateSU();
  const uint32_t value  = state_.Read(instr.RS());
  const uint32_t target = instr.RT();

  const uint32_t result = static_cast<uint32_t>(value < imm);

  state_.Write(target, result);
}

// SLT -- Set on less than (signed) (slt $d, $s, $t)
// If $s is less than $t, $d is set to one. It gets zero otherwise.
void Processor::SetOnLessThan(const Instruction instr)
{
  const int32_t valueS = state_.Read(instr.RS());
  const int32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = static_cast<uint32_t>(valueS < valueT);

  state_.Write(target, result);
}

// SLTU -- Set on less than unsigned (sltu $d, $s, $t)
// If $s is less than $t, $d is set to one. It gets zero otherwise.
void Processor::SetOnLessThanUnsigned(const Instruction instr)
{
  const uint32_t valueS = state_.Read(instr.RS());
  const uint32_t valueT = state_.Read(instr.RT());

  const uint32_t target = instr.RD();
  const uint32_t result = static_cast<uint32_t>(valueS < valueT);

  state_.Write(target, result);
}

void Processor::MoveFromCoprocessor0(const Instruction instr)
{
  const uint32_t index  = instr.RD();
  const uint32_t target = instr.RT();
  const uint32_t value  = cop0_.HandleRead(index);

  state_.SetLoad(target, value);
}

void Processor::MoveToCoprocessor0(const Instruction instr)
{
  const uint32_t index = instr.RD();
  const uint32_t value = state_.Read(instr.RT());

  cop0_.HandleWrite(index, value);
}

void Processor::ReturnFromException()
{
  cop0_.ReturnFromException();
}

} // namespace Cpu
