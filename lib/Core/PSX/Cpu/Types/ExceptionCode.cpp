#include <PSX/Cpu/Types/ExceptionCode.h>

#include <ostream>

namespace Cpu
{

std::ostream& operator<<(std::ostream& os, const ExceptionCode code)
{
  switch (code)
  {
    case ExternalInterrupt:   { return os << "External Interrupt";   }
    case ModifyCache:         { return os << "Modify Cache";         }
    case LoadCacheError:      { return os << "Load Cache Error";     }
    case StoreCacheError:     { return os << "Store Cache Error";    }
    case LoadAddressError:    { return os << "Load Address Error";   }
    case StoreAddressError:   { return os << "Store Address Error";  }
    case FetchBusError:       { return os << "Fetchbus Error";       }
    case DataBusError:        { return os << "Databus Error";        }
    case SysCall:             { return os << "Syscall";              }
    case Breakpoint:          { return os << "Breakpoint";           }
    case ReservedInstruction: { return os << "Reserved Instruction"; }
    case CoprocessorUnusable: { return os << "Coprocessor Unusable"; }
    case ArithmeticOverflow:  { return os << "Arithmetic Overflow";  }
  }

  return os;
}

} // namespace Cpu
