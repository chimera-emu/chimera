#include "PSX/Emulator.h"
#include "PSX/Machine.h"

#include <LibGraphics/Video/VideoBackend.h>

#include <memory>
#include <thread>

Emulator::Emulator(const std::string& bios, const WindowHandle handle)
  : bios_{bios}, handle_{handle}, machine_{nullptr}, main_thread_{}
{
  Initialise();
}

void Emulator::Initialise()
{
  main_thread_ = std::thread(&Emulator::Run, this);
}

void Emulator::Run()
{
  // TODO: This is a turd
  VideoBackend::Initialise(handle_);

  machine_ = std::make_unique<Machine>(bios_);
  machine_->Run();
}
