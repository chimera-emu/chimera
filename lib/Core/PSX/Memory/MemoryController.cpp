#include "PSX/Memory/MemoryController.h"

#include <Core/PSX/Memory/MemoryRegion.h>
#include <Core/PSX/Panic.h>
#include <Core/PSX/Logger.h>

#include <array>
#include <cstdint>
#include <ostream>
#include <string>

namespace
{
uint32_t MaskAddress(const uint32_t addr)
{
  static const std::array<uint32_t, 8> mask = {
      0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
      0x7FFFFFFF, 0x1FFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF };

  return (addr & mask[addr >> 29]);
}
} // anonymous namespace

namespace Memory
{

MemoryRegion& MemoryController::GetAddressRegion(const uint32_t addr)
{
  // TODO: Find faster method of looking up correct region
  for (auto& region : regions_)
  {
    if (region.Contains(addr))
      return region;
  }

  panic_unreachable("No memory region for address: [{:#x}]", addr);
}

template <class Type>
Type MemoryController::Read(const uint32_t addr)
{
  const uint32_t absolute = MaskAddress(addr);

  auto& region = GetAddressRegion(absolute);

  return region.Read<Type>(absolute);
}

template <class Type>
void MemoryController::Write(const uint32_t addr, const Type value)
{
  const uint32_t absolute = MaskAddress(addr);

  auto& region = GetAddressRegion(absolute);

  region.Write<Type>(absolute, value);
}

void MemoryController::AddMemoryRegion(const std::string& name,
                                const uint32_t addr, const uint32_t size)
{
  regions_.push_back(MemoryRegion(name, addr, size));
}

void MemoryController::AddMemoryRegion(MemoryRegion&& region)
{
  regions_.push_back(region);
}

void MemoryController::LoadMemoryRegion(const uint32_t addr,
                                 const std::vector<uint8_t>& data)
{
  auto& region = GetAddressRegion(addr);

  Logger()->info("Loading data into MemoryRegion: {}", region.Name());
  region.Load(data);
}

std::ostream& operator<<(std::ostream& os, const MemoryController& controller)
{
  for (const auto& region : controller.Regions())
    os << region << "\n";

  return os;
}

template uint8_t  MemoryController::Read<uint8_t>(const uint32_t);
template uint16_t MemoryController::Read<uint16_t>(const uint32_t);
template uint32_t MemoryController::Read<uint32_t>(const uint32_t);

template void MemoryController::Write<uint8_t>(const uint32_t, const uint8_t);
template void MemoryController::Write<uint16_t>(const uint32_t, const uint16_t);
template void MemoryController::Write<uint32_t>(const uint32_t, const uint32_t);

} // namespace Memory
