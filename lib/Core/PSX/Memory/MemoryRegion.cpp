#include "PSX/Memory/MemoryRegion.h"

#include <Core/PSX/Logger.h>

#include <ostream>

using namespace std::placeholders;

namespace Memory
{

MemoryRegion::MemoryRegion(std::string  name, const uint32_t address,
                           const uint32_t size, MMIOInterface& mmioInterface)
    : name_(std::move(name)), address_(address), size_(size),
      writeCallback_(std::bind(&MMIOInterface::HandleWrite, &mmioInterface, _1, _2)),
      readCallback_(std::bind(&MMIOInterface::HandleRead, &mmioInterface, _1)) {}


bool MemoryRegion::Contains(const uint32_t address) const
{
  if (address < address_)
    return false;

  if (address > address_ + size_)
    return false;

  return true;
}

void MemoryRegion::Load(std::vector<uint8_t> input)
{
  if (input.size() > size_)
    Logger()->error("Input data larger than memory region. Resizing input.");

  data_ = std::move(input);
  data_.resize(size_, 0x00);
}

template <class Type>
Type MemoryRegion::Read(const uint32_t address)
{
  if (readCallback_ != nullptr)
    return static_cast<Type>(readCallback_(address));

  const uint32_t offset = address - address_;

  Type value = Type(0);

  for (size_t i = 0; i < sizeof(value); ++i)
    value |= data_[offset + i] << (i * 8);

  return value;
}

template <class Type>
void MemoryRegion::Write(const uint32_t address, const Type value)
{
  if (writeCallback_ != nullptr)
    return writeCallback_(address, value);

  const uint32_t offset = address - address_;

  for (size_t i = 0; i < sizeof(Type); ++i)
    data_[offset + i] = (value >> (i * 8)) & 0xFF;
}

std::ostream& operator<<(std::ostream& os, const MemoryRegion& region)
{
  return os << fmt::format("[{:11}: {:0=#10x} - {:0=#8x}]",
                           region.Name(), region.Address(), region.Size());
}

template uint8_t  MemoryRegion::Read<uint8_t>(const uint32_t);
template uint16_t MemoryRegion::Read<uint16_t>(const uint32_t);
template uint32_t MemoryRegion::Read<uint32_t>(const uint32_t);

template void MemoryRegion::Write<uint8_t>(const uint32_t, const uint8_t);
template void MemoryRegion::Write<uint16_t>(const uint32_t, const uint16_t);
template void MemoryRegion::Write<uint32_t>(const uint32_t, const uint32_t);

} // namespace Memory
