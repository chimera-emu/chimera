#include <Core/PSX/Cdrom/Types/Fifo.h>

namespace Cdrom
{

bool Fifo::empty() const
{
  return ((writeIndex_ ^ readIndex_) & 0x1F) == 0;
}

bool Fifo::full() const
{
  return ((writeIndex_ ^ readIndex_ ^ 0x10) & 0x1F) == 0;
}

void Fifo::clear()
{
  readIndex_ = writeIndex_;

  buffer_.fill(0);
}

void Fifo::push_back(const uint8_t value)
{
  buffer_[writeIndex_ & 0x0F] = value;

  ++writeIndex_;
}

uint8_t Fifo::pop_back()
{
  const uint8_t index = readIndex_ & 0x0F;

  ++readIndex_;

  return buffer_[index];
}

uint8_t Fifo::length() const
{
  // TODO: Check for underflow?
  return (writeIndex_ - readIndex_) & 0x1F;
}

}  // namespace Cdrom
