#include "PSX/Cdrom/CdromController.h"

#include <Core/PSX/Interrupts/InterruptController.h>

#include <Core/PSX/Panic.h>
#include <Core/PSX/Logger.h>

#include <gsl/gsl_assert>

namespace Cdrom
{

CdromController::CdromController(Interrupts::InterruptController* interrupts)
  : index_(0), interruptFlags_(0), interruptMask_(0),
    parameters_(), response_(), interrupts_(interrupts)
{
  Expects(interrupts != nullptr);
}


void CdromController::HandleWrite(const uint32_t address, const uint32_t value)
{
  Logger()->info("CDROM Write: [{:#x}] <- {:#x}", address, value);

  // Interrupt state before dispatching the write
  const auto previousState = IsInterruptPending();

  const auto data = static_cast<uint8_t>(value);

  switch (address)
  {
    case 0x1F801800: { HandleGP0Write(data); break; }
    case 0x1F801801: { HandleGP1Write(data); break; }
    case 0x1F801802: { HandleGP2Write(data); break; }
    case 0x1F801803: { HandleGP3Write(data); break; }
    default:
      panic_unreachable("Invalid address: [{:#x}]", address);
  }

  // Interrupt state after dispatching the write
  const auto currentState = IsInterruptPending();

  // If the interrupt state transitioned, trigger an interrupt
  if (!previousState && currentState)
    interrupts_->Trigger(Interrupts::InterruptChannel::CDROM);
}


uint32_t CdromController::HandleRead(const uint32_t address) const
{
  return static_cast<uint32_t>(
    const_cast<CdromController*>(this)->HandleRead(address));
}


uint8_t CdromController::HandleRead(const uint32_t address)
{
  Logger()->info("CDROM Read: [{:#x}]", address);

  switch (address)
  {
    case 0x1F801800: { return HandleGP0Read(); }
    case 0x1F801801: { return HandleGP1Read(); }
    case 0x1F801802: { return HandleGP2Read(); }
    case 0x1F801803: { return HandleGP3Read(); }
    default:
      panic_unreachable("Invalid address: [{:#x}]", address);
  }
}


void CdromController::HandleGP0Write(const uint8_t value)
{
  index_.Set(value);
}


void CdromController::HandleGP1Write(const uint8_t value)
{
  const auto channel = index_.GetCdromIndex();

  switch (channel)
  {
    case 0x00: { HandleGP1WriteIndex0(value); break; }
    default:
      panic("Unhandled GP1Write index: {:#x}", channel);
  }
}


void CdromController::HandleGP2Write(const uint8_t value)
{
  const auto channel = index_.GetCdromIndex();

  switch (channel)
  {
    case 0x00: { HandleGP2WriteIndex0(value); break; }
    case 0x01: { HandleGP2WriteIndex1(value); break; }
    default:
      panic("Unhandled GP2Write index: {:#x}", channel);
  }
}


void CdromController::HandleGP3Write(const uint8_t value)
{
  const auto channel = index_.GetCdromIndex();

  switch (channel)
  {
    case 0x01: { HandleGP3WriteIndex1(value); break; }
    default:
      panic("Unhandled index: {:#x}", channel);
  }
}


void CdromController::HandleGP1WriteIndex0(const uint8_t value)
{
  response_.clear();

  switch (value)
  {
    case 0x01: { CommandGetStat(); break; }
    case 0x19: { CommandTest();    break; }
    default:
      panic("Unhandled command: {:#x}", value);
  }

  parameters_.clear();
}


void CdromController::HandleGP2WriteIndex0(const uint8_t value)
{
  Expects(!parameters_.full());

  parameters_.push_back(value);
}


void CdromController::HandleGP2WriteIndex1(const uint8_t value)
{
  interruptMask_ = (value & 0x1F);
}


void CdromController::HandleGP3WriteIndex1(const uint8_t value)
{
  interruptFlags_ &= ~(value & 0x1F);

  if ((value & (1U << 6)) != 0u)
    parameters_.clear();
}


void CdromController::CommandGetStat()
{
  Expects(parameters_.empty());

  const uint8_t status = (1U << 4);

  response_.push_back(status);

  ConfigureInterrupt(0x03);
}


void CdromController::CommandTest()
{
  Expects(parameters_.length() == 1);

  const auto param = parameters_.pop_back();

  switch (param)
  {
    case 0x20: { return TestVersion(); }
    default:
      panic("Unhandled parameter: {:#x}", param);
  }
}


void CdromController::TestVersion()
{
  // 19 Sep 1994, version vC0 (a)
  response_.push_back(0x94);
  response_.push_back(0x09);
  response_.push_back(0x19);
  response_.push_back(0xC0);

  ConfigureInterrupt(0x03);
}


void CdromController::ConfigureInterrupt(const uint8_t code)
{
  interruptFlags_ = code;
}


uint8_t CdromController::HandleGP0Read()
{
  index_.SetAdpcmFifoEmpty(true);
  index_.SetParameterFifoEmpty(parameters_.empty());
  index_.SetParameterFifoFull(parameters_.full());
  index_.SetResponseFifoEmpty(response_.empty());
  index_.SetDataFifoEmpty(true);
  index_.SetTransmissionBusy(false);

  return static_cast<uint8_t>(index_.Get());
}


uint8_t CdromController::HandleGP1Read()
{
  const auto channel = index_.GetCdromIndex();

  switch (channel)
  {
    case 0x01: { return HandleGP1ReadIndex1(); }
    default:
      panic("Unhandled GP1Read index: {:#x}", channel);
  }
}


uint8_t CdromController::HandleGP2Read()
{
  const auto channel = index_.GetCdromIndex();

  panic("Unhandled GP2Read index: {:#x}", channel);
}


uint8_t CdromController::HandleGP3Read()
{
  const auto channel = index_.GetCdromIndex();

  switch (channel)
  {
    case 0x01: { return interruptFlags_; }
    default:
      panic("Unhandled GP3Read index: {:#x}", channel);
  }
}


uint8_t CdromController::HandleGP1ReadIndex1()
{
  Expects(parameters_.empty());

  return response_.pop_back();
}


bool CdromController::IsInterruptPending() const
{
  return (interruptFlags_ & interruptMask_) != 0;
}

} // namespace Cdrom
