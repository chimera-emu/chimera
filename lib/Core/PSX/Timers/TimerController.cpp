#include "PSX/Timers/TimerController.h"

#include <Core/PSX/Timers/TimerIndex.h>

#include <Core/PSX/Panic.h>
#include <Core/PSX/Logger.h>

#include <fmt/ostream.h>

#include <iostream>

namespace Timers
{

TimerController::TimerController()
    : counters_{RootCounter{TimerIndex::Timer0},
                RootCounter{TimerIndex::Timer1},
                RootCounter{TimerIndex::Timer2}} {}

void TimerController::HandleWrite(const uint32_t address, const uint32_t value)
{
  Logger()->info("TIMER Write: [{:#x}] <- {:#x}", address, value);

  const auto channel = (address >> 4) & 0x03;
  const auto index   = (address & 0x0F);

  RootCounter& counter = counters_[channel];

  switch (index)
  {
    case 0x00: { counter.SetCurrent(value); break; }
    case 0x04: { counter.SetMode(value);    break; }
    case 0x08: { counter.SetTarget(value);  break; }
    default:
      panic_unreachable("Invalid address [{:#x}]", address);
  }

  // Logger()->info("Counter [{}] state updated\n", channel) << counter;
}

uint32_t TimerController::HandleRead(const uint32_t address) const
{
  Logger()->info("TIMER Read: [{:#x}]", address);

  const auto channel = (address >> 4) & 0x03;
  const auto index   = (address & 0x0F);

  const RootCounter& counter = counters_[channel];

  switch (index)
  {
    case 0x00: { return counter.Current();    }
    case 0x04: { return counter.Mode().Get(); }
    case 0x08: { return counter.Target();     }
    default:
      panic_unreachable("Invalid address [{:#x}]", address);
  }
}

} // namespace Timers
