#include <LibGraphics/Window/Window.h>
#include <iostream>

namespace
{
  // TODO: Source this string from somewhere sensible
  const wchar_t* className = L"Chimera_Window";
}

Window::Window(const uint32_t width, const uint32_t height,
               const std::string& title)
    : handle(nullptr)
{
  RegisterWindowClass();

  std::wstring temp = std::wstring(title.begin(), title.end());

  handle = CreateWindowW(
    className,                     // Registered Class Name
    temp.c_str(),                  // Window Title
    WS_OVERLAPPEDWINDOW,           // Window Style
    CW_USEDEFAULT,                 // Window X
    CW_USEDEFAULT,                 // Window Y
    static_cast<int32_t>(width),   // Window Width
    static_cast<int32_t>(height),  // Window Height
    nullptr,                       // Handle - Parent Window
    nullptr,                       // Handle - Menu
    GetModuleHandleW(nullptr),     // Handle - Instance
    this);                         // lpParam?

  if (handle == nullptr)
    throw std::runtime_error("Could not create Win32 Window");

  ShowWindow(handle, SW_SHOW);
  UpdateWindow(handle);
}

LRESULT CALLBACK Window::StaticWndProc(HWND hwnd, UINT message, WPARAM wParam,
                                       LPARAM lParam)
{
  if (message == WM_CREATE)
  {
    LPCREATESTRUCT create = reinterpret_cast<LPCREATESTRUCT>(lParam);

    LONG_PTR window = reinterpret_cast<LONG_PTR>(create->lpCreateParams);

    SetWindowLongPtr(hwnd, GWLP_USERDATA, window);
  }

  Window* window = nullptr;

  if (hwnd)
    window = reinterpret_cast<Window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

  if (window)
    window->WndProc(message, wParam, lParam);

  return DefWindowProcW(hwnd, message, wParam, lParam);
}

void Window::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
  switch (message)
  {
    case WM_KEYDOWN:
    {
      Event event = Event(Keyboard::KeyCode(static_cast<uint32_t>(wParam)));

      PushEvent(event);
    }
  }
}

void Window::RegisterWindowClass()
{
  WNDCLASSW wndClass = {};
  // TODO: Should the window be updated on resize? Probably.
  wndClass.style         = 0;
  wndClass.lpfnWndProc   = &Window::StaticWndProc;
  wndClass.cbClsExtra    = 0;
  wndClass.cbWndExtra    = 0;
  wndClass.hInstance     = GetModuleHandleW(nullptr);
  wndClass.hIcon         = nullptr;
  wndClass.hCursor       = nullptr;
  wndClass.hbrBackground = nullptr;
  wndClass.lpszMenuName  = nullptr;
  wndClass.lpszClassName = className;
  RegisterClassW(&wndClass);
}


void Window::SetTitle(const std::string& title)
{
  // Convert the title to a wchar_t*
  std::wstring temp = std::wstring(title.begin(), title.end());

  SetWindowTextW(handle, temp.c_str());
}
