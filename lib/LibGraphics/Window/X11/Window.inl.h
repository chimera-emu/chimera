#include <LibGraphics/Window/Keyboard.h>
#include <LibGraphics/Window/Window.h>

#include <X11/keysym.h>
#include <X11/keysymdef.h>

Window::Window(const uint32_t width, const uint32_t height,
               const std::string& title)
{
  // Open connection to the X Server
  connection = xcb_connect(nullptr, nullptr);

  // Get the screen
  const XCBSetup* setup = xcb_get_setup(connection);
  screen                = xcb_setup_roots_iterator(setup).data;

  // Create the window
  window = xcb_generate_id(connection);

  // Create the event mask
  uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
  uint32_t list[2];

  list[0] = screen->white_pixel;
  list[1] = XCB_EVENT_MASK_EXPOSURE       | XCB_EVENT_MASK_BUTTON_PRESS   |
            XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION |
            XCB_EVENT_MASK_ENTER_WINDOW   | XCB_EVENT_MASK_LEAVE_WINDOW   |
            XCB_EVENT_MASK_KEY_PRESS      | XCB_EVENT_MASK_KEY_RELEASE;

  xcb_create_window(connection,                    // Connection
                    XCB_COPY_FROM_PARENT,          // Depth
                    window,                        // Window ID
                    screen->root,                  // Parent Window
                    0, 0,                          // Position(x, y)
                    static_cast<uint16_t>(width),  // Size
                    static_cast<uint16_t>(height), // Size
                    10,                            // Border Width
                    XCB_WINDOW_CLASS_INPUT_OUTPUT, // Class
                    screen->root_visual,           // Visual?
                    mask,                          // Event Mask
                    list);                         // Value List

  // Map the window to the screen
  xcb_map_window(connection, window);

  // Set the Window Title
  SetTitle(title);

  // Flush commands
  xcb_flush(connection);
}


void Window::SetTitle(const std::string& title)
{
  xcb_change_property(connection, XCB_PROP_MODE_REPLACE, window,
                      XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
                      static_cast<uint32_t>(title.length()), title.data());

  xcb_flush(connection);
}

bool Window::FetchEvent(Event& event)
{
  xcb_generic_event_t* message;

  while ((message = xcb_poll_for_event(connection)) != nullptr)
  {
    HandleEvent(message);
  }

  if (events.empty())
    return false;

  event = events.front();
  events.pop_front();

  return true;
}

namespace
{
  Keyboard::KeyCode XCBKeyToChimeraKeyCode(const xcb_keycode_t code)
  {
    switch (code)
    {
      case XK_a: { return Keyboard::KeyCode::A; }
      case XK_b: { return Keyboard::KeyCode::B; }
      case XK_c: { return Keyboard::KeyCode::C; }
      case XK_d: { return Keyboard::KeyCode::D; }
      case XK_e: { return Keyboard::KeyCode::E; }
      case XK_f: { return Keyboard::KeyCode::F; }
      case XK_g: { return Keyboard::KeyCode::G; }
      case XK_h: { return Keyboard::KeyCode::H; }
      case XK_i: { return Keyboard::KeyCode::I; }
      case XK_j: { return Keyboard::KeyCode::J; }
      case XK_k: { return Keyboard::KeyCode::K; }
      case XK_l: { return Keyboard::KeyCode::L; }
      case XK_m: { return Keyboard::KeyCode::M; }
      case XK_n: { return Keyboard::KeyCode::N; }
      case XK_o: { return Keyboard::KeyCode::O; }
      case XK_p: { return Keyboard::KeyCode::P; }
      case XK_q: { return Keyboard::KeyCode::Q; }
      case XK_r: { return Keyboard::KeyCode::R; }
      case XK_s: { return Keyboard::KeyCode::S; }
      case XK_t: { return Keyboard::KeyCode::T; }
      case XK_u: { return Keyboard::KeyCode::U; }
      case XK_v: { return Keyboard::KeyCode::V; }
      case XK_w: { return Keyboard::KeyCode::W; }
      case XK_x: { return Keyboard::KeyCode::X; }
      case XK_y: { return Keyboard::KeyCode::Y; }
      case XK_z: { return Keyboard::KeyCode::Z; }
      default:
      { return Keyboard::KeyCode::Unknown; }
    }
  }
}  // namespace


void Window::HandleEvent(xcb_generic_event_t* event)
{
  switch (event->response_type & ~0x80)
  {
    case XCB_KEY_PRESS:
    {
      xcb_key_press_event_t* ev =
        reinterpret_cast<xcb_key_press_event_t*>(event);

      // TODO: There's probably a much safer way of doing this.
      static xcb_key_symbols_t* symbols = xcb_key_symbols_alloc(connection);

      xcb_keysym_t ksym = xcb_key_symbols_get_keysym(symbols, ev->detail, 0);

      PushEvent(Event(XCBKeyToChimeraKeyCode(static_cast<uint8_t>(ksym))));
    }
  }
}
