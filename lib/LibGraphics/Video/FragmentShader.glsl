#version 330

in vec3 vertex_out_colour;
out vec4 fragment_out_colour;

void main()
{
  fragment_out_colour = vec4(vertex_out_colour, 1.0);
};
