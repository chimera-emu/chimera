#include "LibGraphics/Video/Context.h"

#include <stdexcept>

#pragma comment(lib, "opengl32.lib")

namespace
{
  PIXELFORMATDESCRIPTOR GeneratePixelFormatDescriptor()
  {
    PIXELFORMATDESCRIPTOR pfd =
    {
      sizeof(PIXELFORMATDESCRIPTOR),
      1,
      PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
      PFD_TYPE_RGBA,
      32,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      24,
      8,
      0,
      PFD_MAIN_PLANE,
      0, 0, 0, 0
    };

    return pfd;
  }
}

WGLContext::WGLContext(const WindowHandle handle)
  : windowHandle(handle), deviceContext(nullptr), renderContext(nullptr)
{
  CreateSurface();

  CreateContext();

  MakeCurrent();

  LoadExtensions();
}

void WGLContext::CreateSurface()
{
  PIXELFORMATDESCRIPTOR pfd = GeneratePixelFormatDescriptor();

  deviceContext = GetDC(windowHandle);

  if (deviceContext == nullptr)
    throw std::runtime_error("Could not create OpenGL device context");

  int pixelFormat = ChoosePixelFormat(deviceContext, &pfd);

  if (pixelFormat == 0)
    throw std::runtime_error("Could not choose a suitable pixel format");

  if (!SetPixelFormat(deviceContext, pixelFormat, &pfd))
    throw std::runtime_error("Could not set pixel format");
}

void WGLContext::CreateContext()
{
  renderContext = wglCreateContext(deviceContext);

  if (renderContext == nullptr)
    throw std::runtime_error("Could not create OpenGL rendering context");
}

void WGLContext::MakeCurrent()
{
  if (!wglMakeCurrent(deviceContext, renderContext))
    throw std::runtime_error("Could not make context current");
}

void WGLContext::LoadExtensions()
{
  if (!gladLoadGL())
    throw std::runtime_error("Could not load OpenGL extensions");
}

void WGLContext::Render(const Shader& shader)
{
  MakeCurrent();

  SwapBuffers(deviceContext);
}
