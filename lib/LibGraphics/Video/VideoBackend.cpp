#include "LibGraphics/Video/VideoBackend.h"

#include <gsl/gsl_assert>

#include <memory>

namespace
{
  static std::unique_ptr<VideoBackend> video_backend;
} // anonymous namespace

VideoBackend* VideoBackend::GetBackend()
{
  return video_backend.get();
}

void VideoBackend::Initialise(const WindowHandle handle)
{
  video_backend = std::make_unique<VideoBackend>(handle);
}

VideoBackend::VideoBackend(const WindowHandle handle)
  : context(handle), shader()
{
  shader.LoadShaders("Shaders\\VertexShader.glsl", "Shaders\\FragmentShader.glsl");

  vbuffer = Buffer(GenerateVertexBuffer(), 2);
  cbuffer = Buffer(GenerateVertexBuffer(), 3);

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbuffer.GetIndex());
  glVertexAttribPointer(0, vbuffer.GetCount(), GL_FLOAT, GL_FALSE, 0, nullptr);

  glBindBuffer(GL_ARRAY_BUFFER, cbuffer.GetIndex());
  glVertexAttribPointer(1, cbuffer.GetCount(), GL_FLOAT, GL_FALSE, 0, nullptr);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  bufferSize = 0;
}

GLuint VideoBackend::GenerateVertexBuffer()
{
  GLuint bufferObject = 0;
  glGenBuffers(1, &bufferObject);
  glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

  return bufferObject;
}

void VideoBackend::PushTriangle(const std::vector<Position>& positions,
                                const std::vector<Colour>& colours)
{
  Expects(positions.size() == 3);
  Expects(colours.size() == 3);

  for (size_t i = 0; i < 3; ++i)
  {
    vbuffer.push_back(positions[i].NormaliseX());
    vbuffer.push_back(positions[i].NormaliseY());

    cbuffer.push_back(colours[i].NormaliseRed());
    cbuffer.push_back(colours[i].NormaliseGreen());
    cbuffer.push_back(colours[i].NormaliseBlue());
  }

  bufferSize += 3;
}

void VideoBackend::PushQuad(const std::vector<Position>& positions,
                            const std::vector<Colour>& colours)
{
  Expects(positions.size() == 4);
  Expects(colours.size() == 4);

  // OPTIMIZE: Determine if the performance can be improved here.
  // First triangle in the quadrilateral
  auto first_positions = std::vector<Position>(positions.begin(), positions.end() - 1);
  auto first_colours   = std::vector<Colour>(colours.begin(), colours.end() - 1);
  PushTriangle(first_positions, first_colours);

  // Second triangle in the quadrilateral
  auto second_positions = std::vector<Position>(positions.begin() + 1, positions.end());
  auto second_colours   = std::vector<Colour>(colours.begin() + 1, colours.end());
  PushTriangle(second_positions, second_colours);
}

void VideoBackend::Render()
{
  if (vbuffer.empty())
    return;

  glBindBuffer(GL_ARRAY_BUFFER, vbuffer.GetIndex());
  glBufferData(GL_ARRAY_BUFFER, vbuffer.ByteSize(), vbuffer.DataPtr(),
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, cbuffer.GetIndex());
  glBufferData(GL_ARRAY_BUFFER, cbuffer.ByteSize(), cbuffer.DataPtr(),
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glClearColor(0.4f, 0.6f, 0.9f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  glUseProgram(shader.GetProgram());

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(vao);
  glDrawArrays(GL_TRIANGLES, 0, bufferSize);

  context.Render(shader);

  cbuffer.clear();
  vbuffer.clear();

  bufferSize = 0;
}
