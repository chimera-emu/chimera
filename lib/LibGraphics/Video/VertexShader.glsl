#version 330

layout(location = 0) in vec2 vertex_in_position;
layout(location = 1) in vec3 vertex_in_colour;

out vec3 vertex_out_colour;

void main()
{
  vertex_out_colour = vertex_in_colour;

  gl_Position = vec4(vertex_in_position, 0.0, 1.0);
};
