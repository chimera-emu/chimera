#include "LibGraphics/Video/Shader.h"

#include <Support/FileUtility.h>

void Shader::LoadShaders(const std::string& vertexPath, const std::string& fragmentPath)
{
  if (program != 0)
    glDeleteProgram(program);

  program = glCreateProgram();

  // Create vertex shader
  std::string vertexString = Utility::FileToString(vertexPath);
  const char* vertexSource = vertexString.c_str();

  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSource, nullptr);
  glCompileShader(vertexShader);
  glAttachShader(program, vertexShader);
  glDeleteShader(vertexShader);

  // Create fragment shader
  std::string fragmentString = Utility::FileToString(fragmentPath);
  const char* fragmentSource = fragmentString.c_str();

  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);
  glCompileShader(fragmentShader);
  glAttachShader(program, fragmentShader);
  glDeleteShader(fragmentShader);

  glLinkProgram(program);
  glFlush();
}
