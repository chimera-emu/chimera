#include "ConfigData.h"

#include "CommandLineException.h"

#include <docopt/docopt.h>
#include <fmt/format.h>

#include <fstream>
#include <string>

static const char* const usage =
R"(Chimera - Headless Client

Usage:
  chimera-headless (--bios=<path>)

Options:
  --bios=<path>  System path to the BIOS file.
)";

namespace
{
  inline bool FileExists(const std::string& filepath)
  {
    std::ifstream infile(filepath.c_str());

    return infile.is_open();
  }
} // anonymous namespace


ConfigData::ConfigData(int argc, char** argv) : bios_{}
{
  constexpr auto name     = "Chimera - Headless Mode";
  constexpr auto version  = "0.0.0";
  const auto combined     = fmt::format("{} {}", name, version);

  auto args = docopt::docopt(usage, {argv+1, argv+argc}, true, combined);

  // Parse the command line arguments to constuct the config data.
  bios_ = args.find("--bios")->second.asString();

  if (!FileExists(bios_))
  {
    const auto error = fmt::format("Invalid BIOS path: {}", bios_);

    throw CommandLineException(error);
  }
}
