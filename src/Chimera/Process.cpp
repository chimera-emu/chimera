#include "Process.h"

#include <Core/PSX/Emulator.h>
#include <LibGraphics/Window/Window.h>
#include <LibGraphics/Window/Event.h>
#include <LibGraphics/Window/Keyboard.h>

void Process::Initialise()
{
  window_   = std::make_unique<Window>(800, 600, "Chimera Emulator - PSX");
  emulator_ = std::make_unique<Emulator>(config_.Bios(), window_->RenderHandle());
}

void Process::Run()
{
  while (window_->IsOpen())
  {
    Event event;
    while (window_->FetchEvent(event))
    {
      if (event.IsKeyboardEvent())
      {
        const std::string newTitle = std::string("Key Pressed: " +
          Keyboard::KeyCodeToString(event.keyboardEvent.code));

        window_->SetTitle(newTitle);
      }
    }
  }
}

void Process::Shutdown()
{
  // TODO: Implement a clean shutdown
}
