#include "Chimera/Process.h"

#include <Common/ConfigData.h>

#include <memory>

int main(int argc, char** argv)
{
  ConfigData config{argc, argv};

  auto process = std::make_unique<Process>(config);
  process->Initialise();
  process->Run();
  process->Shutdown();
}
