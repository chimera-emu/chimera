#include "ChimeraProcess.h"

#include "Logger.h"

void ChimeraProcess::Initialise()
{
  Logger()->info("Backend Process - Initialising");

  CreateMachine();
}

void ChimeraProcess::Run()
{
  Logger()->info("Backend Process - Running");

  machine_->Run();
}

void ChimeraProcess::Shutdown()
{
  Logger()->info("Backend Process - Shutting down");
}

void ChimeraProcess::CreateMachine()
{
  Logger()->info("Backend Process - Creating Machine");

  machine_ = std::make_unique<Machine>(config_.Bios());
}
