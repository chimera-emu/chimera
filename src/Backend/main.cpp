#include "ChimeraProcess.h"

#include <Common/ConfigData.h>

#include <memory>

int main(int argc, char** argv)
{
  // TODO: Split the frontend and backend command line when IPC is added
  ConfigData config{argc, argv};

  auto process = std::make_unique<ChimeraProcess>(config);
  process->Initialise();
  process->Run();
  process->Shutdown();
}
